package util;

import java.util.ArrayList;
import java.util.List;


import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.FechamentoConta;
import br.com.cerradoinformatica.ePub.funcionalidades.Listas;
import br.com.cerradoinformatica.ePub.model.MovimentoMesa;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import android.content.Context;
import android.util.Log;



public class EpubUtils {
	
	private final static String TAG = "EpubUtils";
	private final static  String FILE_VALOR_CONTA ="valorConta.txt";
	private final static String FILE_MOVIMENTO_MESA="movimento.txt";
	private final static String FILE_MOVIMENTO_PRODUTO="movimentoProduto.txt";
	private final static String FILE_LISTAS = "listas.txt";
	private final static String FILE_MOVIMENTO_SOLICITACAO_AUTORIZACAO="movimentoSolicitacaoAutorizacao.txt";
	private final static String FILE_MOV_SOLICITACAO_FECHAMENTO_CONTA="movFechamento.txt";

	
	public static void setValorConta(Context context , double valorUnitario , int qtd){
		
		double valor = learquivo(context);
		valor+=valorUnitario*qtd;
		IOUtils.writeString(context, valor+"", FILE_VALOR_CONTA);
		
	}
	public static void setValorConta(Context context, double valor){
		IOUtils.writeString(context, valor+"", FILE_VALOR_CONTA);
		
	}
	public static double getValorConta(Context context){
		
		return learquivo(context);
	}
	
	private static  double learquivo(Context context){
		
		try{
			double valor = Double.parseDouble(IOUtils.readString(context, FILE_VALOR_CONTA));
			return valor;
			
		}
		catch(Exception e){
			Log.e(TAG, e.getMessage());
			return 0;
		}
		
	
	}
	/*public static int getImgGrupo(int idGrupo){
		
		int drawable=0;
		
		try {
			switch (idGrupo) {
			case 1:
				drawable=R.drawable.beer;
				break;
			case 2:
				drawable=R.drawable.refri;
				break;
			case 3:
				drawable=R.drawable.suco;
				break;
			case 4:
				drawable=R.drawable.petisco2;
				break;
			case 5:
				drawable=R.drawable.drink;
				break;
			case 6:
				drawable=R.drawable.espeto2;
				break;
			case 7:
				drawable=R.drawable.shake;
				break;

			default:
				break;
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}
		
		return drawable;
	}
	
	public static int getImgGrupo(String nomeImagem){
		
		int drawable=R.drawable.beer;
		
		try {
			if(nomeImagem.equalsIgnoreCase("Grupo_Cerveja.jpg")){
				drawable=R.drawable.beer;
			}
			else if(nomeImagem.equalsIgnoreCase("Grupo_Refrigerante.jpg")){
				drawable=R.drawable.refri;
			}
			else if(nomeImagem.equalsIgnoreCase("Grupo_Suco.jpg")){
				drawable=R.drawable.suco;
			}
			else if(nomeImagem.equalsIgnoreCase("Grupo_Petisco.jpg")){
				drawable = R.drawable.petisco2;
			}
			else if(nomeImagem.equalsIgnoreCase("Grupo_Drink.jpg")){
				drawable=R.drawable.drink;
			}
			else if(nomeImagem.equalsIgnoreCase("Grupo_Shake.jpg")){
				drawable=R.drawable.shake;
			}
			
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
		}
		return drawable;
		
	}*/
	
	public static void writeMovimentoMesa(Context context, MovimentoMesa movimentoMesa){
		
		
		IOUtils.writeObjectInFile(context, movimentoMesa, FILE_MOVIMENTO_MESA);
	}
	public static  MovimentoMesa readMovimentoMesa(Context context){
		MovimentoMesa movimento = new MovimentoMesa();
		movimento =IOUtils.readObjectInFile(context, FILE_MOVIMENTO_MESA);
		return movimento;
	}
	
	public static  void writeMovimentoProduto(Context context, MovimentoProduto movimento){
		
		IOUtils.writeObjectInFile(context, movimento, FILE_MOVIMENTO_PRODUTO);
		
		
	}
	public static  MovimentoProduto readMovimentoProduto(Context context){
		MovimentoProduto movimento =IOUtils.readObjectInFile(context,FILE_MOVIMENTO_PRODUTO);
		return movimento;
	}
	
	public static void writeListas(Context context, Listas listas){
			IOUtils.writeObjectInFile(context, listas, FILE_LISTAS);
		}
	
	public static Listas readLisas(Context context){
		Listas listas;
		listas=IOUtils.readObjectInFile(context, FILE_LISTAS);
		
		return listas;
	}
	
	public static void writeMovimentoSolicitacaoAutorizacao(Context context, MovimetoSolicitacaoAutorizacao mov){
		IOUtils.writeObjectInFile(context, mov, FILE_MOVIMENTO_SOLICITACAO_AUTORIZACAO);
	}
	
	public static MovimetoSolicitacaoAutorizacao readMovimentoSolicitacaoAutorizacao(Context context){
		MovimetoSolicitacaoAutorizacao mov;
		mov=IOUtils.readObjectInFile(context, FILE_MOVIMENTO_SOLICITACAO_AUTORIZACAO);
		
		return mov;
	}
	
	public static void writeFechamentoConta(Context context, FechamentoConta fechamentoConta){
		IOUtils.writeObjectInFile(context, fechamentoConta, FILE_MOV_SOLICITACAO_FECHAMENTO_CONTA);
	}
	
	public static FechamentoConta readFechamentoConta(Context context){
		FechamentoConta fechamentoConta;
		fechamentoConta= IOUtils.readObjectInFile(context, FILE_MOV_SOLICITACAO_FECHAMENTO_CONTA);
		return fechamentoConta;
	}
	
	public static void clearFiles(Context context){
		IOUtils.writeObjectInFile(context, null,FILE_LISTAS);
		IOUtils.writeObjectInFile(context, null,FILE_MOVIMENTO_MESA);
		IOUtils.writeObjectInFile(context, null,FILE_MOVIMENTO_PRODUTO);
		IOUtils.writeObjectInFile(context, null,FILE_VALOR_CONTA);
		IOUtils.writeString(context, "0.0", FILE_VALOR_CONTA);
		IOUtils.writeObjectInFile(context, null, FILE_MOVIMENTO_SOLICITACAO_AUTORIZACAO);
		IOUtils.writeObjectInFile(context, null, FILE_MOV_SOLICITACAO_FECHAMENTO_CONTA);
	}
	
	public static String formataData(String strData){

		 
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		
	//	Calendar cl = Calendar.getInstance();
	//	Calendar = new Cale
		
		 strData=strData.replace("T","_");
		 strData= strData.replace(":", "-");
		 return strData;
		
	
	}
	
	public static List<MovimentoProduto> arrayMovProdutoToList(MovimentoProduto[] movimentos){
		List<MovimentoProduto> lstMovimentoProdutos = new ArrayList<MovimentoProduto>();
		
		for(int i=0;i<movimentos.length;i++){
			lstMovimentoProdutos.add(movimentos[i]);
		
		
	}
		return lstMovimentoProdutos;
	}


}
