package util;

import android.os.NetworkOnMainThreadException;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/********************Horarios*********************
 * 5-01-2014/7:13  Chegada - Juliano Vince de Campos.
 * 30-01-2014/12:00 Inicio do almoço - Juliano Vince de Campos
 * 30-01-2014/13:02 Volta do almoço - Juliano Vince de Campos
 * 30-01-2014/16:21 Saida - Juliano Vince de Campos
 ************************************************/

public class HttpUtil {
	private final static String TAG="<< HttpUtil >>";
    private static int TIME_OUT = 10000;
    public static String MENSAGEM;

    public static void alteraTimeOut(int TIME_OUT_PASSADO){
        TIME_OUT = TIME_OUT_PASSADO;
    }

    public static String  httpGet(String url){
        try {
            MENSAGEM = "";
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams,TIME_OUT); //tempo para estabelecer a conexão.
            HttpConnectionParams.setSoTimeout(httpParams,TIME_OUT);   //Tempo em que fica aguardando a resposta de dados.
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
            HttpGet httpGet = new HttpGet(url);
            HttpResponse response;

            response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK )
                MENSAGEM = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
            if(response.getStatusLine().getStatusCode() == 401){
                MENSAGEM = "401";
            }
        }catch (UnknownHostException e){
            MENSAGEM = "ERRO_COMUNICACAO_COM_O_SERVIDOR";
            Log.e(TAG,"Erro: " +e.getMessage() + " causa = " + e.getCause() + " Mensagem = " + MENSAGEM);
        }catch (ConnectTimeoutException e){
            MENSAGEM = "TIME_OUT";
            Log.e(TAG,"Erro: " +e.getMessage() + " causa = " + e.getCause() + " Mensagem = " + MENSAGEM);
        }catch (IOException e){
            MENSAGEM = "ERRO_DESCONHECIDO";
            Log.e(TAG,"Erro: " +e.getMessage() + " causa = " + e.getCause() + " Mensagem = " + MENSAGEM);
        }
        return MENSAGEM;
    }

    public static String httpPost(String url,String strJson){
        try {
            MENSAGEM = "";

            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, TIME_OUT); //tempo para estabelecer a conexão.
            HttpConnectionParams.setSoTimeout(httpParams,TIME_OUT);   //Tempo em que fica aguardando a resposta de dados.
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
            HttpPost httpPost = new HttpPost(url);
            StringEntity se = new StringEntity(strJson, HTTP.UTF_8);
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            httpPost.setEntity(se);
            HttpResponse response;

            response = httpClient.execute(httpPost);
            response.setHeader(new BasicHeader(HTTP.CONTENT_ENCODING, HTTP.UTF_8));
            if(response.getStatusLine().getStatusCode()==304){
                MENSAGEM = ""+response.getStatusLine().getStatusCode();
            }else if(response.getStatusLine().getStatusCode()==300){
                MENSAGEM = "300";
            }else {
                HttpEntity entity = response.getEntity();
                MENSAGEM = EntityUtils.toString(entity,HTTP.UTF_8);
            }
        }catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage());
            MENSAGEM = e.toString();
        }catch (UnknownHostException e){
            MENSAGEM = "ERRO_COMUNICACAO_COM_O_SERVIDOR";
        }catch (ConnectTimeoutException e){
            MENSAGEM = "TIME_OUT";
        }catch(SocketTimeoutException e){
            MENSAGEM = "ERRO_FLUXO_DE_DADOS_INTERROMPIDO";
        }catch (IOException e) {
            MENSAGEM = "ERRO_DESCONHECIDO";
        }
        return MENSAGEM;
    }
    public static String httpPut(String url,String strJson){
        try {

            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams,TIME_OUT); //tempo para estabelecer a conexão.
            HttpConnectionParams.setSoTimeout(httpParams,TIME_OUT);   //Tempo em que fica aguardando a resposta de dados.
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
            HttpPut httpPut = new HttpPut(url);
            StringEntity se = new StringEntity(strJson, HTTP.UTF_8);
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            httpPut.setEntity(se);
            HttpResponse response = httpClient.execute(httpPut);
            response.setHeader(new BasicHeader(HTTP.CONTENT_ENCODING, HTTP.UTF_8));
            HttpEntity entity = response.getEntity();
            MENSAGEM = EntityUtils.toString(entity,HTTP.UTF_8);

        }catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.getMessage());
            MENSAGEM = e.toString();
        }catch (UnknownHostException e){
            MENSAGEM = "ERRO_COMUNICACAO_COM_O_SERVIDOR";
        }catch (ConnectTimeoutException e){
            MENSAGEM = "TIME_OUT";
        }catch(SocketTimeoutException e){
            MENSAGEM = "ERRO_FLUXO_DE_DADOS_INTERROMPIDO";
        }catch (IOException e) {
            MENSAGEM = "ERRO_DESCONHECIDO";
        }
        return MENSAGEM;
    }
	/*
	public static String httpPut(String url,String strJson){
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams,TIME_OUT); //tempo para estabelecer a conexão.
        HttpConnectionParams.setSoTimeout(httpParams,TIME_OUT);   //Tempo em que fica aguardando a resposta de dados.
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
		HttpPut httpPut = new HttpPut(url);
		StringEntity se;
		try {
			se = new StringEntity(strJson);
			se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpResponse response = httpClient.execute(httpPut);
			String retorno = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
			return retorno;
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, e.getMessage());
			return null;
		} catch (ClientProtocolException e) {
			Log.e(TAG, e.getMessage());
			return null;
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
			return null;
		}
	}
    */
}
