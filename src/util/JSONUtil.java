package util;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import br.com.cerradoinformatica.ePub.model.MovimentoMesa;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;

public class JSONUtil {
	
	private static String TAG ="JSONUTIL";
	
	public static MovimetoSolicitacaoAutorizacao jsonToMovimentoSolicitacaoAutorizacao(String strJson){
		
		try {
			JSONObject holder = new JSONObject(strJson);
			MovimetoSolicitacaoAutorizacao mov = new MovimetoSolicitacaoAutorizacao();
			mov.setIdEstabelecimento(holder.getInt("IdEstabelecimento"));
			mov.setIdMesa(holder.getInt("IdMesa"));
			mov.setData(holder.getString("Data"));
			mov.setNumComanda(holder.getInt("Numero_da_Comanda"));
			mov.setDataAutorizacao(holder.getString("Data_da_Autorizacao"));
			mov.setDataNegacao("Data_da_Negacao");
			mov.setNegadoPor(holder.getInt("Negado_Por"));
			mov.setDataAbertura(holder.getString("Data_da_Abertura"));
			mov.setNumero(holder.getInt("Numero"));
			mov.setIdUsuarioWeb(holder.getInt("IdUsuario_Web"));

			
			return mov;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, e.getMessage());
			return null;
		}
		
	}
	
	public static MovimentoMesa jsonToMovimentoMesa(String strJson){
		
		try {
			JSONObject holder = new JSONObject(strJson);
			MovimentoMesa mov = new MovimentoMesa();
			mov.setIdEstabelecimento(holder.getInt("IdEstabelecimento"));
			mov.setIdMesa(holder.getInt("IdMesa"));
		//	mov.setData(holder.getString("Data"));
			mov.setNumComanda(holder.getInt("Numero_da_Comanda"));
			mov.setDadtaAutorizacao(holder.getString("Data_da_Autorizacao"));
		
			
			return mov;
			
		} catch (JSONException e) {
			Log.e(TAG, e.getMessage());
			return null;
		}
		
	}

}
