package util;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.Perfil;
import br.com.cerradoinformatica.ePub.txts.PerfilTXT;

import android.content.Context;

public class URLUtil {

	public enum Mode{
		DESENVOLVIMENTO, PRODUCAO;
	}

    private static String  MENSAGEM_VERSAO = "OK";

	public static Mode mode;

    public static void setMode(String modeString){
        if(modeString.equalsIgnoreCase("PRODUCAO")){
            mode = Mode.PRODUCAO;
        }else{
            mode = Mode.DESENVOLVIMENTO;
        }
    }
    private static Mode getMode(){
        /*if(mode == null || mode.equals(null)){
            mode = Mode.PRODUCAO;
        }*/
        return Mode.PRODUCAO;
    }

    public static String versao(String jsonRetorno){
        String novoJsonRetorno = jsonRetorno.substring(1,2);
        if(getMode() == Mode.PRODUCAO && novoJsonRetorno.equalsIgnoreCase("P")){
            return MENSAGEM_VERSAO;
        }else if(getMode() == Mode.DESENVOLVIMENTO && novoJsonRetorno.equalsIgnoreCase("T")){
            return MENSAGEM_VERSAO;
        }else if(getMode() == Mode.PRODUCAO && novoJsonRetorno.equalsIgnoreCase("T")){
            return MENSAGEM_VERSAO = "VARIAVEL_PRODUCAO_JSON_TESTE";
        }else {
            return MENSAGEM_VERSAO = "VARIAVEL_DESENVOLVIMENTO_JSON_PRODUCAO";
        }
    }
	
	public static String getUrlEstabelecimento(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlEstabelecimento);
		}
		else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlEstabelecimento);
		}
		return "";
	}
	
	public static String getUrlMesa(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlMesa);
		}
		else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlMesa);
		}
		return "";
	}
    public static String getUrlMesas(Context context){
        if(getMode() == Mode.DESENVOLVIMENTO){
            return context.getResources().getString(R.string.testeUrlMesas);
        }
        else if(getMode() == Mode.PRODUCAO){
            return context.getResources().getString(R.string.producaoUrlMesas);
        }
        return "";
    }
 
	public static String getUrlMovSoliciatcaoAuturizacao(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlMovSolicitacaoAutorizacao);
		}
		else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlMovSolicitacaoAutorizacao);
		}
		return "";
	}
	
	public static String getUrlMovAberturaMesa(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlMovAberturaMesa);
		}
		else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlMovAberturaMesa);
		}
		return "";
	}
	
	public static String getUrlVWProdutosEstabelecimento(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlVWProdutoEstabelecimento);
		}
		else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlVWProdutoEstabelecimento);
		}
		return "";
	}
    public static String getUrlProdutosEstabelecimento(Context context){
        if(getMode() == Mode.DESENVOLVIMENTO){
            return context.getResources().getString(R.string.testeUrlProdutoEstabelecimento);
        }
        else if(getMode() == Mode.PRODUCAO){
            return context.getResources().getString(R.string.producaoUrlProdutoEstabelecimento);
        }
        return "";
    }
	
	public static String getUrlGrupos(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlGrupo);
		}
		else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlGrupo);
		}
		return "";
	}
	
	public static String getUrlMovProduto(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlMovProduto);
		}
		else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlMovProduto);
		}
		return "";
	}
	
	public static String getUrlVwMovProduto(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlVwMovProduto);
		}
		else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlVwMovProduto);
		}
		return "";
	}
	public static String getUrlFechamentoConta(Context context){
		if(getMode() == Mode.DESENVOLVIMENTO){
			return context.getResources().getString(R.string.testeUrlFechamentoConta);
		}else if(getMode() == Mode.PRODUCAO){
			return context.getResources().getString(R.string.producaoUrlFechamentoConta);
		}
		return "";
	}
    public static String getUrlVerificaAPISerive(Context context){
        if(getMode() == Mode.DESENVOLVIMENTO){
            return context.getResources().getString(R.string.testeUrlVersaoAPI);
        }else if(getMode() == Mode.PRODUCAO){
            return context.getResources().getString(R.string.producaoUrlVersaoAPI);
        }
        return "";
    }
    public static String getUrlPreferenciaProduto(Context context){
        if(getMode() == Mode.DESENVOLVIMENTO){
            return context.getResources().getString(R.string.testeUrlPreferenciaProduto);
        }else if(getMode() == Mode.PRODUCAO){
            return context.getResources().getString(R.string.producaoUrlPreferenciaProduto);
        }
        return "";
    }
    public static String getUrlGruposProdutos(Context context){
        if(getMode() == Mode.DESENVOLVIMENTO){
            return context.getResources().getString(R.string.testeUrlGrupoProduto);
        }else if(getMode() == Mode.PRODUCAO){
            return context.getResources().getString(R.string.producaoUrlGrupoProduto);
        }
        return "";
    }
    public static String getUrlUsuario(Context context){
        if(getMode() == Mode.DESENVOLVIMENTO){
            return context.getResources().getString(R.string.testeUrlUsuarioDispositivo);
        }
        else if(getMode() == Mode.PRODUCAO){
            return context.getResources().getString(R.string.producaoUrlUsuarioDispositivo);
        }
        return "";
    }
    public static String getUrlImagens(Context context){
        return "https://nuvemdocerrado.blob.core.windows.net/mobilepub/";
    }
}
