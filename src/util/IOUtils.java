package util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;

//import br.com.cerradoinformatica.ePub.model.HistoricoConta;

public class IOUtils {
    private final static String TAG="IOUTILS";


    public static String toString(InputStream in,String charset) {
        String texto = "";
        try {

        InputStreamReader ir = new InputStreamReader(in);
        BufferedReader bfr = new BufferedReader(ir);

        byte[] bytes = toBytes(in);
        texto = new String(bytes,charset);
        }catch (IOException e){

        }
        return texto;
    }

    public static byte[] toBytes(InputStream in){
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {

            byte[] buffer = new byte[1024];
            int len;

            while((len=in.read(buffer))>0){
                bos.write(buffer,0,len);
            }
            byte[] bytes = bos.toByteArray();
            return bytes;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }

    }
    public static void writeObjectInFile(Context context,Serializable obj,String nomeArquivo){
        try {
            FileOutputStream fos = context.openFileOutput(nomeArquivo, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
            oos.close();
            fos.close();

            Log.i(TAG, "OBJETO GRAVADO COM SUCESSO");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.getMessage());
        }
    }

    public static <T> T readObjectInFile(Context context,String nomeArquivo){
        File file = context.getFileStreamPath(nomeArquivo);
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            @SuppressWarnings("unchecked")
            T t = (T) ois.readObject();
            ois.close();
            fis.close();
            return t;

        }
        catch(FileNotFoundException fe){
            writeObjectInFile(context, null, nomeArquivo);
            return null;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.getMessage());

            return null;
        }
    }
    public static void clearFile(String nomeDoarquivo){

        File diretorioAqrquivo = new File("/data/data/br.com.cerradoinformatica.ePub.activity/files/"+nomeDoarquivo);
        boolean existe = diretorioAqrquivo.exists();
        if(existe){
            Log.i(TAG,"Arquivo Existe! Excluindo.");
            diretorioAqrquivo.delete();
        }else{
            Log.i(TAG,"Arquivo não Existe!");
        }
    }
    public static ArrayList<String> readListInFile(Context context){
        ArrayList<String>listaDados = new ArrayList<String>();
        File file = new File(Environment.getDataDirectory() + "/data/br.com.cerradoinformatica.ePub.activity/files/");

        File[] files = file.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().startsWith("MovProduto_");
           }
        });
        String[] vetor = new String[files.length];
        for (int i = 0; i < files.length; ++i) {
            vetor[i] = files[i].toString();
            String[] ultPasta = vetor[i].toString().split("/");
            listaDados.add(ultPasta[5]);
            Log.i("Aplicativo", "Arquivo : "+ vetor[i].toString());
        }
        return listaDados;
    }
  /*  public static ArrayList<String> clearFile(Context context){
        ArrayList<String>listaDados = new ArrayList<String>();
        File file = new File(Environment.getDataDirectory() + "/data/br.com.cerradoinformatica.ePub.activity/files/");

        File[] files = file.listFiles(new FileFilter() {
            public boolean accept(File pathname) {
                return pathname.getName().startsWith("MovProduto_");
            }
        });
        String[] vetor = new String[files.length];
        for (int i = 0; i < files.length; ++i) {

        }
        return listaDados;
    }*/
    public static void writeString(Context context,String value,String nomeArquivo){

        File file = context.getFileStreamPath(nomeArquivo);
        try{
            OutputStream os = new FileOutputStream(file);
            OutputStreamWriter ow = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(ow);

            bw.write(value);

            bw.close();

        }catch(Exception e){
            Log.e(TAG, e.getMessage());
        }
    }

    public static String readString(Context context,String nomeArquivo){

        File file = context.getFileStreamPath(nomeArquivo);
        try{
            InputStream in = new FileInputStream(file);
            InputStreamReader ir = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(ir);
            StringBuilder strBuilder = new StringBuilder();

            while(br.ready()){
                strBuilder.append(br.readLine());
            }

            br.close();

            return strBuilder.toString();


        }
        catch(FileNotFoundException fe){
            writeString(context, "0.0", nomeArquivo);
            return "0.0";
        }
        catch(Exception e){
            Log.e(TAG, e.getMessage());
            return null;
        }
    }
  /*  public static String readStringProdutos(Context context,String nomeArquivo){

        File file = context.getFileStreamPath(nomeArquivo);
        try{
            InputStream in = new FileInputStream(file);
            InputStreamReader ir = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(ir);
            StringBuilder strBuilder = new StringBuilder();

            int i =0;
            while(br.ready()){
                if (i==0){
                    String xTeste = br.readLine();
                    if (xTeste.startsWith("[")){
                        strBuilder.append(xTeste);
                    }
                }
                else{
                    strBuilder.append(br.readLine());
                }
                i++;
            }

            br.close();

            return strBuilder.toString();


        }
        catch(FileNotFoundException fe){
            writeString(context, "0.0", nomeArquivo);
            return "0.0";
        }
        catch(Exception e){
            Log.e(TAG, e.getMessage());
            return null;
        }
    }
    */
}
