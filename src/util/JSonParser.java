package util;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;
import br.com.cerradoinformatica.ePub.funcionalidades.DatasUtils;
import br.com.cerradoinformatica.ePub.model.*;
import br.com.cerradoinformatica.ePub.model.DateUtil;

public class JSonParser {

    private static final String TAG = "<< JSONPARSER >>";


    //INICIO GRUPO PRODUTO
    private static final String ID_GRUPO_PRODUTO = "Id_GrupoProduto";
    private static final String NOME_GRUPO_PRODUTO = "Nome_GrupoProduto";
    private static final String NOME_IMAGEM_GRUPO_PRODUTO = "NomeImagem_GrupoProduto";
    // FIM GRUPO PRODUTO

    //INICIO MOVIMENTO PRODUTO
    private static final String ID_ESTABELECIMENTO = "IdEstabelecimento";
    private static final String ID_MESA = "IdMesa";
    private static final String DATA_DA_ABERTURA = "Data_da_Abertura";
    private static final String NUMERO_DA_COMANDA = "Numero_da_Comanda";
    private static final String DATA_DO_MOVIMENTO = "Data_do_Movimento";
    private static final String ID_PRODUTO = "IdProduto";
    private static final String VALOR_UNITARIO = "Valor_Unitario";
    private static final String QUANTIDADE = "Quantidade";
    private static final String ID_USUARIO_MOVEL = "IdUsuario_Movel";
    private static final String DETALHE = "Detalhe";
    private static final String ESTABELECIMENTO_CLIENTE = "Estabelecimento_Ciente";
    private static final String PRODUCAO_INICIADA = "Producao_Iniciada";
    private static final String PRODUCAO_CONCLUIDA = "Producao_Concluida";
    private static final String GARCON_ACIONADO = "Garcon_Acionado";
    private static final String CANCELADO_USUARIO = "Cancelado_Usuario";
    private static final String CANCELADO_ESTABELECIMENTO = "Cancelado_Estabelecimento";
    private static final String ENTREGUE_NA_MESA = "Entregue_na_Mesa";
    //FIM MOVIMENTO PRODUTO

    //INICIO PRODUTO ESTABELECIMENTO
    private static final String ID_ESTABELECIMENTO_PRODUTO_ESTABELECIMENTO = "IdEstabelecimento_ProdutoEstabelecimento";
    private static final String ID_PRODUTO_ESTABELECIMENTO =  "Id_ProdutoEstabelecimento";
    private static final String NOME_PRODUTO_ESTABELECIMENTO = "Nome_ProdutoEstabelecimento";
    private static final String CARACTERISTICA_PRODUTO_ESTABELECIMENTO = "Caracteristica_ProdutoEstabelecimento";
    private static final String UNIDADE_PRODUTO_ESTABELECIMENTO = "Unidade_ProdutoEstabelecimento";
    private static final String PRECO_VENDA_PRODUTO_ESTABELECIMENTO =  "PrecoVenda_ProdutoEstabelecimento";
    private static final String ID_PRODUTO_SINCRONIZADO_PRODUTO_ESTABELECIMENTO = "IdProdutoSincronizado_ProdutoEstabelecimento";
    private static final String ID_GRUPO_PRODUTO_ESTABELECIMENTO = "IdGrupo_ProdutoEstabelecimento";
    private static final String NOME_GRUPO = "NomeGrupo";
    private static final String NOME_IMAGEM_PRODUTO_ESTABELECIMENTO = "NomeImagem_ProdutoEstabelecimento";
    private static final String COZINHA_PRODUTO_ESTABELECIMENTO = "Cozinha_ProdutoEstabelecimento";
    private static final String BARMAN_PRODUTO_ESTABELECIMENTO = "Barman_ProdutoEstabelecimento";
    //FIM PRODUTO ESTABELECIMENTO

    //INICIO ESTABELECIMENTO jsonToEstabelecimentoList
    private static final String ESTABELECIMENTO_CODIGO = "codigo";
    private static final String ESTABELECIMENTO_RAZAO_SOCIAL = "razao_social";
    private static final String ESTABELECIMENTO_NOME_FANTASIA = "nome_fantasia";
    private static final String ESTABELECIMENTO_NOME_IMAGEM_ESTABELECIMENTO = "NomeImagem_Estabelecimento";
    private static final String TAXA_SERVICO ="taxa_servico";
    //FIM ESTABELECIMENTO

    //MOVIMENTO PRODUTO FECHAMENTO jsonToMovimentoProdutoList
    private static final String MOVIMENTO_PRODUTO_FECHAMENTO_NOME_PRODUTO_ESTABELECIMENTO = "Nome_ProdutoEstabelecimento";
    private static final String MOVIMENTO_PRODUTO_FECHAMENTO_QUANTIDADE = "Quantidade";
    private static final String MOVIMENTO_PRODUTO_FECHAMENTO_VALOR_UNITARIO = "ValorUnitario";
    private static final String MOVIMENTO_PRODUTO_FECHAMENTO_TOTAL = "Total";
    //MOVIMENTO PRODUTO FECHAMENTO

    //MOVIMENTO PRODUTO ACOMPANHAMENTO
    private static final String ACOMPANHAMENTO_NOME_PRODUTO_ESTABELECIMENTO = "Nome_ProdutoEstabelecimento";
    private static final String ACOMPANHAMENTO_QUANTIDADE = "Quantidade";
    private static final String ACOMPANHAMENTO_VALOR_UNITARIO = "ValorUnitario";
    private static final String ACOMPANHAMENTO_TOTAL = "Total";
    private static final String ACOMPANHAMENTO_DATA_MOVIMENTO = "DataMovimento";
    //MOVIMENTO PRODUTO ACOMPANHAMENTO

    //CLIENTE DISPOSITIVO
    private static final String ID_CLIENTE_DISPOSITIVO = "Id_ClienteDispositivo";
    private static final String NOME_CLIENTE_DISPOSITIVO = "Nome_ClienteDispositivo";
    private static final String EMAIL_CLIENTE_DISPOSITIVO = "Email_ClienteDispositivo";
    private static final String TELEFONE_CLIENTE_DISPOSITIVO = "Telefone_ClienteDispositivo";
    private static final String DATA_NASCIMENTO_CLIENTE_DISPOSITIVO = "DataNascimento_ClienteDispositivo";
    private static final String SEXO_CLIENTE_DISPOSITIVO = "Sexo_ClienteDispositivo";
    private static final String SENHA_CLIENTE_DISPOSITIVO = "Senha_ClienteDispositivo";
    private static final String FABRICANTE_HARDWARE_CLIENTE_DISPOSITIVO ="FabricanteHardware_ClienteDispositivo";
    private static final String MODELO_HARDWARE_CLIENTE_DISPOSITIVO = "ModeloHardware_ClienteDispositivo";
    private static final String VERSAO_HARDWARE_CLIENTE_DISPOSITIVO = "VersaoHardware_ClienteDispositivo";
    private static final String SO_DISPOSITIVO_CLIENTE_DISPOSITIVO = "SODispositivo_ClienteDispositivo";
    private static final String ID_DISPOSITIVO_CLIENTE_DISPOSITIVO = "IDDispositivo_ClienteDispositivo";
    private static final String EXCLUIDO_CLIENTE_DISPOSITIVO = "Excluido_ClienteDispositivo";
    private static final String DATA_ALTERACAO_CLIENTE_DISPOSITIVO = "DataAlteracao_ClienteDispositivo";
    private static final String DATA_CADASTRO_CLIENTE_DISPOSITIVO = "DataCadastro_ClienteDispositivo";
    private static final String ID_FACEBBOK_CLIENTE_DISPOSITIVO = "IdFacebook_ClienteDispositivo";
    private static final String URI_IMAGEM_CLIENTE_DISPOSITIVO = "UriImagem_ClienteDispositivo";
    //CLIENTE DISPOSITIVO

    //INICIO COLABORADOR Método: jsonToUsuario
    private static final String COLABORADOR_ID_COLABORADOR = "Id_Colaborador";
    private static final String COLABORADOR_LOGIN_COLABORADOR = "Login_Colaborador";
    private static final String COLABORADOR_NOME_COLABORADOR = "Nome_Colaborador";
    private static final String COLABORADOR_FUNCAO_COLABORADOR = "Funcao_Colaborador";
    private static final String COLABORADOR_ID_GRUPO_COLABORADOR_COLABORADOR = "IdGrupoColaborador_Colaborador";
    private static final String COLABORADOR_IDS_ESTABELECIMENTO = "IdsEstabelecimentos";
    private static final String COLABORADOR_NOMES_ESTABELECIMENTOS = "NomesEstabelecimentos";
    private static final String COLABORADOR_NOMES_IMAGENS = "NomesImagens";
    private static final String COLABORADOR_NIVEIS_ACESSO = "NiveisAcessos";
    private static final String COLABORADOR_QUANTIDADES_MESAS = "QuantidadesMesas";
    private static final String COLABORADOR_TAXAS_SERVICOS = "TaxasServicos";
    //FIM COLABORADOR

    //INICIO MOVIMENTO_PRODUTO Método: movimentoProdutoToJSon / jsonToMovimentoProduto
    private static final String MOVIMENTO_PRODUTO_ID_ESTABELECIMENTO = "IdEstabelecimento";
    private static final String MOVIMENTO_PRODUTO_ID_MESA = "IdMesa";
    private static final String MOVIMENTO_PRODUTO_DATA_DA_ABERTURA = "Data_da_Abertura";
    private static final String MOVIMENTO_PRODUTO_NUMERO_DA_COMANDA = "Numero_da_Comanda";
    private static final String MOVIMENTO_PRODUTO_DATA_DO_MOVIMENTO = "Data_do_Movimento";
    private static final String MOVIMENTO_PRODUTO_ID_PRODUTO = "IdProduto";
    private static final String MOVIMENTO_PRODUTO_ID_VALOR_UNITARIO = "Valor_Unitario";
    private static final String MOVIMENTO_PRODUTO_ID_QUANTIDADE = "Quantidade";
    private static final String MOVIMENTO_PRODUTO_ID_USUARIO_MOVEL = "IdUsuario_Movel";
    private static final String MOVIMENTO_PRODUTO_DETALHE = "Detalhe";
    private static final String MOVIMENTO_PRODUTO_ESTABELECIMENTO_CIENTE = "Estabelecimento_Ciente";
    private static final String MOVIMENTO_PRODUTO_PRODUCAO_INICIADA = "Producao_Iniciada";
    private static final String MOVIMENTO_PRODUTO_PRODUCAO_CONCLUIDA = "Producao_Concluida";
    private static final String MOVIMENTO_PRODUTO_GARCOM_ACIONADO = "Garcon_Acionado";
    private static final String MOVIMENTO_PRODUTO_CANCELADO_USUARIO = "Cancelado_Usuario";
    private static final String MOVIMENTO_PRODUTO_CANCELADO_ESTABELECIMENTO = "Cancelado_Estabelecimento";
    private static final String MOVIMENTO_PRODUTO_ENTREGUE_NA_MESA = "Entregue_na_Mesa";
    private static final String MOVIMENTO_PRODUTO_NOME = "Nome";
    private static final String MOVIMENTO_PRODUTO_NOME_DA_IMAGEM = "Nome_da_Imagem";
    private static final String MOVIMENTO_PRODUTO_ID_GARCOM = "id_garcom";
    private static final String MOVIMENTO_PRODUTO_VALOR_UNITARIO = "Valor_Unitario";
    private static final String MOVIMENTO_PRODUTO_QUANTIDADE = "Quantidade";
    private static final String MOVIMENTO_PRODUTO_ESTABELECIMENTO_CLIENTE = "Estabelecimento_Ciente";
    private static final String MOVIMENTO_PRODUTO_ESTABELECIMENTO_AUTORIZADO_GARCON_MOV_PRODUTO = "AutorizadoGarcon_MovProduto";
    //FIM MOVIMENTO_PRODUTO

    //INICIO FECHAMENTO Método: jsonRetornoPost
    private static final String FECHAMENTO_DATA_CONTA = "data_conta";
    private static final String FECHAMENTO_DATA_SOLICITACAO = "data_solicitacao";
    private static final String FECHAMENTO_ID_MESA = "id_mesa";
    private static final String FECHAMENTO_NEGADO_POR = "negado_por";
    private static final String FECHAMENTO_PARCIAL = "parcial";
    private static final String FECHAMENTO_ID = "id";
    private static final String FECHAMENTO_PESSOAS_PARA_RATEIO = "pessoas_para_rateio";
    private static final String FECHAMENTO_NUMERO_DA_COMANDA = "numero_comanda";
    private static final String FECHAMENTO_ID_ESTABELECIMENTO = "id_estabelecimento";
    private static final String FECHAMENTO_ID_FORMA_PAGAMENTO = "id_forma_pagamento";
    private static final String FECHAMENTO_ID_USUARIO_DISPOSITIVO = "id_usuario_dispositivo";
    private static final String FECHAMENTO_SEQUENCIA = "sequencia";
    private static final String FECHAMENTO_TROCO_PARA = "troco_para";
    private static final String FECHAMENTO_ID_GARCOM = "id_garcom";
    private static final String FECHAMENTO_AUTORIZADO_POR = "autorizado_por";
    //FINAL FECHAMENTO

    // INICIO MESA Método: jsonToMesa
    private static final String MESA_ID_ESTABELECIMENTO = "id_estabelecimento";
    private static final String MESA_ID = "id";
    private static final String MESA_NOME = "nome";
    private static final String MESA_NOME_USUARIO_DISPOSITIVO = "nome_usuario_dispositivo";
    private static final String MESA_DISPONIBILIDADE = "disponibilidade";
    private static final String MESA_NUMERO_COMANDA = "numero_comanda";
    private static final String MESA_DATA_ABERTURA = "data_abertura";
    private static final String MESA_ID_USUARIO_DISPOSITIVO = "id_usuario_dispositivo";
    private static final String MESA_ID_SOLICITACAO_AUTORIZACAO = "id_solicitacao_autorizacao";
    private static final String MESA_CODIGO_AUTORIZACAO_MESA = "CodigoAutorizacao_Mesa";
    // FIM MESA

    //INICIO MOVIMENTO SOLICITACAO AUTORIZACAO Método: movimentoSolicitacaoAutorisacaoToJson / jsonToMovimentoSolicitacaoAutorisacao
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID = "id";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_ESTABELECIMENTO = "id_estabelecimento";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_MESA = "id_mesa";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_SOLICITACAO = "data_solicitacao";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_NUMERO_SOLICITACAO = "numero_solicitacao";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_AUTORIZACAO = "data_autorizacao";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_AUTORIZADO_POR = "autorizado_por";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_NEGACAO = "data_negacao";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_NEGADO_POR = "negado_por";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_USUARIO_DISPOSITIVO = "id_usuario_dispositivo";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_ABERTURA = "data_abertura";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_NUMERO_COMANDA = "numero_comanda";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_GARCOM = "id_garcom";
    private static final String MOVIMENTO_SOLICITACAO_AUTORIZACAO_CODIGO_AUTORIZACAO_MOV_SOLICITACAO_AUTORIZACAO = "CodigoAutorizacao_MovSolicitacaoAutorizacao";
    //FIM MOVIMENTO SOLICITACAO AUTORIZACAO

    //INICIO MOVIMENTO_MESA Método: movimentoMesaTojson
    private static final String MOVIMENTO_MESA_ID_MESA = "idMesa";
    private static final String MOVIMENTO_MESA_ID_ESTABELECIMENTO = "IdEstabelecimento";
    private static final String MOVIMENTO_MESA_DATA = "Data";
    private static final String MOVIMENTO_MESA_NUMERO_DA_COMANDA = "Numero_da_Comanda";
    private static final String MOVIMENTO_MESA_DATA_DO_FECHAMENTO = "Data_do_fechamento";
    private static final String MOVIMENTO_MESA_ID_USUARIO_DISPOSITIVO = "IdUsuario_Dispositivo";
    //FIM MOVIMENTO_MESA

    //INICIO FECHAMENTO CONTA Método: fechamentoContaToJson
    private static final String FECHAMENTO_CONTA_ID = "id";
    private static final String FECHAMENTO_CONTA_ID_GARCOM = "id_garcom";
    private static final String FECHAMENTO_CONTA_ID_ESTABELECIMENTO = "id_estabelecimento";
    private static final String FECHAMENTO_CONTA_ID_MESA = "id_mesa";
    private static final String FECHAMENTO_CONTA_DATA_CONTA = "data_conta";
    private static final String FECHAMENTO_CONTA_NUMERO_COMANDA = "numero_comanda";
    private static final String FECHAMENTO_CONTA_ID_USUARIO_DISPOSITIVO = "id_usuario_dispositivo";
    private static final String FECHAMENTO_CONTA_DATA_SOLICITACAO = "data_solicitacao";
    private static final String FECHAMENTO_CONTA_SEQUENCIA = "sequencia";
    private static final String FECHAMENTO_CONTA_PARCIAL = "parcial";
    private static final String FECHAMENTO_CONTA_ID_FORMA_PAGAMENTO = "id_forma_pagamento";
    private static final String FECHAMENTO_CONTA_TROCO_PARA = "troco_para";
    private static final String FECHAMENTO_CONTA_PESSOA_PARA_RATEIO = "pessoas_para_rateio";
    private static final String FECHAMENTO_CONTA_DATA_AUTORIZACAO = "data_autorizacao";
    private static final String FECHAMENTO_CONTA_AUTORIZADO_POR = "autorizado_por";
    private static final String FECHAMENTO_CONTA_DATA_NEGACAO = "data_negacao";
    private static final String FECHAMENTO_CONTA_NEGADO_POR = "negado_por";
    private static final String FECHAMENTO_CONTA_SITUACAO_ATUAL = "situacao_atual";
    //FIM FECHAMENTO

    //INICIO PREFERENCIA PRODUTO
    private static final String PREFERENCIA_PRODUTO_ID = "id";
    private static final String PREFERENCIA_PRODUTO_NOME = "nome";
    private static final String PREFERENCIA_PRODUTO_ID_GRUPO = "id_grupo";
    //FIM PREFERENCIA PRODUTO


	public static String mesaToJSon(Mesa mesa) {
		JSONObject holder = new JSONObject();
		try {
			holder.put("ID", mesa.getId());
			holder.put("Nome", mesa.getNome());
			holder.put("Disponibilidade", mesa.getDisponibilidade());
			holder.put("IDEstabelecimento", mesa.getIDEstabelecimento());
			return holder.toString();
		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}

	public static String mesaToJSon(List<Mesa> lstMesas) {
		JSONArray jsArray = new JSONArray();
		JSONObject holder = new JSONObject();

		try {
			for (Mesa mesa : lstMesas) {
				holder.put("ID", mesa.getId());
				holder.put("Nome", mesa.getNome());
				holder.put("Disponibilidade", mesa.getDisponibilidade());
				holder.put("IDEstabelecimento", mesa.getIDEstabelecimento());
				jsArray.put(holder);
			}
			return jsArray.toString();
		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}

    public static MovimetoSolicitacaoAutorizacao jsonToMovimetoSolicitacaoAutorizacao(String stringJson){
        try{
            JSONArray jsonArray = new JSONArray(stringJson);
            JSONObject holder = null;
            List<MovimetoSolicitacaoAutorizacao> listaMovimetoSolicitacaoAutorizacao = new ArrayList<MovimetoSolicitacaoAutorizacao>();

            for(int i =0; i < jsonArray.length(); i++){

                MovimetoSolicitacaoAutorizacao   movimetoSolicitacaoAutorizacao = new MovimetoSolicitacaoAutorizacao();
                holder = jsonArray.getJSONObject(i);

                movimetoSolicitacaoAutorizacao.setId(holder.getInt("id"));
                movimetoSolicitacaoAutorizacao.setIdEstabelecimento(holder.getInt("id_estabelecimento"));
                movimetoSolicitacaoAutorizacao.setIdMesa(holder.getInt("id_mesa"));
                movimetoSolicitacaoAutorizacao.setDataSolicitacao(holder.getString("data_solicitacao"));
                movimetoSolicitacaoAutorizacao.setNumero(holder.getInt("numero_solicitacao"));
                movimetoSolicitacaoAutorizacao.setDataAutorizacao(holder.getString("data_autorizacao"));
                movimetoSolicitacaoAutorizacao.setAutorizadoPor(holder.getInt("autorizado_por"));
                movimetoSolicitacaoAutorizacao.setDataNegacao(holder.getString("data_negacao"));
                movimetoSolicitacaoAutorizacao.setNegadoPor(holder.getInt("negado_por"));
                movimetoSolicitacaoAutorizacao.setIdUsuarioDispositivo(holder.getInt("id_usuario_dispositivo"));
                movimetoSolicitacaoAutorizacao.setDataAbertura(holder.getString("data_abertura"));
                movimetoSolicitacaoAutorizacao.setNumComanda(holder.getInt("numero_comanda"));
                movimetoSolicitacaoAutorizacao.setCodigoAutorizacaoMoviSolicitacaoAutorizacao(holder.getInt("CodigoAutorizacao_MovSolicitacaoAutorizacao"));

                listaMovimetoSolicitacaoAutorizacao.add(movimetoSolicitacaoAutorizacao);
                return listaMovimetoSolicitacaoAutorizacao.get(i);
            }
            return null;
        }catch (JSONException e){
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
            return null;
        }
    }

    public static String movimentoSolicitacaoAutorisacaoToJSon(
		List<MovimetoSolicitacaoAutorizacao> lstMov) {
		JSONArray jsArray = new JSONArray();
		JSONObject holder;
		try {
			for (MovimetoSolicitacaoAutorizacao mov : lstMov) {
				holder = new JSONObject();
				holder.put("id", mov.getId());
				holder.put("id_estabelecimento", mov.getIdEstabelecimento());
				holder.put("id_mesa", mov.getIdMesa());
				holder.put("data_solicitacao", mov.getDataSolicitacao());
				holder.put("numero_solicitacao", mov.getNumero());
				holder.put("data_autorizacao", mov.getDataAutorizacao());
				holder.put("autorizado_por", mov.getAutorizadoPor());
				holder.put("data_negacao", mov.getDataNegacao());
				holder.put("negado_por", mov.getNegadoPor());
				holder.put("id_usuario_dispositivo",mov.getIdUsuarioDispositivo());
				holder.put("data_abertura", mov.getDataAbertura());
				holder.put("numero_comanda", mov.getNumComanda());
				jsArray.put(holder);
			}
			return jsArray.toString();

		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}

	public static List<MovimetoSolicitacaoAutorizacao> jsonToMovimentoSolicitacaoAutorisacaoList(
		String strJson) {
		try {
			JSONArray jsArray = new JSONArray(strJson);
			JSONObject holder;
			List<MovimetoSolicitacaoAutorizacao> lstMov = new ArrayList<MovimetoSolicitacaoAutorizacao>();
			for (int i = 0; i < jsArray.length(); i++) {
				holder = jsArray.getJSONObject(i);
				MovimetoSolicitacaoAutorizacao mov = new MovimetoSolicitacaoAutorizacao();
				holder = jsArray.getJSONObject(i);
				mov.setId(holder.getInt("id"));
				mov.setIdEstabelecimento(holder.getInt("id_estabelecimento"));
				mov.setIdMesa(holder.getInt("id_mesa"));
				mov.setDataSolicitacao(holder.getString("data_solicitacao"));
				mov.setDataAutorizacao(holder.getString("data_autorizacao"));
				mov.setNumero(holder.getInt("numero_solicitacao"));
				mov.setAutorizadoPor(holder.getInt("autorizado_por"));
				mov.setDataNegacao(holder.getString("data_negacao"));
				mov.setNegadoPor(holder.getInt("negado_por"));
				mov.setIdUsuarioDispositivo(holder.getInt("id_usuario_dispositivo"));
				mov.setDataAbertura(holder.getString("data_abertura"));
				mov.setNumComanda(holder.getInt("numero_comanda"));
				lstMov.add(mov);
			}
			return lstMov;
		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}

	public static String movimentoMesaTojson(
		List<MovimentoMesa> lstMovimentoMesa) {
		JSONArray jsArray = new JSONArray();
		JSONObject holder;
		try {
			for (MovimentoMesa mov : lstMovimentoMesa) {
				holder = new JSONObject();
				holder.put("IdMesa", mov.getIdMesa());
				holder.put("IdEstabelecimento", mov.getIdEstabelecimento());
				holder.put("Data", br.com.cerradoinformatica.ePub.model.DateUtil.getDateString(mov.getData(),"yyyy/MM/dd hh:mm"));
				holder.put("Numero_da_Comanda", mov.getNumComanda());
				holder.put("Data_do_fechamento", br.com.cerradoinformatica.ePub.model.DateUtil.getDateString(mov.getDataFechamento(), "yyyy/MM/dd hh:mm"));
				holder.put("IdUsuario_Dispositivo",mov.getIdUsuarioDispositivo());
				jsArray.put(holder);
			}

			return jsArray.toString();

		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}

	public static List<MovimentoMesa> jsonToMovimentoMesaList(String strJson) {
		try {
			JSONArray jsArray = new JSONArray(strJson);
			JSONObject holder;
			List<MovimentoMesa> lstMov = new ArrayList<MovimentoMesa>();
			for (int i = 0; i < jsArray.length(); i++) {
				holder = jsArray.getJSONObject(i);
				MovimentoMesa mov = new MovimentoMesa();
				mov.setIdMesa(holder.getInt("IdMesa"));
				mov.setIdEstabelecimento(holder.getInt("IdEstabelecimento"));
				mov.setData(br.com.cerradoinformatica.ePub.model.DateUtil.getWebApiDate(holder.getString("Data")));
				mov.setNumComanda(holder.getInt("Numero_da_Comanda"));
				mov.setDataFechamento(br.com.cerradoinformatica.ePub.model.DateUtil.getWebApiDate(holder.getString("Data_do_fechamento")));
				mov.setIdUsuarioDispositivo(holder.getInt("IdUsuario_Dispositivo"));
				lstMov.add(mov);
				holder = new JSONObject();
			}
			return lstMov;
		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}
	public static String produtoToJSon(Produto produto) {
		JSONObject holder = new JSONObject();
		try {
			holder.put("Id", produto.getId());
			holder.put("Id_Grupo", produto.getIdGrupo());
			holder.put("Nome", produto.getNome());
			holder.put("Preco", produto.getPreco());
			holder.put("Nome_da_Imagem", produto.getNomeImagem());
			return holder.toString();
		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}
	public static String produtoToJSon(List<Produto> lstProduto) {
		JSONArray jsArray = new JSONArray();
		JSONObject holder;
		try {
			for (Produto produto : lstProduto) {
				holder = new JSONObject();
				holder.put("Id", produto.getId());
				holder.put("Id_Grupo", produto.getIdGrupo());
				holder.put("Nome", produto.getNome());
				holder.put("Preco", produto.getPreco());
				holder.put("Nome_da_Imagem", produto.getNomeImagem());
				jsArray.put(holder);
			}
			return jsArray.toString();
		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}
	public static Produto jsonToProduto(String strJson) {
		try {
			JSONObject holder = new JSONObject(strJson);
			Produto produto = new Produto();
			produto.setId(holder.getInt("Id"));
			produto.setIdGrupo(holder.getInt("Id_Grupo"));
			produto.setNome(holder.getString("Nome"));
			produto.setPreco(holder.getDouble("Preco"));
			produto.setNomeImagem(holder.getString("Nome_da_Imagem"));
			return produto;
		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}
    public static Usuario jsonToUsuarioArray(String stringJson){
        try{
            JSONArray jsonArray = new JSONArray(stringJson);
            JSONObject holder = null;
            Usuario usuario1=null;
            List<Usuario> listaUsuario = new ArrayList<Usuario>();
            for(int i =0; i < jsonArray.length(); i++){
                Usuario usuario = new Usuario();
                holder = jsonArray.getJSONObject(i);
                usuario.setIdClienteDispositivo(holder.getInt("Id_ClienteDispositivo"));
                usuario.setNomeClienteDispositivo(holder.getString("Nome_ClienteDispositivo"));
                usuario.setEmailClienteDispositivo(holder.getString("Email_ClienteDispositivo"));
                usuario.setTelefoneClienteDispositivo(holder.getString("Telefone_ClienteDispositivo"));
                usuario.setDataNascimentoClienteDispositivo(DatasUtils.getStringDate(holder.getString("DataNascimento_ClienteDispositivo")));
                usuario.setSexoClienteDispositivo(holder.getString("Sexo_ClienteDispositivo"));
                usuario.setSenhaClienteDispositivo("Senha_ClienteDispositivo");
                usuario.setFabricanteHardwareClienteDispositivo("FabricanteHardware_ClienteDispositivo");
                usuario.setModeloHardwareClienteDispositivo("ModeloHardware_ClienteDispositivo");
                usuario.setVersaoHardwareClienteDispositivo("VersaoHardware_ClienteDispositivo");
                usuario.setSoDispositivoClienteDispositivo("SODispositivo_ClienteDispositivo");
                usuario.setIdDispositivoClienteDispositivo("IDDispositivo_ClienteDispositivo");
                usuario.setExcluidoClienteDispositivo(holder.getBoolean("Excluido_ClienteDispositivo"));
                usuario.setDataAlteracaoClienteDispositivo(DatasUtils.getStringDate(holder.getString("DataAlteracao_ClienteDispositivo")));
                usuario.setDataCadastroClienteDispositivo(DatasUtils.getStringDate(holder.getString("DataCadastro_ClienteDispositivo")));
                listaUsuario.add(usuario);
                return listaUsuario.get(i);
            }
            return usuario1;

        }catch(JSONException e){
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
            return null;
        }
    }
    public static List<PreferenciaProduto> jsonToPreferenciaProdutoListChar(String json){
        try{
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject;
            List<CharSequence> charListaPreferenciaProduto = new ArrayList<CharSequence>();
            List<PreferenciaProduto> listaPreferenciaProduto = new ArrayList<PreferenciaProduto>();

            for(int i = 0; i < jsonArray.length(); i++){
                PreferenciaProduto preferenciaProduto = new PreferenciaProduto();
                jsonObject = jsonArray.getJSONObject(i);

                preferenciaProduto.setIdPreferenciaProduto(jsonObject.getString("id"));
                preferenciaProduto.setNome(jsonObject.getString("nome"));
                preferenciaProduto.setIdGrupo(jsonObject.getInt("id_grupo"));
                preferenciaProduto.setSelecionado(false);
                charListaPreferenciaProduto.add(jsonObject.getString("nome"));
                listaPreferenciaProduto.add(preferenciaProduto);
            }
            return listaPreferenciaProduto;
        }catch(JSONException e){
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
            return null;
        }
    }
    public static List<Mesa> jsonToMesaList(String strJson) {
        JSONArray jsArray = null;
        JSONObject holder = null;
        List<Mesa> listaMesa = null;
        try {
            jsArray = new JSONArray(strJson);
            listaMesa = new ArrayList<Mesa>();
            for (int i = 0; i < jsArray.length(); i++) {
                Mesa mesa = new Mesa();
                holder = jsArray.getJSONObject(i);
                mesa.setIDEstabelecimento(holder.getString(MESA_ID_ESTABELECIMENTO));
                mesa.setId(holder.getInt(MESA_ID));
                mesa.setNome(holder.getString(MESA_NOME));
                mesa.setDisponibilidade(holder.getString(MESA_DISPONIBILIDADE));
                mesa.setIdSolicitacaoAutorizacao(holder.getInt(MESA_ID_SOLICITACAO_AUTORIZACAO));
                mesa.setNumeroComanda(holder.getInt(MESA_NUMERO_COMANDA));
                mesa.setDataAbertura(holder.getString(MESA_DATA_ABERTURA));
                mesa.setidUsuarioDispositivo(holder.getInt(MESA_ID_USUARIO_DISPOSITIVO));
                listaMesa.add(mesa);
            }
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return listaMesa;
    }
    public static String movimentoMesaTojson(MovimentoMesa mov) {
        JSONObject holder = new JSONObject();
        try {
            holder.put(MOVIMENTO_MESA_ID_MESA, mov.getIdMesa());
            holder.put(MOVIMENTO_MESA_ID_ESTABELECIMENTO, mov.getIdEstabelecimento());
            holder.put(MOVIMENTO_MESA_DATA,DateUtil.getDateString(mov.getData(), "yyyy/MM/dd hh:mm"));
            holder.put(MOVIMENTO_MESA_NUMERO_DA_COMANDA, mov.getNumComanda());
            holder.put(MOVIMENTO_MESA_DATA_DO_FECHAMENTO, DateUtil.getDateString(mov.getDataFechamento(), "yyyy/MM/dd hh:mm"));
            holder.put(MOVIMENTO_MESA_ID_USUARIO_DISPOSITIVO, mov.getIdUsuarioDispositivo());
            return holder.toString();
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
            return null;
        }
    }
    public static MovimentoMesa jsonToMovimentoMesa(String strJson) {
        JSONObject holder = null;
        MovimentoMesa movimentoMesa = null;
        try {
            holder = new JSONObject(strJson);
            movimentoMesa = new MovimentoMesa();
            movimentoMesa.setIdMesa(holder.getInt(MOVIMENTO_MESA_ID_MESA));
            movimentoMesa.setIdEstabelecimento(holder.getInt(MOVIMENTO_MESA_ID_ESTABELECIMENTO));
            movimentoMesa.setData(DateUtil.getWebApiDate(holder.getString(MOVIMENTO_MESA_DATA)));
            movimentoMesa.setNumComanda(holder.getInt(MOVIMENTO_MESA_NUMERO_DA_COMANDA));
            movimentoMesa.setDataFechamento(DateUtil.getWebApiDate(holder.getString(holder.getString(MOVIMENTO_MESA_DATA_DO_FECHAMENTO))));
            movimentoMesa.setIdUsuarioDispositivo(holder.getInt(MOVIMENTO_MESA_ID_USUARIO_DISPOSITIVO));
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return movimentoMesa;
    }
    public static String fechamentoContaToJson(FechamentoConta fechamentoConta){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(FECHAMENTO_CONTA_ID, fechamentoConta.getId());
            jsonObject.put(FECHAMENTO_CONTA_ID_GARCOM,fechamentoConta.getIdUsuario());
            jsonObject.put(FECHAMENTO_CONTA_ID_ESTABELECIMENTO, fechamentoConta.getIdEstabelecimento());
            jsonObject.put(FECHAMENTO_CONTA_ID_MESA, fechamentoConta.getIdMesa());
            jsonObject.put(FECHAMENTO_CONTA_DATA_CONTA, DateUtil.getWebApiDateString(fechamentoConta.getDataConta()));
            jsonObject.put(FECHAMENTO_CONTA_NUMERO_COMANDA, fechamentoConta.getNumeroComanda());
            jsonObject.put(FECHAMENTO_CONTA_ID_USUARIO_DISPOSITIVO, fechamentoConta.getIdUsuarioDispositivo());
            jsonObject.put(FECHAMENTO_CONTA_DATA_SOLICITACAO, fechamentoConta.getDataSolicitacao());
            jsonObject.put(FECHAMENTO_CONTA_SEQUENCIA, fechamentoConta.getSequencia());
            jsonObject.put(FECHAMENTO_CONTA_PARCIAL, fechamentoConta.isParcial());
            jsonObject.put(FECHAMENTO_CONTA_ID_FORMA_PAGAMENTO, fechamentoConta.getIdFormaPagamento());
            jsonObject.put(FECHAMENTO_CONTA_TROCO_PARA, fechamentoConta.getTrocoPara());
            jsonObject.put(FECHAMENTO_CONTA_PESSOA_PARA_RATEIO, fechamentoConta.getPessoasParaRateio());
            jsonObject.put(FECHAMENTO_CONTA_DATA_AUTORIZACAO, fechamentoConta.getDataAutorizacao());
            jsonObject.put(FECHAMENTO_CONTA_AUTORIZADO_POR, fechamentoConta.getAutorizadoPor());
            jsonObject.put(FECHAMENTO_CONTA_DATA_NEGACAO, fechamentoConta.getDataNegacao());
            jsonObject.put(FECHAMENTO_CONTA_NEGADO_POR, fechamentoConta.getNegadoPor());
            jsonObject.put(FECHAMENTO_CONTA_SITUACAO_ATUAL, null);
            return jsonObject.toString();
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
            return null;
        }
    }
    public static String movimentoSolicitacaoAutorisacaoToJson(MovimetoSolicitacaoAutorizacao mov) {
        JSONObject holder = new JSONObject();
        String stringJson = "";
        try {
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID, mov.getId());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_ESTABELECIMENTO, mov.getIdEstabelecimento());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_MESA, mov.getIdMesa());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_SOLICITACAO, mov.getDataSolicitacao());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_NUMERO_SOLICITACAO, mov.getNumero());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_AUTORIZACAO, mov.getDataAutorizacao());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_AUTORIZADO_POR, mov.getAutorizadoPor());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_NEGACAO, mov.getDataNegacao());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_NEGADO_POR, mov.getNegadoPor());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_USUARIO_DISPOSITIVO, mov.getIdUsuarioDispositivo());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_ABERTURA, mov.getDataAbertura());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_NUMERO_COMANDA, mov.getNumComanda());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_GARCOM,mov.getIdUsuario());
            holder.put(MOVIMENTO_SOLICITACAO_AUTORIZACAO_CODIGO_AUTORIZACAO_MOV_SOLICITACAO_AUTORIZACAO,mov.getCodigoAutorizacaoMoviSolicitacaoAutorizacao());
            stringJson = holder.toString();
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return stringJson;
    }
    public static MovimetoSolicitacaoAutorizacao jsonToMovimentoSolicitacaoAutorisacao(String stringJson){
        JSONObject jsonObject = null;
        MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao = null;
        try {
            jsonObject = new JSONObject(stringJson);
            movimetoSolicitacaoAutorizacao = new MovimetoSolicitacaoAutorizacao();
            movimetoSolicitacaoAutorizacao.setId(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID));
            movimetoSolicitacaoAutorizacao.setIdEstabelecimento(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_ESTABELECIMENTO));
            movimetoSolicitacaoAutorizacao.setIdMesa(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_MESA));
            movimetoSolicitacaoAutorizacao.setDataSolicitacao(jsonObject.getString(MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_SOLICITACAO));
            movimetoSolicitacaoAutorizacao.setDataAutorizacao(jsonObject.getString(MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_AUTORIZACAO));
            movimetoSolicitacaoAutorizacao.setNumero(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_NUMERO_SOLICITACAO));
            movimetoSolicitacaoAutorizacao.setAutorizadoPor(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_AUTORIZADO_POR));
            movimetoSolicitacaoAutorizacao.setDataNegacao(jsonObject.getString(MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_NEGACAO));
            movimetoSolicitacaoAutorizacao.setNegadoPor(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_NEGADO_POR));
            movimetoSolicitacaoAutorizacao.setIdUsuarioDispositivo(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_ID_USUARIO_DISPOSITIVO));
            movimetoSolicitacaoAutorizacao.setDataAbertura(jsonObject.getString(MOVIMENTO_SOLICITACAO_AUTORIZACAO_DATA_ABERTURA));
            movimetoSolicitacaoAutorizacao.setNumComanda(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_NUMERO_COMANDA));
            movimetoSolicitacaoAutorizacao.setCodigoAutorizacaoMoviSolicitacaoAutorizacao(jsonObject.getInt(MOVIMENTO_SOLICITACAO_AUTORIZACAO_CODIGO_AUTORIZACAO_MOV_SOLICITACAO_AUTORIZACAO));
        }catch (JSONException e){
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return movimetoSolicitacaoAutorizacao;
    }
    public static Mesa jsonToMesa(String stringJson) {
        JSONArray jsonArray = null;
        JSONObject holder = null;
        Mesa mesa = null;
        try {
            jsonArray = new JSONArray(stringJson);
            for(int i=0; i < jsonArray.length(); i++){
                mesa = new Mesa();
                holder = jsonArray.getJSONObject(i);
                mesa.setId(holder.getInt(MESA_ID));
                mesa.setNumeroComanda(holder.getInt(MESA_NUMERO_COMANDA));
                mesa.setDisponibilidade(holder.getString(MESA_DISPONIBILIDADE));
                mesa.setIDEstabelecimento(holder.getString(MESA_ID_ESTABELECIMENTO));
                mesa.setNomeUsuarioDispositivo(holder.getString(MESA_NOME_USUARIO_DISPOSITIVO));
                mesa.setDataAbertura(holder.getString(MESA_DATA_ABERTURA));
                mesa.setidUsuarioDispositivo(holder.getInt(MESA_ID_USUARIO_DISPOSITIVO));
                mesa.setIdSolicitacaoAutorizacao(holder.getInt(MESA_ID_SOLICITACAO_AUTORIZACAO));
                mesa.setNome(holder.getString(MESA_NOME));
            }
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return mesa;
    }
    public static RetornoPostFechamento jsonRetornoPost(String stringJson){
        JSONObject jsonObject = null;
        RetornoPostFechamento retornoPostFechamento = null;
        try{
            jsonObject = new JSONObject(stringJson);
            retornoPostFechamento = new RetornoPostFechamento();
            retornoPostFechamento.setDataConta(DatasUtils.getStringDate(jsonObject.getString(FECHAMENTO_DATA_CONTA)));
            retornoPostFechamento.setDataSolicitacao(DatasUtils.getStringDate(jsonObject.getString(FECHAMENTO_DATA_SOLICITACAO)));
            retornoPostFechamento.setIdMesa(jsonObject.getInt(FECHAMENTO_ID_MESA));
            retornoPostFechamento.setNegadoPor(jsonObject.getInt(FECHAMENTO_NEGADO_POR));
            retornoPostFechamento.setParcial(jsonObject.getBoolean(FECHAMENTO_PARCIAL));
            retornoPostFechamento.setId(jsonObject.getInt(FECHAMENTO_ID));
            retornoPostFechamento.setPessoasParaRateio(jsonObject.getInt(FECHAMENTO_PESSOAS_PARA_RATEIO));
            retornoPostFechamento.setNumeroComanda(jsonObject.getInt(FECHAMENTO_NUMERO_DA_COMANDA));
            retornoPostFechamento.setIdEstabelecimento(jsonObject.getInt(FECHAMENTO_ID_ESTABELECIMENTO));
            retornoPostFechamento.setIdFormaDePagamento(jsonObject.getInt(FECHAMENTO_ID_FORMA_PAGAMENTO));
            retornoPostFechamento.setIdUsuarioDispositivo(jsonObject.getInt(FECHAMENTO_ID_USUARIO_DISPOSITIVO));
            retornoPostFechamento.setSequencia(jsonObject.getInt(FECHAMENTO_SEQUENCIA));
            retornoPostFechamento.setTroco(jsonObject.getDouble(FECHAMENTO_TROCO_PARA));
            retornoPostFechamento.setIdGarcom(jsonObject.getInt(FECHAMENTO_ID_GARCOM));
            retornoPostFechamento.setAutorizadoPor(jsonObject.getInt(FECHAMENTO_AUTORIZADO_POR));
        }catch(JSONException e){
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return retornoPostFechamento;
    }
    public static List<Estabelecimento> jsonToEstabelecimentoList(String strJson) {
        JSONArray jsArray = null;
        JSONObject holder = null;
        List<Estabelecimento> lstEstabelecimentos = null;
        try {
            jsArray = new JSONArray(strJson);
            lstEstabelecimentos = new ArrayList<Estabelecimento>();
            for (int i = 0; i < jsArray.length(); i++) {
                Estabelecimento estabelecimento = new Estabelecimento();
                holder = jsArray.getJSONObject(i);
                estabelecimento.setCodigo(holder.getInt(ESTABELECIMENTO_CODIGO));
                estabelecimento.setRazaoSocial(holder.getString(ESTABELECIMENTO_RAZAO_SOCIAL));
                estabelecimento.setNomeFantasia(holder.getString(ESTABELECIMENTO_NOME_FANTASIA));
                estabelecimento.setNomeImagemEstabelecimento(holder.getString(ESTABELECIMENTO_NOME_IMAGEM_ESTABELECIMENTO));
                estabelecimento.setTaxaServico(holder.getDouble(TAXA_SERVICO));
                lstEstabelecimentos.add(estabelecimento);
            }
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return lstEstabelecimentos;
    }
    public static ArrayList<Produto> jsonToProdutoList(String strJson) {
        JSONArray jsArray = null;
        JSONObject jsonObject;
        ArrayList<Produto> listaProdutos = null;
        try {
            jsArray = new JSONArray(strJson);
            listaProdutos = new ArrayList<Produto>();
            for (int i = 0; i < jsArray.length(); i++) {
                Produto produto = new Produto();
                jsonObject = jsArray.getJSONObject(i);
                produto.setIdProdutoEstabelecimento(jsonObject.getInt(ID_ESTABELECIMENTO_PRODUTO_ESTABELECIMENTO));
                produto.setId(jsonObject.getInt(ID_PRODUTO_ESTABELECIMENTO));
                produto.setNome(jsonObject.getString(NOME_PRODUTO_ESTABELECIMENTO));
                produto.setCaracteristicas(jsonObject.getString(CARACTERISTICA_PRODUTO_ESTABELECIMENTO));
                produto.setUnidade(jsonObject.getString(UNIDADE_PRODUTO_ESTABELECIMENTO));
                produto.setPreco(jsonObject.getDouble(PRECO_VENDA_PRODUTO_ESTABELECIMENTO));
                produto.setIdSincronizadoComProduto(jsonObject.getInt(ID_PRODUTO_SINCRONIZADO_PRODUTO_ESTABELECIMENTO));
                produto.setIdGrupo(jsonObject.getInt(ID_GRUPO_PRODUTO_ESTABELECIMENTO));
                produto.setNomeGrupo(jsonObject.getString(NOME_GRUPO));
                produto.setNomeImagem(jsonObject.getString(NOME_IMAGEM_PRODUTO_ESTABELECIMENTO));
                produto.setCozinha(jsonObject.getBoolean(COZINHA_PRODUTO_ESTABELECIMENTO));
                produto.setBarman(jsonObject.getBoolean(BARMAN_PRODUTO_ESTABELECIMENTO));
                listaProdutos.add(produto);
            }
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return listaProdutos;
    }
	public static String movimentoProdutoToJSon(MovimentoProduto mov) {
		JSONObject holder = new JSONObject();
		try {
			holder.put(MOVIMENTO_PRODUTO_ID_ESTABELECIMENTO, mov.getIdEstabelecimento());
			holder.put(MOVIMENTO_PRODUTO_ID_MESA, mov.getIdMesa());
			holder.put(MOVIMENTO_PRODUTO_DATA_DA_ABERTURA, mov.getDataAbertura());
			holder.put(MOVIMENTO_PRODUTO_NUMERO_DA_COMANDA, mov.getNumComanda());
			holder.put(MOVIMENTO_PRODUTO_DATA_DO_MOVIMENTO, mov.getDataMovimento());
			holder.put(MOVIMENTO_PRODUTO_ID_PRODUTO, mov.getIdProduto());
			holder.put(MOVIMENTO_PRODUTO_ID_VALOR_UNITARIO, mov.getValorUnitario());
			holder.put(MOVIMENTO_PRODUTO_ID_QUANTIDADE, mov.getQuantidade());
			holder.put(MOVIMENTO_PRODUTO_ID_USUARIO_MOVEL, mov.getIdUsuarioMovel());
			holder.put(MOVIMENTO_PRODUTO_DETALHE, mov.getDetalhe());
			holder.put(MOVIMENTO_PRODUTO_ESTABELECIMENTO_CIENTE, mov.getEstabelecimentoCiente());
			holder.put(MOVIMENTO_PRODUTO_PRODUCAO_INICIADA, mov.getProducaoIniciada());
			holder.put(MOVIMENTO_PRODUTO_PRODUCAO_CONCLUIDA, mov.getProducaoConcluida());
			holder.put(MOVIMENTO_PRODUTO_GARCOM_ACIONADO, mov.getGarConAcionado());
			holder.put(MOVIMENTO_PRODUTO_CANCELADO_USUARIO, mov.getCanceladoUsuario());
			holder.put(MOVIMENTO_PRODUTO_CANCELADO_ESTABELECIMENTO,mov.getCanceladoEstabelecimento());
			holder.put(MOVIMENTO_PRODUTO_ENTREGUE_NA_MESA, mov.getEntregueNaMesa());
			holder.put(MOVIMENTO_PRODUTO_NOME, mov.getNomeProduto());
			holder.put(MOVIMENTO_PRODUTO_NOME_DA_IMAGEM, mov.getNomeImagem());
            holder.put(MOVIMENTO_PRODUTO_ESTABELECIMENTO_AUTORIZADO_GARCON_MOV_PRODUTO,mov.isAutorizadoGarcomMovProduto());
			return holder.toString();
		} catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
			return null;
		}
	}
    public static String usuarioDispositivoToJson(Usuario usuario){
        JSONObject holder = null;
        try {
            holder = new JSONObject();
            holder.put(ID_CLIENTE_DISPOSITIVO,usuario.getIdClienteDispositivo());
            holder.put(NOME_CLIENTE_DISPOSITIVO,usuario.getNomeClienteDispositivo());
            holder.put(EMAIL_CLIENTE_DISPOSITIVO,usuario.getEmailClienteDispositivo());
            holder.put(TELEFONE_CLIENTE_DISPOSITIVO,usuario.getTelefoneClienteDispositivo());
            holder.put(DATA_NASCIMENTO_CLIENTE_DISPOSITIVO,DatasUtils.getDateString(usuario.getDataNascimentoClienteDispositivo()));
            holder.put(SEXO_CLIENTE_DISPOSITIVO,usuario.getSexoClienteDispositivo());
            holder.put(SENHA_CLIENTE_DISPOSITIVO,usuario.getSenhaClienteDispositivo());
            holder.put(FABRICANTE_HARDWARE_CLIENTE_DISPOSITIVO,usuario.getFabricanteHardwareClienteDispositivo());
            holder.put(MODELO_HARDWARE_CLIENTE_DISPOSITIVO,usuario.getModeloHardwareClienteDispositivo());
            holder.put(VERSAO_HARDWARE_CLIENTE_DISPOSITIVO,usuario.getVersaoHardwareClienteDispositivo());
            holder.put(SO_DISPOSITIVO_CLIENTE_DISPOSITIVO,usuario.getSoDispositivoClienteDispositivo());
            holder.put(ID_DISPOSITIVO_CLIENTE_DISPOSITIVO,usuario.getIdDispositivoClienteDispositivo());
            holder.put(EXCLUIDO_CLIENTE_DISPOSITIVO, usuario.isExcluidoClienteDispositivo());
            holder.put(DATA_ALTERACAO_CLIENTE_DISPOSITIVO,DatasUtils.getDateString(usuario.getDataAlteracaoClienteDispositivo()));
            holder.put(DATA_CADASTRO_CLIENTE_DISPOSITIVO,DatasUtils.getDateString(usuario.getDataCadastroClienteDispositivo()));
            holder.put(ID_FACEBBOK_CLIENTE_DISPOSITIVO,usuario.getIdFacebookClienteDispositivo());
            holder.put(URI_IMAGEM_CLIENTE_DISPOSITIVO,usuario.getUriImagemClienteDispositivo());
        }catch (JSONException e){
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return holder.toString();
    }
    public static Usuario jsonToUsuario(String stringJson){
        Usuario usuario = null;
        try {
            usuario = new Usuario();
            JSONObject holder = new JSONObject(stringJson);
            usuario.setIdClienteDispositivo(holder.getInt(ID_CLIENTE_DISPOSITIVO));
            usuario.setNomeClienteDispositivo(holder.getString(NOME_CLIENTE_DISPOSITIVO));
            usuario.setEmailClienteDispositivo(holder.getString(EMAIL_CLIENTE_DISPOSITIVO));
            usuario.setTelefoneClienteDispositivo(holder.getString(TELEFONE_CLIENTE_DISPOSITIVO));
            usuario.setDataNascimentoClienteDispositivo(DatasUtils.getStringDate(holder.getString(DATA_NASCIMENTO_CLIENTE_DISPOSITIVO)));
            usuario.setSexoClienteDispositivo(holder.getString(SEXO_CLIENTE_DISPOSITIVO));
            usuario.setSenhaClienteDispositivo(SENHA_CLIENTE_DISPOSITIVO);
            usuario.setFabricanteHardwareClienteDispositivo(FABRICANTE_HARDWARE_CLIENTE_DISPOSITIVO);
            usuario.setModeloHardwareClienteDispositivo(MODELO_HARDWARE_CLIENTE_DISPOSITIVO);
            usuario.setVersaoHardwareClienteDispositivo(VERSAO_HARDWARE_CLIENTE_DISPOSITIVO);
            usuario.setSoDispositivoClienteDispositivo(SO_DISPOSITIVO_CLIENTE_DISPOSITIVO);
            usuario.setIdDispositivoClienteDispositivo(ID_DISPOSITIVO_CLIENTE_DISPOSITIVO);
            usuario.setExcluidoClienteDispositivo(holder.getBoolean(EXCLUIDO_CLIENTE_DISPOSITIVO));
            usuario.setDataAlteracaoClienteDispositivo(DatasUtils.getStringDate(holder.getString(DATA_ALTERACAO_CLIENTE_DISPOSITIVO)));
            usuario.setDataCadastroClienteDispositivo(DatasUtils.getStringDate(holder.getString(DATA_CADASTRO_CLIENTE_DISPOSITIVO)));
            usuario.setIdFacebookClienteDispositivo(holder.getInt(ID_FACEBBOK_CLIENTE_DISPOSITIVO));
            usuario.setUriImagemClienteDispositivo(holder.getString(URI_IMAGEM_CLIENTE_DISPOSITIVO));
        }catch (JSONException e){
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return usuario;
    }
    public static String movimentoProdutoToJSon(List<MovimentoProduto> lstMov) {
        JSONArray jsonArray = new JSONArray();
        JSONObject holder;
        try {
            for (MovimentoProduto mov : lstMov) {
                holder = new JSONObject();
                holder.put(MOVIMENTO_PRODUTO_ID_ESTABELECIMENTO, mov.getIdEstabelecimento());
                holder.put(MOVIMENTO_PRODUTO_ID_MESA, mov.getIdMesa());
                holder.put(MOVIMENTO_PRODUTO_DATA_DA_ABERTURA, mov.getDataAbertura());
                holder.put(MOVIMENTO_PRODUTO_NUMERO_DA_COMANDA, mov.getNumComanda());
                holder.put(MOVIMENTO_PRODUTO_DATA_DO_MOVIMENTO, mov.getDataMovimento());
                holder.put(MOVIMENTO_PRODUTO_ID_PRODUTO, mov.getIdProduto());
                holder.put(MOVIMENTO_PRODUTO_ID_VALOR_UNITARIO, mov.getValorUnitario());
                holder.put(MOVIMENTO_PRODUTO_ID_QUANTIDADE, mov.getQuantidade());
                holder.put(MOVIMENTO_PRODUTO_ID_USUARIO_MOVEL, mov.getIdUsuarioMovel());
                holder.put(MOVIMENTO_PRODUTO_DETALHE, mov.getDetalhe());
                holder.put(MOVIMENTO_PRODUTO_ESTABELECIMENTO_CIENTE,mov.getEstabelecimentoCiente());
                holder.put(MOVIMENTO_PRODUTO_PRODUCAO_INICIADA, mov.getProducaoIniciada());
                holder.put(MOVIMENTO_PRODUTO_PRODUCAO_CONCLUIDA, mov.getProducaoConcluida());
                holder.put(MOVIMENTO_PRODUTO_GARCOM_ACIONADO, mov.getGarConAcionado());
                holder.put(MOVIMENTO_PRODUTO_CANCELADO_USUARIO, mov.getCanceladoUsuario());
                holder.put(MOVIMENTO_PRODUTO_CANCELADO_ESTABELECIMENTO,mov.getCanceladoEstabelecimento());
                holder.put(MOVIMENTO_PRODUTO_ENTREGUE_NA_MESA, mov.getEntregueNaMesa());
                holder.put(MOVIMENTO_PRODUTO_NOME, mov.getNomeProduto());
                holder.put(MOVIMENTO_PRODUTO_NOME_DA_IMAGEM, mov.getNomeImagem());
                jsonArray.put(holder);
            }
            return jsonArray.toString();

        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
            return null;
        }
    }
    public static MovimentoProduto jsonToMovimentoProduto(String strJson) {
        JSONObject holder = null;
        MovimentoProduto movimentoProduto = null;
        try {
            holder = new JSONObject(strJson);
            movimentoProduto = new MovimentoProduto();
            movimentoProduto.setIdEstabelecimento(holder.getInt(MOVIMENTO_PRODUTO_ID_ESTABELECIMENTO));
            movimentoProduto.setIdMesa(holder.getInt(MOVIMENTO_PRODUTO_ID_MESA));
            movimentoProduto.setDataAbertura(holder.getString(MOVIMENTO_PRODUTO_DATA_DA_ABERTURA));
            movimentoProduto.setNumComanda(holder.getInt(MOVIMENTO_PRODUTO_NUMERO_DA_COMANDA));
            movimentoProduto.setDataMovimento(DatasUtils.getStringDate(holder.getString(MOVIMENTO_PRODUTO_DATA_DO_MOVIMENTO)));
            movimentoProduto.setIdProduto(holder.getInt(MOVIMENTO_PRODUTO_ID_PRODUTO));
            movimentoProduto.setValorUnitario(holder.getDouble(MOVIMENTO_PRODUTO_ID_VALOR_UNITARIO));
            movimentoProduto.setQuantidade(holder.getInt(MOVIMENTO_PRODUTO_ID_QUANTIDADE));
            movimentoProduto.setIdUsuarioMovel(holder.getLong(MOVIMENTO_PRODUTO_ID_USUARIO_MOVEL));
            movimentoProduto.setDetalhe(holder.getString(MOVIMENTO_PRODUTO_DETALHE));
            movimentoProduto.setEstabelecimentoCiente(holder.getString(MOVIMENTO_PRODUTO_ESTABELECIMENTO_CIENTE));
            movimentoProduto.setProducaoIniciada(holder.getString(MOVIMENTO_PRODUTO_PRODUCAO_INICIADA));
            movimentoProduto.setProducaoConcluida(holder.getString(MOVIMENTO_PRODUTO_PRODUCAO_CONCLUIDA));
            movimentoProduto.setGarConAcionado(MOVIMENTO_PRODUTO_GARCOM_ACIONADO);
            movimentoProduto.setCanceladoUsuario(holder.getString(MOVIMENTO_PRODUTO_CANCELADO_USUARIO));
            movimentoProduto.setCanceladoEstabelecimento(holder.getString(MOVIMENTO_PRODUTO_CANCELADO_ESTABELECIMENTO));
            movimentoProduto.setEntregueNaMesa(holder.getString(MOVIMENTO_PRODUTO_ENTREGUE_NA_MESA));
        } catch (JSONException e) {
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
        }
        return movimentoProduto;
    }
    public static List<MovimentoProduto> jsonToMovimentoProdutoListFechamento(String strJson) {
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;
        List<MovimentoProduto> listaMovimentoProduto = null;
        try {
            jsonArray = new JSONArray(strJson);
            jsonObject = new JSONObject();
            listaMovimentoProduto = new ArrayList<MovimentoProduto>();
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                MovimentoProduto movimentoProduto = new MovimentoProduto();
                movimentoProduto.setNomeProduto(jsonObject.getString(ACOMPANHAMENTO_NOME_PRODUTO_ESTABELECIMENTO));
                movimentoProduto.setQuantidade(jsonObject.getInt(ACOMPANHAMENTO_QUANTIDADE));
                movimentoProduto.setValorUnitario(jsonObject.getDouble(ACOMPANHAMENTO_VALOR_UNITARIO));
                movimentoProduto.setValorTotal(jsonObject.getDouble(ACOMPANHAMENTO_TOTAL));
                listaMovimentoProduto.add(movimentoProduto);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Erro: " + e.getMessage() + "causa: " + e.getCause());
        }
        return listaMovimentoProduto;
    }
    public static List<MovimentoProduto> jsonToMovimentoProdutoList(String strJson) {
        JSONArray jsonArray = null;
        JSONObject jsonObject = null;
        List<MovimentoProduto> listaMovimentoProduto = null;
        try {
            jsonArray = new JSONArray(strJson);
            jsonObject = new JSONObject();
            listaMovimentoProduto = new ArrayList<MovimentoProduto>();
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                MovimentoProduto movimentoProduto = new MovimentoProduto();
                movimentoProduto.setNomeProduto(jsonObject.getString(MOVIMENTO_PRODUTO_FECHAMENTO_NOME_PRODUTO_ESTABELECIMENTO));
                movimentoProduto.setQuantidade(jsonObject.getInt(MOVIMENTO_PRODUTO_FECHAMENTO_QUANTIDADE));
                movimentoProduto.setValorUnitario(jsonObject.getDouble(MOVIMENTO_PRODUTO_FECHAMENTO_VALOR_UNITARIO));
                movimentoProduto.setValorTotal(jsonObject.getDouble(MOVIMENTO_PRODUTO_FECHAMENTO_TOTAL));
                movimentoProduto.setDataMovimento(DatasUtils.getStringDate(jsonObject.getString(ACOMPANHAMENTO_DATA_MOVIMENTO)));
                listaMovimentoProduto.add(movimentoProduto);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Erro: " + e.getMessage() + "causa: " + e.getCause());
        }
        return listaMovimentoProduto;
    }
    public static List<Grupo> jsonToGrupoProdutoList(String stringJson){
        List<Grupo> listaGrupo;
        try{
            JSONArray jsonArray;
            JSONObject holder;
            jsonArray = new JSONArray(stringJson);
            listaGrupo = new ArrayList<Grupo>();
            for(int i = 0; i < jsonArray.length(); i++){
                Grupo grupo = new Grupo();
                holder = jsonArray.getJSONObject(i);
                grupo.setIdGrupoProduto(holder.getInt(ID_GRUPO_PRODUTO));
                grupo.setNomeGrupoProduto(holder.getString(NOME_GRUPO_PRODUTO));
                grupo.setNomeImagemGrupoProduto(holder.getString(NOME_IMAGEM_GRUPO_PRODUTO));
                listaGrupo.add(grupo);
            }
        }catch (JSONException e){
            Log.e(TAG,"Erro: " + e.getMessage() + "causa: "+ e.getCause());
            listaGrupo = null;
        }
        return listaGrupo;
    }
}
