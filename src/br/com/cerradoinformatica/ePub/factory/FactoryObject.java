package br.com.cerradoinformatica.ePub.factory;

import br.com.cerradoinformatica.ePub.model.Estabelecimento;

public class FactoryObject {
	
	private static FactoryObject instance;
	public Estabelecimento estabelecimento;
	
	
	private FactoryObject() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}



	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}



	public static FactoryObject getInstance(){
		if(instance==null){
			instance=new FactoryObject();
		}
		return instance;
	}

}
