package br.com.cerradoinformatica.ePub.funcionalidades;

public interface Transacao {
	public void executar() throws Exception;
	// atualiza a view sincronizada com a thred da interface
	public void atualizarView();

}
