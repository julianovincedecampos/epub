package br.com.cerradoinformatica.ePub.funcionalidades;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by juliano on 04/02/14.
 */
public class DatasUtils {

    private static  final String  TAG = "DatasUtils";

    public static String getDateString(Date date){
        if(date==null)
            return "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return format.format(date);
    }
    public static Date getStringDate(String dateString) {
        SimpleDateFormat format = null;
        Date date = null;
        try {
            format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            date = format.parse(dateString);
        } catch (Exception e) {
            Log.e(TAG,"Erro = " + e.getMessage());
        }
        return date;
    }
    public static Date getStringDateCadastro(String dataString){
        Date date = null;
        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse(dataString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
    public static String getDataHora(Date date){
        if(date==null)
            return "";
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        return format.format(date);
    }
    public static String getDataHoraSemSegundos(Date date){
        if(date==null)
            return "";
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        return format.format(date);
    }
    public static String getDataDiaMesHoraMinuto(Date date){
        if(date==null)
            return "";
        SimpleDateFormat format = new SimpleDateFormat("dd/MM HH:mm");
        return format.format(date);
    }
}
