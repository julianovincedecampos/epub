package br.com.cerradoinformatica.ePub.funcionalidades;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.Toast;


/**
 * Created by juliano on 28/11/13.
 */
public class TestaConnection{
    private Context context;
    private static String TAG = "TestaConnection";

    public boolean verificaConexao(Context context){
        this.context = context;
        try{
            ConnectivityManager connection = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            String Message;//Mensagem que sera usada no Metodo Toast
            if(connection.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()){
                Message = "Conectado a internet 3G! Sua conexão pode ser lenta!";
                Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
                return true;
            }else if(connection.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()){
                Message = "Conectado a Internet WIFI";
                Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
                return true;
            }else{
                //Message = "Não possui a conexão com a internet";
                //Toast.makeText(context, Message, Toast.LENGTH_SHORT).show();
                return false;
            }
        }catch(Exception e){
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e(TAG, "Erro na classe conexão: " + e.getMessage());
            return false;
        }
    }
}
