package br.com.cerradoinformatica.ePub.funcionalidades;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.Usuario;

/**
 * Created by juliano on 08/01/14.
 */
public class GerenciadorAlertDialog {

    private static AlertDialog alertDialog;
    private boolean retorno = false;

    private Context context;

    public GerenciadorAlertDialog(Context context){
        this.context = context;
    }


  /*  public void showAlertDialogComIncones(Context context, String title, String mensagem, Boolean status){

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        //Titulo
        alertDialog.setTitle(title);

        //Mensagem
        alertDialog.setMessage(mensagem);

        if(status != null){
            //incone Dialog
            alertDialog.setIcon((status)? R.drawable.success : R.drawable.fail);

            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            alertDialog.show();
        }
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialog.show();
    }
*/
    public void showAlertDialogSairDoisButtons(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive, String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(1);
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }
    public void showAlertDialogSairUmButton(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive, String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(1);
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }
    public void showAlertDialogMainVerificaVersao(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(1);
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }
    public boolean showAlertDialogMainUsuario(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive){


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                retorno = true;
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
        return retorno;
    }
    public boolean showAlertDialogTelaPrincipalMesaEspecifica(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive, String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                retorno = true;
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(1);
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
        return retorno;
    }
    public boolean showAlertDialogDetalhesFragments(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive, String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                retorno = true;
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(1);
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
        return retorno;
    }
    public boolean showAlertDialogFechamentoActivityGetProduto(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive, String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                retorno = true;
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(1);
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
        return retorno;
    }

    public boolean showAlertDialogPerfil(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                retorno = true;
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
        return retorno;
    }
    public boolean showAlertDialogDrawerLayout(Context context,int name_incone, String titulo, String mensagem, String nomeButtonPositive, String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                retorno = true;
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                retorno = false;
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
        return retorno;
    }
}
