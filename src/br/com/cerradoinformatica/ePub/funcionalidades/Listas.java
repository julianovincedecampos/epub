package br.com.cerradoinformatica.ePub.funcionalidades;

import java.io.Serializable;
import java.util.List;

import br.com.cerradoinformatica.ePub.model.Grupo;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;
import br.com.cerradoinformatica.ePub.model.Produto;

public class Listas implements Serializable{

    /**
     *
     */
    //private static final long serialVersionUID = 3125281298800656073L;

    private  List<Produto> listProduto;
    private List<MovimentoProduto> lstMovimentoProduto;
    private List<Grupo> listGrupo;
    public static String KEY_GRUPO = "listGrupo";
    public static String KEY_PRODUTO = "listProduto";
    public static String KEY_PREFERENCIAS = "listaPreferencias";

    public List<Produto> getListProduto() {
        return listProduto;
    }

    public void setListProduto(List<Produto> listProduto) {
        this.listProduto = listProduto;
    }



    public List<MovimentoProduto> getLstMovimentoProduto() {
        return lstMovimentoProduto;
    }

    public void setLstMovimentoProduto(List<MovimentoProduto> lstMovimentoProduto) {
        this.lstMovimentoProduto = lstMovimentoProduto;
    }

    public List<Grupo> getListGrupo(){
        return this.listGrupo;
    }
    public void setListGrupo(List<Grupo> listGrupo){
        this.listGrupo = listGrupo;
    }

}

