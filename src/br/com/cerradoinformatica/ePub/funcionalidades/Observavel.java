package br.com.cerradoinformatica.ePub.funcionalidades;

import android.os.Bundle;

import java.io.Serializable;
import java.util.Observer;

/**
 * Created by danilo on 27/09/13.
 */
public interface Observavel extends Serializable{
    public void addObserver(Observer o);
    public void removeObserver(Observer o);
    public void notifyObservers(Bundle bundle);
}
