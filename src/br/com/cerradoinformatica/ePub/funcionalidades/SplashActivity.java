package br.com.cerradoinformatica.ePub.funcionalidades;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import br.com.cerradoinformatica.ePub.activity.AcompanhamentoDeContaTelaPrincipalActivity;
import br.com.cerradoinformatica.ePub.activity.R;

/**
 * Created by juliano on 11/03/14.
 */
public class SplashActivity extends Activity {
    // Tempo que a activity vai aparecer em milisegundos
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                //Chamando a Activity principal, no caso AcompanhamentoDeContaTelaPrincipalActivity
                Intent i = new Intent(SplashActivity.this, AcompanhamentoDeContaTelaPrincipalActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
