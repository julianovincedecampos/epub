package br.com.cerradoinformatica.ePub.funcionalidades;

import android.os.Bundle;

/**
 * Created by juliano on 27/09/13.
 */
public interface ObserverImplementacao {

    public void update(Observavel o, Bundle bundle);

}
