package br.com.cerradoinformatica.ePub.funcionalidades;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


public class TransacaoTask extends AsyncTask<Void, Void, Boolean>{
	
	private final Context context;
	private final Transacao transacao;
	private ProgressDialog progresso;
	private Throwable exceptionErro;
	private int aguardMsg;

	public TransacaoTask(Context context, Transacao transacao, int aguardMessage) {
		// TODO Auto-generated constructor stub
		this.context=context;
		this.transacao=transacao;
		this.aguardMsg=aguardMessage;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		abrirProgress();
	}
	@Override
	protected Boolean doInBackground(Void... params) {
		try{
			transacao.executar();
            fecharProgress();
            return true;
		}catch(Exception e){
			this.exceptionErro=e;
			return false;
		}
	}
	@Override
	protected void onPostExecute(Boolean result) {
		// TODO Auto-generated method stub
		if(result){
			transacao.atualizarView();
		}
	}
	public void abrirProgress(){
		try{
			progresso = ProgressDialog.show(context, "", context.getString(aguardMsg));
		}catch(Exception e){
			progresso.dismiss();
		}
	}
	public void fecharProgress(){
		try{
			if(progresso!=null){
				progresso.dismiss();
			}
		}catch(Exception e){
			progresso.dismiss();
		}
	}
}
