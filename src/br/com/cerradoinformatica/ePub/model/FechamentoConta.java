package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;
import java.util.Date;


import java.io.Serializable;
import java.util.Date;

public class FechamentoConta implements Serializable {

    private static final long serialVersionUID = 1L;
    private int id;
    private int idEstabelecimento;
    private int idMesa;
    private Date dataConta;
    private int numeroComanda;
    private long idUsuarioDispositivo;
    private String dataSolicitacao;
    private int sequencia;
    private boolean parcial;
    private int idFormaPagamento;
    private double trocoPara;
    private int pessoasParaRateio;
    private String dataAutorizacao;
    private int autorizadoPor;
    private String dataNegacao;
    private int negadoPor;
    private String situacaoAtual;
    private int idUsuario;

    public int getIdUsuario(){
        return this.idUsuario;
    }
    public void setIdUsuario(int idUsuario){
        this.idUsuario = idUsuario;
    }

    public void setSituacaoAtual(String situacaoAtual){
        this.situacaoAtual = situacaoAtual;
    }
    public String getSituacaoAtual(){
        return this.situacaoAtual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdEstabelecimento() {
        return idEstabelecimento;
    }

    public void setIdEstabelecimento(int idEstabelecimento) {
        this.idEstabelecimento = idEstabelecimento;
    }

    public int getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(int idMesa) {
        this.idMesa = idMesa;
    }

    public Date getDataConta() {
        return dataConta;
    }

    public void setDataConta(Date dataConta) {
        this.dataConta = dataConta;
    }

    public int getNumeroComanda() {
        return numeroComanda;
    }

    public void setNumeroComanda(int numeroComanda) {
        this.numeroComanda = numeroComanda;
    }

    public long getIdUsuarioDispositivo() {
        return idUsuarioDispositivo;
    }

    public void setIdUsuarioDispositivo(long idUsuarioDispositivo) {
        this.idUsuarioDispositivo = idUsuarioDispositivo;
    }

    public String getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(String dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public int getSequencia() {
        return sequencia;
    }

    public void setSequencia(int sequencia) {
        this.sequencia = sequencia;
    }

    public boolean isParcial() {
        return parcial;
    }

    public void setParcial(boolean parcial) {
        this.parcial = parcial;
    }

    public int getIdFormaPagamento() {
        return idFormaPagamento;
    }

    public void setIdFormaPagamento(int idFormaPagamento) {
        this.idFormaPagamento = idFormaPagamento;
    }

    public double getTrocoPara() {
        return trocoPara;
    }

    public void setTrocoPara(double trocoPara) {
        this.trocoPara = trocoPara;
    }

    public int getPessoasParaRateio() {
        return pessoasParaRateio;
    }

    public void setPessoasParaRateio(int pessoasParaRateio) {
        this.pessoasParaRateio = pessoasParaRateio;
    }

    public String getDataAutorizacao() {
        return dataAutorizacao;
    }

    public void setDataAutorizacao(String dataAutorizacao) {
        this.dataAutorizacao = dataAutorizacao;
    }

    public int getAutorizadoPor() {
        return autorizadoPor;
    }

    public void setAutorizadoPor(int autorizadoPor) {
        this.autorizadoPor = autorizadoPor;
    }

    public String getDataNegacao() {
        return dataNegacao;
    }

    public void setDataNegacao(String dataNegacao) {
        this.dataNegacao = dataNegacao;
    }

    public int getNegadoPor() {
        return negadoPor;
    }

    public void setNegadoPor(int negadoPor) {
        this.negadoPor = negadoPor;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
