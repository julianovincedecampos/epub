package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by juliano on 10/04/14.
 */
public class UsuarioHerbalife implements Serializable{

    private int idEstabelecimento;
    private int idMesa;
    private Date dataAbertura;
    private int numeroComanda;
    private boolean logado;
    private int CodigoAutorizacaoMoviSolicitacaoAutorizacao;
    private String nome;

    public int getIdEstabelecimento() {
        return idEstabelecimento;
    }

    public void setIdEstabelecimento(int idEstabelecimento) {
        this.idEstabelecimento = idEstabelecimento;
    }

    public int getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(int idMesa) {
        this.idMesa = idMesa;
    }

    public Date getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(Date dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    public int getNumeroComanda() {
        return numeroComanda;
    }

    public void setNumeroComanda(int numeroComanda) {
        this.numeroComanda = numeroComanda;
    }

    public boolean isLogado() {
        return logado;
    }

    public void setLogado(boolean logado) {
        this.logado = logado;
    }

    public int getCodigoAutorizacaoMoviSolicitacaoAutorizacao() {
        return CodigoAutorizacaoMoviSolicitacaoAutorizacao;
    }

    public void setCodigoAutorizacaoMoviSolicitacaoAutorizacao(int codigoAutorizacaoMoviSolicitacaoAutorizacao) {
        CodigoAutorizacaoMoviSolicitacaoAutorizacao = codigoAutorizacaoMoviSolicitacaoAutorizacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
