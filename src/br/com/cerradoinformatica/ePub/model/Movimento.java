package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;

public abstract class Movimento implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	protected int idEstabelecimento;
	

	protected int idMesa;
	
	

	protected String dataAbertura;
	

	protected int numComanda;

	public int getIdEstabelecimento() {
		return idEstabelecimento;
	}

	public void setIdEstabelecimento(int idEstabelecimento) {
		this.idEstabelecimento = idEstabelecimento;
	}

	public int getIdMesa() {
		return idMesa;
	}

	public void setIdMesa(int idMesa) {
		this.idMesa = idMesa;
	}

	public String getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public int getNumComanda() {
		return numComanda;
	}

	public void setNumComanda(int numComanda) {
		this.numComanda = numComanda;
	}
	
	

}
