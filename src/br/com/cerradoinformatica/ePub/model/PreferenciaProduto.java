package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;

/**
 * Created by juliano on 08/01/14.
 */
public class PreferenciaProduto implements Serializable{

    private String idPreferenciaProduto;
    private String nome;
    private Boolean selecionado;
    private String idsUtilizados;
    private int idGrupo;

    public String getNome(){
        return this.nome;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    public String getIdPreferenciaProduto(){
        return this.idPreferenciaProduto;
    }
    public void setIdPreferenciaProduto(String idPreferenciaProduto){
        this.idPreferenciaProduto = idPreferenciaProduto;
    }
    public Boolean isSelecionado() { return selecionado;}
    public void setSelecionado(Boolean selecionado) {
        this.selecionado = selecionado;
    }

    public String getIdsUtilizados() {
        return idsUtilizados;
    }

    public void setIdsUtilizados(String idsUtilizados) {
        this.idsUtilizados = idsUtilizados;
    }

    public int getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(int idGrupo) {
        this.idGrupo = idGrupo;
    }
}

