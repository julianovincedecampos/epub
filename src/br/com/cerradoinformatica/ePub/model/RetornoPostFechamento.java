package br.com.cerradoinformatica.ePub.model;

import java.util.Date;

/**
 * Created by juliano on 31/03/14.
 */
public class RetornoPostFechamento {

    private int id;
    private int idGarcom;
    private int idMesa;
    private int idEstabelecimento;
    private int idFormaDePagamento;
    private int idUsuarioDispositivo;
    private Date dataConta;
    private Date dataSolicitacao;
    private int negadoPor;
    private boolean parcial;
    private int pessoasParaRateio;
    private int numeroComanda;
    private int sequencia;
    private double troco;
    private int autorizadoPor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdGarcom() {
        return idGarcom;
    }

    public void setIdGarcom(int idGarcom) {
        this.idGarcom = idGarcom;
    }

    public int getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(int idMesa) {
        this.idMesa = idMesa;
    }

    public int getIdEstabelecimento() {
        return idEstabelecimento;
    }

    public void setIdEstabelecimento(int idEstabelecimento) {
        this.idEstabelecimento = idEstabelecimento;
    }

    public int getIdFormaDePagamento() {
        return idFormaDePagamento;
    }

    public void setIdFormaDePagamento(int idFormaDePagamento) {
        this.idFormaDePagamento = idFormaDePagamento;
    }

    public int getIdUsuarioDispositivo() {
        return idUsuarioDispositivo;
    }

    public void setIdUsuarioDispositivo(int idUsuarioDispositivo) {
        this.idUsuarioDispositivo = idUsuarioDispositivo;
    }

    public Date getDataConta() {
        return dataConta;
    }

    public void setDataConta(Date dataConta) {
        this.dataConta = dataConta;
    }

    public Date getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(Date dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    public int getNegadoPor() {
        return negadoPor;
    }

    public void setNegadoPor(int negadoPor) {
        this.negadoPor = negadoPor;
    }

    public boolean getParcial() {
        return parcial;
    }

    public void setParcial(boolean parcial) {
        this.parcial = parcial;
    }

    public int getPessoasParaRateio() {
        return pessoasParaRateio;
    }

    public void setPessoasParaRateio(int pessoasParaRateio) {
        this.pessoasParaRateio = pessoasParaRateio;
    }

    public int getNumeroComanda() {
        return numeroComanda;
    }

    public void setNumeroComanda(int numeroComanda) {
        this.numeroComanda = numeroComanda;
    }

    public int getSequencia() {
        return sequencia;
    }

    public void setSequencia(int sequencia) {
        this.sequencia = sequencia;
    }

    public double getTroco() {
        return troco;
    }

    public void setTroco(double troco) {
        this.troco = troco;
    }

    public int getAutorizadoPor() {
        return autorizadoPor;
    }

    public void setAutorizadoPor(int autorizadoPor) {
        this.autorizadoPor = autorizadoPor;
    }
}
