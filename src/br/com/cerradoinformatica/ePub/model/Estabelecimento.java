package br.com.cerradoinformatica.ePub.model;






public class Estabelecimento {

    private int codigo;
	private String razaoSocial;
	private String nomeFantasia;
    private String nomeImagemEstabelecimento;
    private double taxaServico;

	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getTaxaServico() {
        return taxaServico;
    }

    public void setTaxaServico(double taxaServico) {
        this.taxaServico = taxaServico;
    }

    public String getNomeImagemEstabelecimento() {
        return nomeImagemEstabelecimento;
    }

    public void setNomeImagemEstabelecimento(String nomeImagemEstabelecimento) {
        this.nomeImagemEstabelecimento = nomeImagemEstabelecimento;
    }
}
