package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;
import java.util.Date;

public class MovimentoMesa implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6500180053496927127L;
	
	private int idEstabelecimento;
	private int idMesa;
	private Date data;
	private int numComanda;
	private Date dataFechamento;
	private int idUsuarioDispositivo;
	private String dadtaAutorizacao;
	
	public int getIdEstabelecimento() {
		return idEstabelecimento;
	}
	public void setIdEstabelecimento(int idEstabelecimento) {
		this.idEstabelecimento = idEstabelecimento;
	}
	public int getIdMesa() {
		return idMesa;
	}
	public void setIdMesa(int idMesa) {
		this.idMesa = idMesa;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public int getNumComanda() {
		return numComanda;
	}
	public void setNumComanda(int numComanda) {
		this.numComanda = numComanda;
	}
	public Date getDataFechamento() {
		return dataFechamento;
	}
	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
	public int getIdUsuarioDispositivo() {
		return idUsuarioDispositivo;
	}
	public void setIdUsuarioDispositivo(int idUsuarioDispositivo) {
		this.idUsuarioDispositivo = idUsuarioDispositivo;
	}
	public String getDadtaAutorizacao() {
		return dadtaAutorizacao;
	}
	public void setDadtaAutorizacao(String dadtaAutorizacao) {
		this.dadtaAutorizacao = dadtaAutorizacao;
	}
	
	
	

/*
 * 

	*//**
	 * 
	 *//*
	

	public MovimentoMesa() {
		// TODO Auto-generated constructor stub
		setDataAbertura("2013/01/15 11:22:33");
		setNumComanda(3);
		
	}		
	
	@JsonProperty(value="IdEstabelecimento")
	protected int idEstabelecimento;
	
	@JsonProperty(value="IdMesa")
	protected int idMesa;
	
	
	@JsonProperty(value="Data_da_Abertura")
	protected String dataAbertura;
	
	@JsonProperty(value="Numero_da_Comanda")
	protected int numComanda;
	
	
	@JsonProperty(value="Data_da_Autorizacao")
	private String dadtaAutorizacao;
	
	@JsonProperty(value="Numero")
	private int numAutorizacao;	

	

	public String getDadtaAutorizacao() {
		return dadtaAutorizacao;
	}
	


	public void setDadtaAutorizacao(String dadtaAutorizacao) {
		this.dadtaAutorizacao = dadtaAutorizacao;
	}

	public int getNumAutorizacao() {
		return numAutorizacao;
	}

	public void setNumAutorizacao(int numAutorizacao) {
		this.numAutorizacao = numAutorizacao;
	}

	public int getIdEstabelecimento() {
		return idEstabelecimento;
	}

	public void setIdEstabelecimento(int idEstabelecimento) {
		this.idEstabelecimento = idEstabelecimento;
	}

	public int getIdMesa() {
		return idMesa;
	}

	public void setIdMesa(int idMesa) {
		this.idMesa = idMesa;
	}

	public String getDataAbertura() {
		return dataAbertura;
	}

	public void setDataAbertura(String dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public int getNumComanda() {
		return numComanda;
	}

	public void setNumComanda(int numComanda) {
		this.numComanda = numComanda;
	}*/

	

	
	
	
	
	

}
