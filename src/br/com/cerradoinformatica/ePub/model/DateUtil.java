package br.com.cerradoinformatica.ePub.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Created by juliano on 16/01/14.
 */
public class DateUtil {

    public static Date getWebApiDate(String dateString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = format.parse(dateString);
            return date;
        } catch (Exception e) {
            return null;
        }
    }

    public static String getWebApiDateString(Date date){
        if(date==null)
            return "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return format.format(date);
    }
    public static String getDataHora(Date date){
        if(date==null)
            return "";
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        return format.format(date);
    }
    public static Date getSQLiteDate(String dateString) throws ParseException{
        // try{
        SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date data;
        // String dateString2 = dateString.replace(" ", "T");

        //Date date2 = formataData.parse("2014-01-10T14:59:32");
        if (!dateString.equalsIgnoreCase("")){
            return   data = formataData.parse(dateString);
        }else{
            return null;
        }
      /* }catch (DataFormatException e){

       }*/
    }

    /*public static String getSQLiteDateString(Date date){
        if(date==null)
            return "";
        SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return formataData.format(date);
    }*/

    public static String getDateString(Date date,String pattern){
        if(date==null)
            return "";
        DateFormat format=new SimpleDateFormat(pattern);
        String teste = format.toString();
        return format.format(date);

    }
}

