package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by juliano on 23/12/13.
 */
public class ListMesas implements Serializable{

    public static  String KEY="listMesas";

    private List<Mesa> mesaList;


    public List<Mesa> getMesaList() {
        return mesaList;
    }

    public void setMesaList(List<Mesa> mesaList) {
        this.mesaList = mesaList;
    }
}