package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;

public class Mesa implements Serializable{

    private String IDEstabelecimento;
    private int id;
    private String nome;
    private String disponibilidade;
    private int idSolicitacaoAutorizacao;
    private int numeroComanda;
    private String dataAbertura;
    private int idUsuarioDispositivo;
    private String nomeUsuarioDispositivo;

    public String getDataAbertura(){return this.dataAbertura;}
    public void setDataAbertura(String dataAbertura){this.dataAbertura = dataAbertura;}

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getIDEstabelecimento() {
        return IDEstabelecimento;
    }
    public void setIDEstabelecimento(String IDEstabelecimento) {
        this.IDEstabelecimento = IDEstabelecimento;
    }
    public String getDisponibilidade() {
        return disponibilidade;
    }
    public void setDisponibilidade(String disponibilidade) {
        this.disponibilidade = disponibilidade;
    }
    public int getIdSolicitacaoAutorizacao(){
        return this.idSolicitacaoAutorizacao;
    }
    public void setIdSolicitacaoAutorizacao(int idSolicitacaoAutorizacao){
        this.idSolicitacaoAutorizacao = idSolicitacaoAutorizacao;
    }
    public int getNumeroComanda(){
        return this.numeroComanda;
    }
    public void setNumeroComanda(int numeroComanda){
        this.numeroComanda = numeroComanda;
    }
    public int getIdUsuarioDispositivo(){
        return this.idUsuarioDispositivo;
    }
    public void setidUsuarioDispositivo(int idUsuarioDispositivo){
        this.idUsuarioDispositivo = idUsuarioDispositivo;
    }
    public String getNomeUsuarioDispositivo(){
        return this.nomeUsuarioDispositivo;
    }
    public void setNomeUsuarioDispositivo(String nomeUsuarioDispositivo){
        this.nomeUsuarioDispositivo = nomeUsuarioDispositivo;
    }
}

