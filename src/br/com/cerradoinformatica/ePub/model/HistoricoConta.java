package br.com.cerradoinformatica.ePub.model;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by juliano on 14/01/14.
 */
public class HistoricoConta implements Serializable {

    private Date data;
    private int idEstabelecimento;
    private String nomeArquivo;

    public Date getDate() {
        return data;
    }

    public void setDate(Date data) {
        this.data = data;
    }

    public int getIdEstabelecimento() {
        return idEstabelecimento;
    }

    public void setIdEstabelecimento(int idEstabelecimento) {
        this.idEstabelecimento = idEstabelecimento;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }
}
