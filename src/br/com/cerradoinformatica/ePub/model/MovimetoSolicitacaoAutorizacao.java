package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;

public class MovimetoSolicitacaoAutorizacao implements Serializable {

    private static final long serialVersionUID = -7273925348053019706L;

    private int id;
    private int idUsuarioDispositivo;
    private int idUsuario;
    private int idEstabelecimento;
    private int idMesa;
    private int idUsuarioWeb = 444;
    private String dataSolicitacao;
    private int autorizadoPor;
    private int codigoAutorizacaoMoviSolicitacaoAutorizacao;
    private boolean usuarioAutorizado;
    private int numComanda;
    private String dataAutorizacao;
    private String dataNegacao;
    private int negadoPor;
    private String dataAbertura;
    private String data;
    private int numero;
    private String qrCode;


    public int getIdUsuario(){
        return this.idUsuario;
    }
    public void setIdUsuario(int idUsuario){
        this.idUsuario = idUsuario;
    }

    public String getDataSolicitacao() {
        return dataSolicitacao;
    }

    public void setDataSolicitacao(String dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    // Get e Set

    public String getDataAutorizacao() {
        return dataAutorizacao;
    }

    public void setDataAutorizacao(String dataAutorizacao) {
        this.dataAutorizacao = dataAutorizacao;
    }

    public String getDataNegacao() {
        return dataNegacao;
    }

    public void setDataNegacao(String dataNegacao) {
        this.dataNegacao = dataNegacao;
    }

    public int getNegadoPor() {
        return negadoPor;
    }

    public void setNegadoPor(int negadoPor) {
        this.negadoPor = negadoPor;
    }

    public String getDataAbertura() {
        return dataAbertura;
    }

    public void setDataAbertura(String dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getIdEstabelecimento() {
        return idEstabelecimento;
    }

    public void setIdEstabelecimento(int idEstabelecimento) {
        this.idEstabelecimento = idEstabelecimento;
    }

    public int getIdMesa() {
        return idMesa;
    }

    public void setIdMesa(int idMesa) {
        this.idMesa = idMesa;
    }

    public int getNumComanda() {
        return numComanda;
    }

    public void setNumComanda(int numComanda) {
        this.numComanda = numComanda;
    }

    public int getIdUsuarioWeb() {
        return idUsuarioWeb;
    }

    public void setIdUsuarioWeb(int idUsuarioWeb) {
        this.idUsuarioWeb = idUsuarioWeb;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAutorizadoPor() {
        return autorizadoPor;
    }

    public void setAutorizadoPor(int autorizadoPor) {
        this.autorizadoPor = autorizadoPor;
    }

    public int getIdUsuarioDispositivo() {
        return idUsuarioDispositivo;
    }

    public void setIdUsuarioDispositivo(int idUsuarioDispositivo) {
        this.idUsuarioDispositivo = idUsuarioDispositivo;
    }

    public int getCodigoAutorizacaoMoviSolicitacaoAutorizacao() {
        return codigoAutorizacaoMoviSolicitacaoAutorizacao;
    }

    public void setCodigoAutorizacaoMoviSolicitacaoAutorizacao(int codigoAutorizacaoMoviSolicitacaoAutorizacao) {
        this.codigoAutorizacaoMoviSolicitacaoAutorizacao = codigoAutorizacaoMoviSolicitacaoAutorizacao;
    }

    public boolean isUsuarioAutorizado() {
        return usuarioAutorizado;
    }

    public void setUsuarioAutorizado(boolean usuarioAutorizado) {
        this.usuarioAutorizado = usuarioAutorizado;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}
