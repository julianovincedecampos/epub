package br.com.cerradoinformatica.ePub.model;

import java.io.Serializable;

public class Produto implements Serializable{
	
	private int idProdutoEstabelecimento;
	
	private int id;

    private String nome;

    private String caracteristicas;

    private String unidade;

    private double preco;

    private int idSincronizadoComProduto;

	private int IdGrupo;

    private String nomeGrupo;

    private String nomeImagem;

    private boolean cozinha;

    private boolean barman;

	public String getNomeImagem() {
		return nomeImagem;
	}

	public void setNomeImagem(String nomeImagem) {
		this.nomeImagem = nomeImagem;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdGrupo() {
		return IdGrupo;
	}

	public void setIdGrupo(int id_Grupo) {
		IdGrupo = id_Grupo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}


    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getNomeGrupo() {
        return nomeGrupo;
    }

    public void setNomeGrupo(String nomeGrupo) {
        this.nomeGrupo = nomeGrupo;
    }

    public boolean getCozinha() {
        return cozinha;
    }

    public void setCozinha(boolean cozinha) {
        this.cozinha = cozinha;
    }

    public boolean getBarman() {
        return barman;
    }

    public void setBarman(boolean barman) {
        this.barman = barman;
    }

    public int getIdProdutoEstabelecimento() {
        return idProdutoEstabelecimento;
    }

    public void setIdProdutoEstabelecimento(int idProdutoEstabelecimento) {
        this.idProdutoEstabelecimento = idProdutoEstabelecimento;
    }

    public int getIdSincronizadoComProduto() {
        return idSincronizadoComProduto;
    }

    public void setIdSincronizadoComProduto(int idSincronizadoComProduto) {
        this.idSincronizadoComProduto = idSincronizadoComProduto;
    }
}
