package br.com.cerradoinformatica.ePub.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.Usuario;
import br.com.cerradoinformatica.ePub.service.MovimentoMesaSrv;
import br.com.cerradoinformatica.ePub.service.MovimentoProdutoSrv;
import br.com.cerradoinformatica.ePub.txts.PubMovimentosolicitacaoAutorizacaoUtil;
import br.com.cerradoinformatica.ePub.txts.UsuarioTXT;
import jim.h.common.android.zxinglib.integrator.IntentIntegrator;
import jim.h.common.android.zxinglib.integrator.IntentResult;

/**
 * Created by juliano on 13/05/2014.
 */
public class QRCode extends Activity {

    private MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao,retornoMovimetoSolicitacaoAutorizacao;
    private MovimentoMesaSrv movimentoMesaSrv;
    private Usuario usuario;
    private AlertDialog alertDialog;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_code);
        inicializaObjetos();

        IntentIntegrator.initiateScan(this,  R.layout.qr_code, R.id.viewfinder_view, R.id.preview_view, false);
    }

    private void inicializaObjetos(){
        movimetoSolicitacaoAutorizacao = new MovimetoSolicitacaoAutorizacao();
        movimentoMesaSrv = new MovimentoMesaSrv(this);
        if(UsuarioTXT.readUsuario(this)==null){
            usuario = new Usuario();
            usuario.setIdDispositivoClienteDispositivo("0");
        }else{
            usuario = UsuarioTXT.readUsuario(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                final String result = scanResult.getContents();
                if ((result != null) && (scanResult.getFormatName().toString().contentEquals("QR_CODE"))) {
                   // movimetoSolicitacaoAutorizacao.setQrCode(result);
                   // movimetoSolicitacaoAutorizacao.setIdUsuarioDispositivo(Integer.parseInt(usuario.getIdDispositivoClienteDispositivo()));
                   // Toast.makeText(getBaseContext(), "Código QRCode: " + result, Toast.LENGTH_LONG).show();
                    //String [] vetor = result.split("?");
                   // Toast.makeText(getBaseContext(), "Código QRCode: " + vetor.toString(), Toast.LENGTH_LONG).show();
                    movimetoSolicitacaoAutorizacao.setQrCode(result);
                    new TaskPostMovimentoSolicitacaoAutorizacao(movimetoSolicitacaoAutorizacao).execute();
                } else {
                    Toast.makeText(getBaseContext(), "Código inválido ou inexistente.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this,QRCode.class);
                    startActivity(intent);
                    finish();
                }
                break;
            default:
        }
    }
    //Toast.makeText(getBaseContext(), "result: "  + result + " scanResult.getFormatName().toString() : " + scanResult.getFormatName().toString(), Toast.LENGTH_LONG).show();
    private class TaskPostMovimentoSolicitacaoAutorizacao extends AsyncTask<String,String,String>{

        private MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao;

        public TaskPostMovimentoSolicitacaoAutorizacao(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
            this.movimetoSolicitacaoAutorizacao = movimetoSolicitacaoAutorizacao;
        }

        @Override
        protected String doInBackground(String... params) {
            retornoMovimetoSolicitacaoAutorizacao = movimentoMesaSrv.postMovimentoAutorizarQRCode(movimetoSolicitacaoAutorizacao);
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(retornoMovimetoSolicitacaoAutorizacao!=null){
                retornoMovimetoSolicitacaoAutorizacao.setUsuarioAutorizado(true);
                PubMovimentosolicitacaoAutorizacaoUtil.clearFile(QRCode.this);
                PubMovimentosolicitacaoAutorizacaoUtil.writeMovimetoSolicitacaoAutorizacao(QRCode.this, retornoMovimetoSolicitacaoAutorizacao);
                Intent intent = new Intent(QRCode.this,AcompanhamentoDeContaTelaPrincipalActivity.class);
                startActivity(intent);
                finish();
            }else{
                showAlertDialogDrawerLayout(0,"Mobilepub","Ouve um problema na autenticação do QRcode \n Você deseja ler o QRCode novamente?","Sim","Não");
            }
        }
    }
    public void showAlertDialogDrawerLayout(int nomeIcone,String titulo,String mensagem,String nomeButtonPositive,String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(nomeIcone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(QRCode.this,QRCode.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}