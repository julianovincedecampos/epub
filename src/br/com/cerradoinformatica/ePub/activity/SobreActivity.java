package br.com.cerradoinformatica.ePub.activity;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.cerradoinformatica.ePub.adapter.NavDrawerItem;
import br.com.cerradoinformatica.ePub.adapter.NavDrawerListAdapter;
import br.com.cerradoinformatica.ePub.funcionalidades.GerenciadorAlertDialog;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.Perfil;
import br.com.cerradoinformatica.ePub.model.UsuarioHerbalife;
import br.com.cerradoinformatica.ePub.service.VerificaAPIService;
import br.com.cerradoinformatica.ePub.txts.PerfilTXT;
import br.com.cerradoinformatica.ePub.txts.PubMovimentosolicitacaoAutorizacaoUtil;
import br.com.cerradoinformatica.ePub.txts.UsuarioHerbalifeTXT;
import br.com.cerradoinformatica.ePub.txts.UsuarioTXT;
import br.com.cerradoinformatica.ePub.txts.UtilTXT;
import br.com.cerradoinformatica.ePub.txts.VersaoTXT;
import util.HttpUtil;
import util.URLUtil;

/**
 * Created by juliano on 27/01/14.
 */
public class SobreActivity extends ActionBarActivity implements View.OnClickListener{

    private ImageView imageViewFacebook, imageViewTwitter, imageViewGoogle;
    private TextView textViewVesaoAPP, textViewVersaoAPI, textViewAvaliarAPP, textViewSite;
    private PackageInfo packageInfo;
    private String versionName;
    private String versaoAPI;
    private ActionBar actionBar;
    private VerificaAPIService verificaAPIService;
    private Perfil perfil;
    //Drawer
    private String[] mItensListaMenu;
    private ListView mListaMenu;
    private DrawerLayout mNavigationDrawer;
    private ActionBarDrawerToggle mSelectorActionBar;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    //Drawer
    private MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao;
    private GerenciadorAlertDialog gerenciadorAlertDialog;
    //Dialog
    private TextView textViewCodigo,textViewTexto;
    private Button btnOk;
    private Dialog dialog;
    private AlertDialog alertDialog;
    //Dialog

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sobre);
        inicializaObjetos();

        TaskVerificaVersao taskVerificaVersao = new TaskVerificaVersao(this);
        taskVerificaVersao.execute();

        criaDrawerLayout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mSelectorActionBar.onOptionsItemSelected(item)){
            //Se for um click no ícone da aplicação, informa o seletor e encerra o tratamento
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void inicializaObjetos(){
        gerenciadorAlertDialog = new GerenciadorAlertDialog(this);
        movimetoSolicitacaoAutorizacao = PubMovimentosolicitacaoAutorizacaoUtil.readMovimetoSolicitacaoAutorizacao(this);

        if(PerfilTXT.readPerfil(this) != null){
            perfil = PerfilTXT.readPerfil(this);
            if(perfil.isHabilitaAPIProducao()){
                URLUtil.setMode("PRODUCAO");
            }else{
                URLUtil.setMode("DESENVOLVIMENTO");
            }
        }


        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        verificaAPIService = new VerificaAPIService(this);
        versionName = packageInfo.versionName;


        imageViewFacebook  = (ImageView) findViewById(R.id.imageViewFacebook);
        imageViewFacebook.setOnClickListener(this);

        imageViewTwitter   = (ImageView) findViewById(R.id.imageViewTwitter);
        imageViewTwitter.setOnClickListener(this);

        imageViewGoogle    = (ImageView) findViewById(R.id.imageViewGoogle);
        imageViewGoogle.setOnClickListener(this);

        textViewAvaliarAPP = (TextView) findViewById(R.id.textViewAvaliarAPP);
        textViewAvaliarAPP.setOnClickListener(this);

        textViewVesaoAPP = (TextView) findViewById(R.id.textViewVesaoAPP);
        textViewVesaoAPP.setText("Versão APP \n" + versionName);

        textViewVersaoAPI = (TextView) findViewById(R.id.textViewVersaoAPI);

        textViewSite = (TextView) findViewById(R.id.textViewSite);
        textViewSite.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(imageViewFacebook == view){
            linkFacebook();
        }else if(imageViewTwitter == view){
            linkTwitter();
        }else if(imageViewGoogle == view){
            linkGoogle();
        }else if(textViewAvaliarAPP == view){
            linkLoja();
        }else if(textViewSite == view){
            linkSite();
        }else if(btnOk == view){
            dialog.dismiss();
        }
    }
    public void linkFacebook(){
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://www.facebook.com/Mobilepubapp"));
        startActivity(intent);
    }
    public void linkLoja(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=br.com.cerradoinformatica.ePub.activity&hl=pt_BR"));
        startActivity(intent);
    }
    public void linkTwitter(){
        Intent intent = new Intent("android.intent.action.VIEW",Uri.parse("https://twitter.com/Mobilepub1"));
        startActivity(intent);
    }
    public void linkGoogle(){
        Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("https://plus.google.com/u/0/b/100040960750379508305/100040960750379508305/posts"));
        startActivity(browserIntent);
    }
    public void linkSite(){
        Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://mobilepub.com.br/"));
        startActivity(browserIntent);
    }
    private class TaskVerificaVersao extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskVerificaVersao(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            //Cria novo ProgressDialog e exibe
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            versaoAPI = verificaAPIService.getVerificaAPISerive();
            progressDialog.dismiss();

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
        @Override
        protected void onPostExecute(String result) {
            if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_DESCONHECIDO")){
                gerenciadorAlertDialog.showAlertDialogMainVerificaVersao(SobreActivity.this, 0,"MobilePub","Erro Desconhecido!","Sair do aplicativo");
            }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR")){
                gerenciadorAlertDialog.showAlertDialogMainVerificaVersao(SobreActivity.this, 0,"MobilePub","Erro de conexão com Servidor!","Sair do aplicativo");
            }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("TIME_OUT")){
                gerenciadorAlertDialog.showAlertDialogMainVerificaVersao(SobreActivity.this, 0, "MobilePub", "Time out!", "Sair do aplicativo");
            }else if(URLUtil.versao(versaoAPI).equalsIgnoreCase("OK")){
                textViewVersaoAPI.setText("Versão API \n" + versaoAPI.substring(1, versaoAPI.length() - 1));
            }else if(URLUtil.versao(versaoAPI).equalsIgnoreCase("VARIAVEL_PRODUCAO_JSON_TESTE")){
                gerenciadorAlertDialog.showAlertDialogMainVerificaVersao(SobreActivity.this, 0, "Erro de compatibilidade!", "App (Garçom) de PRODUÇÃO incompatível com TESTE.", "Sair do aplicativo");
            }else if(URLUtil.versao(versaoAPI).equalsIgnoreCase("VARIAVEL_DESENVOLVIMENTO_JSON_PRODUCAO")){
                gerenciadorAlertDialog.showAlertDialogMainVerificaVersao(SobreActivity.this, 0,"Erro de compatibilidade!","App (Garçom) de TESTE incompatível com PRODUÇÃO.","Sair do aplicativo");
            }
        }
    };
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }
    public void criaDrawerLayout(){
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        mListaMenu = (ListView)findViewById(R.id.lista);
        // mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Sair da Conta","Codigo Autorização"};

        if(movimetoSolicitacaoAutorizacao == null){
            getSupportActionBar().setTitle("Sobre");
            mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre"};
        }else{
            getSupportActionBar().setTitle("Sobre");
            mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Codigo Autorização: "+movimetoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Sair da Conta"};
        }

        mListaMenu.setAdapter(new ArrayAdapter<String>(this, R.layout.menu_item_layout, mItensListaMenu));
        navDrawerItems = new ArrayList<NavDrawerItem>();
        // adding nav drawer items to array

        //Conta
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[0], navMenuIcons.getResourceId(1, -1)));
        //Cardapio
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[1], navMenuIcons.getResourceId(0, -1)));
        //Fechar Conta
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[2], navMenuIcons.getResourceId(2, -1)));
        //Historico
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[3], navMenuIcons.getResourceId(3, -1)));
        //Perfil
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[4], navMenuIcons.getResourceId(4, -1)));
        //Sobre
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[5], navMenuIcons.getResourceId(5, -1)));

        if(movimetoSolicitacaoAutorizacao == null ){

        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()!=false){
            //Codigo autorização
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[6], navMenuIcons.getResourceId(6, -1)));
            //Sair da conta
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[7], navMenuIcons.getResourceId(7, -1)));
        }

        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);
        mListaMenu.setAdapter(adapter);

        /*Configuração do DrawerLayout*/
        mNavigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mNavigationDrawer.setDrawerShadow(R.color.transparente, GravityCompat.START);

        /*Configuração do seletor no action bar*/
        mSelectorActionBar = new ActionBarDrawerToggle(SobreActivity.this, mNavigationDrawer, R.drawable.ic_drawer, 0, 0 ){


            @Override
            public void onDrawerClosed(View drawerView){
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
                //Evento que informará que drawer foi fechado
            }
        };
        mNavigationDrawer.setDrawerListener(mSelectorActionBar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mListaMenu.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> container, View view, int indice, long id) {
                if(indice==0){
                    conta();
                }else if(indice==1){
                    cardapio();
                }else if(indice==2){
                    fecharConta();
                }else if(indice==3){
                    historico();
                }else if(indice==4){
                    perfil();
                }else if(indice==5){
                    sobre();
                }else if(indice==6){
                    showAlertDialogCodigoAutorizacao(movimetoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Use este código para autorizar mais dispositivos e acompanhar sua conta ");
                }else if(indice==7){
                    showAlertDialogSairConta(R.drawable.ic_action_about, "Mobilepub", "Você tem certeza que deseja sair da conta?", "Sim", "Não");
                }
            }
        });
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mSelectorActionBar.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mSelectorActionBar.onConfigurationChanged(newConfig);
    }
    public void conta() {
        if(movimetoSolicitacaoAutorizacao == null){
            Intent intent = new Intent(this, AbrirConta.class);
            startActivity(intent);
            finish();
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
            Toast.makeText(this, "Sua conta ainda não foi liberada!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void cardapio(){
        if(movimetoSolicitacaoAutorizacao == null){
            showAlertDialogDrawerLayout(R.drawable.ic_action_about,"Mobilepub","Você não possui conta aberta no momento! \n Deseja acompanhar uma conta?","Sim","Não");
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
            Toast.makeText(this, "conta ainda não foi liberada!", Toast.LENGTH_LONG).show();
        }else{
            Intent intent = new Intent(this, CardapioActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void fecharConta(){
        if(movimetoSolicitacaoAutorizacao == null){
            showAlertDialogDrawerLayout(R.drawable.ic_action_about,"Mobilepub","Você não possui conta aberta no momento! \n Deseja acompanhar uma conta?","Sim","Não");
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
            Toast.makeText(this, "Você sua conta ainda não foi liberada!", Toast.LENGTH_LONG).show();
        }else{
            Intent intent = new Intent(this, FechamentoActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void historico(){
        Intent intent = new Intent(this, HistoricoActivity.class);
        startActivity(intent);
        finish();
    }
    public void perfil(){
        if(UsuarioTXT.readUsuario(this) == null){
            Intent intent = new Intent(this,CadastroActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("br.com.cerradoinformatica.ePub.fragments.DetalhesFragments",null);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(this,PerfilActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void sobre(){

    }
    public void  limpar(){
        if(UsuarioHerbalifeTXT.readUsuarioHerbalife(this) != null){
            if(UsuarioHerbalifeTXT.readUsuarioHerbalife(this).isLogado()){
                UsuarioHerbalife usuarioHerbalife = UsuarioHerbalifeTXT.readUsuarioHerbalife(this);
                usuarioHerbalife.setLogado(false);
                UsuarioHerbalifeTXT.writeUsuarioHerbalife(this,usuarioHerbalife);
            }
        }
        UtilTXT.limparTodosArquivos(this);
        Intent intent= new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }
    public void showAlertDialogCodigoAutorizacao(int codigoAutorizacao,String mensagem){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_codigo_autorizacao);
        dialog.setTitle("Código de autorização");
        textViewCodigo = (TextView) dialog.findViewById(R.id.textViewCodigo);
        textViewTexto = (TextView) dialog.findViewById(R.id.textViewTexto);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(SobreActivity.this);

        textViewCodigo.setText(""+codigoAutorizacao);
        textViewTexto.setText(mensagem);
        dialog.show();
        dialog.setCancelable(false);
    }
    public void showAlertDialogSairConta(int name_incone, String titulo, String mensagem, String nomeButtonPositive,String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                limpar();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void showAlertDialogDrawerLayout(int nomeIcone,String titulo,String mensagem,String nomeButtonPositive,String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(nomeIcone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(SobreActivity.this, AbrirConta.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
}