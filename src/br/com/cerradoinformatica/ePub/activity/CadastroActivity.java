package br.com.cerradoinformatica.ePub.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import br.com.cerradoinformatica.ePub.funcionalidades.DatasUtils;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.Usuario;
import br.com.cerradoinformatica.ePub.service.UsuarioDispositivoService;
import br.com.cerradoinformatica.ePub.txts.URLImagemFacebook;
import br.com.cerradoinformatica.ePub.txts.UsuarioTXT;
import util.DownloadsImagens;
import util.HttpUtil;

/**
 * Created by juliano on 30/01/14.
 */
public class CadastroActivity extends FragmentActivity implements View.OnClickListener, Serializable{

    private EditText editTextNome, editTextEmail, editTextSenha, editTextDataNascimento, editTextTelefone;
    private RadioButton radioButtonMasculino, radioButtonFeminino;
    private Button buttonCadastrar, buttonCancelar;
    private UsuarioDispositivoService usuarioDispositivoService;
    private String sexo;
    private Date dataHoje;
    private MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao;
    private String senhaNormal, senhaCriptografada;
    //Dialog
    private Button buttonLoginDialog, buttonCancelarDialog;
    private TextView textViewCadastrarseDialog, editTextEmailDialog, editTextSenhaDialog, textViewEsqueceuSuaSenhaDialog,textViewLoginFacebook;
    private Dialog dialog;
    private Usuario usuario;
    //Dialog
    private ImageView imageUsuario;
    private boolean usuarioFacebook = false;
    private long idUsuarioFacebook = 0;
    static final int DATE_DIALOG_ID = 0;
    private String URL = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastro);
        inicializaObjetos();
    }

    public void inicializaObjetos(){
        usuario = UsuarioTXT.readUsuario(this);
        if(usuario==null){
            usuario = new Usuario();
        }

        movimetoSolicitacaoAutorizacao = (MovimetoSolicitacaoAutorizacao) getIntent().getExtras().getSerializable("br.com.cerradoinformatica.ePub.fragments.DetalhesFragments");

        this.sexo = "";
        usuarioDispositivoService = new UsuarioDispositivoService(this);
        //getInformacoes();
        dataHoje = new Date();

        editTextNome = (EditText)  findViewById(R.id.editTextNome);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextSenha = (EditText) findViewById(R.id.editTextSenha);
        editTextDataNascimento = (EditText) findViewById(R.id.editTextDataNascimento);
        editTextDataNascimento.setOnClickListener(this);
        editTextTelefone = (EditText) findViewById(R.id.editTextTelefone);

        imageUsuario = (ImageView) findViewById(R.id.imageUsuario);

        radioButtonMasculino = (RadioButton) findViewById(R.id.radioButtonMasculino);
        radioButtonMasculino.setOnClickListener(this);

        radioButtonFeminino = (RadioButton) findViewById(R.id.radioButtonFeminino);
        radioButtonFeminino.setOnClickListener(this);

        buttonCadastrar = (Button) findViewById(R.id.buttonCadastrar);
        buttonCadastrar.setOnClickListener(this);

        buttonCancelar = (Button) findViewById(R.id.buttonCancelar);
        buttonCancelar.setOnClickListener(this);

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_cadastro_de_usuario);

        dialog.setTitle("Login");
        //facebook
        textViewLoginFacebook = (TextView) dialog.findViewById(R.id.textViewLoginFacebook);
        textViewLoginFacebook.setOnClickListener(this);
        //facebook
        buttonLoginDialog = (Button) dialog.findViewById(R.id.buttonLoginDialog);
        buttonLoginDialog.setOnClickListener(this);

        buttonCancelarDialog = (Button) dialog.findViewById(R.id.buttonCancelarDialog);
        buttonCancelarDialog.setOnClickListener(this);

        textViewCadastrarseDialog = (TextView) dialog.findViewById(R.id.textViewCadastrarseDialog);
        textViewCadastrarseDialog.setOnClickListener(this);

        textViewEsqueceuSuaSenhaDialog = (TextView) dialog.findViewById(R.id.textViewEsqueceuSuaSenhaDialog);
        textViewEsqueceuSuaSenhaDialog.setOnClickListener(this);

        editTextEmailDialog = (TextView) dialog.findViewById(R.id.editTextEmailDialog);

        editTextSenhaDialog = (TextView) dialog.findViewById(R.id.editTextSenhaDialog);
        editTextSenhaDialog.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                verificaUsuario();
                return true;
            }
        });

        dialog.show();
        dialog.setCancelable(false);
    }
    public String validaSenhaDialog(){
        senhaNormal = "";
        senhaCriptografada = "";
        try {
            senhaNormal = editTextSenha.getText().toString();
            senhaCriptografada = criptografaSenha(editTextSenha.getText().toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return senhaCriptografada;
    }
    public String verificaSenha(){
        senhaNormal = "";
        senhaCriptografada = "";
        try {
            senhaNormal = editTextSenhaDialog.getText().toString();
            senhaCriptografada = criptografaSenha(editTextSenhaDialog.getText().toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return senhaCriptografada;
    }
    public String criptografaSenha (String senha) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
        String s = hash.toString(16);
        if (s.length() %2 != 0)
            s = "0" + s;
        return s;
    }
    @Override
    public void onClick(View view) {
        if(view == radioButtonFeminino){
            setValorSexo("F");
            radioButtonFeminino.setChecked(true);
            radioButtonMasculino.setChecked(false);
        }else if(view == radioButtonMasculino){
            setValorSexo("M");
            radioButtonMasculino.setChecked(true);
            radioButtonFeminino.setChecked(false);
        }else if(view == buttonCadastrar){
            buttonCadastrar.setEnabled(false);
            cadastraUsuario();
        }else if(view == buttonCancelar){
            cancelar();
        }else if(view == buttonCancelarDialog){
            cancelar();
        }else if(view == buttonLoginDialog){
            verificaUsuario();
        }else if(view == textViewCadastrarseDialog){
            dialog.dismiss();
        }else if(view == textViewEsqueceuSuaSenhaDialog){
            linkSite();
        }else if(view ==editTextDataNascimento){
            showDialog(DATE_DIALOG_ID);
            //editTextDataNascimento.setEnabled(false);
        }else if(view == textViewLoginFacebook){
            loginFacebookDialog();
        }
    }

    public void setValorSexo(String sexo){
        this.sexo = "";
        this.sexo = sexo;
    }
    public String getValorSexo(){
        if(this.sexo.equalsIgnoreCase("")){
            this.sexo = "M";
        }
        return this.sexo;
    }
    public void cadastraUsuario(){
        if(editTextNome.getText().toString().equalsIgnoreCase("") || editTextNome.getText().toString().length() < 2 ){
            Toast.makeText(this,"Nome esta incorreto!",Toast.LENGTH_SHORT).show();
            editTextNome.requestFocus();
        }else if(editTextEmail.getText().toString().equalsIgnoreCase("") || editTextEmail.getText().toString().length() < 9 ){
            Toast.makeText(this,"Email esta incorreto!",Toast.LENGTH_SHORT).show();
            editTextEmail.requestFocus();
        }else if(usuarioFacebook == false && (editTextSenha.getText().toString().equalsIgnoreCase("") || editTextSenha.getText().toString().length() < 4)){
            Toast.makeText(this,"Senha esta incorreta!",Toast.LENGTH_SHORT).show();
            editTextSenha.requestFocus();
        }else{
            TaskPostUsuario taskPostUsuario = new TaskPostUsuario(this);
            taskPostUsuario.execute();
        }
        buttonCadastrar.setEnabled(true);
    }
    public void verificaUsuario(){
        if(editTextEmailDialog.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(this,"Email incorreto!",Toast.LENGTH_SHORT).show();
        }else if(editTextSenhaDialog.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(this,"Senha incorreta!",Toast.LENGTH_SHORT).show();
        }else{
            TaskGetUsuario taskGetUsuario = new TaskGetUsuario(this);
            taskGetUsuario.execute();
        }
    }
    public void cancelar(){
       if(movimetoSolicitacaoAutorizacao != null){
           Intent intent = new Intent(this, CardapioActivity.class);
           startActivity(intent);
           finish();
       }else{
           Intent intent = new Intent(this, AcompanhamentoDeContaTelaPrincipalActivity.class);
           startActivity(intent);
           finish();
       }
    }
    public void linkSite(){
        Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://mobilepub.com.br/senha_cliente"));
        startActivity(browserIntent);
    }

    private class TaskPostUsuario extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskPostUsuario(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            //Cria novo ProgressDialog e exibe
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {

            //Dados da tela
            usuario.setNomeClienteDispositivo(editTextNome.getText().toString());
            usuario.setEmailClienteDispositivo(editTextEmail.getText().toString());
            usuario.setSenhaClienteDispositivo(validaSenhaDialog());
            usuario.setDataNascimentoClienteDispositivo(DatasUtils.getStringDateCadastro(editTextDataNascimento.getText().toString()));
            usuario.setTelefoneClienteDispositivo(editTextTelefone.getText().toString());
            usuario.setSexoClienteDispositivo(getValorSexo());
            //Dados da tela

            //Dados do dispositivo
            usuario.setIdClienteDispositivo(0);
            usuario.setIdFacebookClienteDispositivo(idUsuarioFacebook);
            usuario.setUriImagemClienteDispositivo(URL);
            usuario.setFabricanteHardwareClienteDispositivo(android.os.Build.MANUFACTURER);
            usuario.setModeloHardwareClienteDispositivo(android.os.Build.MODEL);
            usuario.setVersaoHardwareClienteDispositivo(android.os.Build.PRODUCT);
            usuario.setSoDispositivoClienteDispositivo("android-" + android.os.Build.VERSION.RELEASE);
            usuario.setIdDispositivoClienteDispositivo(android.os.Build.ID);
            usuario.setExcluidoClienteDispositivo(false);
            usuario.setDataAlteracaoClienteDispositivo(dataHoje);
            usuario.setDataCadastroClienteDispositivo(dataHoje);
            //Dados do dispositivo
            usuarioDispositivoService.postUsuario(usuario);
            progressDialog.dismiss();
            return null;

        }
        @Override
        protected void onPostExecute(String result) {
            if(HttpUtil.MENSAGEM.equalsIgnoreCase("300") || HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO: (300)")){
                Toast.makeText(CadastroActivity.this,"Usuario já possui cadastro!",Toast.LENGTH_SHORT).show();
            }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("304")){
                Toast.makeText(CadastroActivity.this,"Erro 304!",Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(CadastroActivity.this,"Seja bem vindo: " + usuario.getNomeClienteDispositivo(),Toast.LENGTH_SHORT).show();
                UsuarioTXT.writeUsuario(CadastroActivity.this,usuario);
                direciona();
            }
        }
    };
    public void getInformacoes(){
        //String debug2 = System.getProperty("os.version");
        String debug3 = android.os.Build.VERSION.RELEASE;
        //String debug4 = android.os.Build.DEVICE;
        String debug5 = android.os.Build.MODEL;
        String debug6 = android.os.Build.PRODUCT;
        //String debug7 = android.os.Build.BRAND;
        //String debug8 = android.os.Build.DISPLAY;
        //String debug9 = android.os.Build.CPU_ABI;
        //String debug10 = android.os.Build.CPU_ABI2;
        //String debug11 = android.os.Build.UNKNOWN;
        //String debug12 = android.os.Build.HARDWARE;
        String debug13 = android.os.Build.ID;
        String debug14 = android.os.Build.MANUFACTURER;
        //String debug15 = android.os.Build.SERIAL;
        //String debug16 = android.os.Build.USER;
        //String debug17 = android.os.Build.HOST;
    }
    private class TaskGetUsuario extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskGetUsuario(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            usuario   =  usuarioDispositivoService.getUsuario(editTextEmailDialog.getText().toString(),verificaSenha());
            progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if(HttpUtil.MENSAGEM.equalsIgnoreCase("401")){
                Toast.makeText(CadastroActivity.this,"Senha Não Confere!",Toast.LENGTH_SHORT).show();
                usuario = new Usuario();
            }else if(usuario == null){
                Toast.makeText(CadastroActivity.this,"Usuario não cadastrado!",Toast.LENGTH_SHORT).show();
                usuario = new Usuario();
            }else{
                Toast.makeText(CadastroActivity.this,"Bem Vindo! " + usuario.getNomeClienteDispositivo(),Toast.LENGTH_SHORT).show();
                UsuarioTXT.writeUsuario(CadastroActivity.this,usuario);
                direciona();
            }
        }
    };
    public void direciona(){
        if(movimetoSolicitacaoAutorizacao != null){
            Intent intent = new Intent(this, CardapioActivity.class);
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(this, AcompanhamentoDeContaTelaPrincipalActivity.class);
            startActivity(intent);
            finish();
        }
    }
    @Override
    public void onBackPressed() {
        direciona();
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        Calendar calendario = Calendar.getInstance();
        int ano = calendario.get(Calendar.YEAR);
        int mes = calendario.get(Calendar.MONTH);
        int dia = calendario.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID: return new DatePickerDialog(this, mDateSetListener, ano, mes, dia);
        } return null;
    }
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int ano, int mesDoAno, int diaDoMes) {
            String data = String.valueOf(diaDoMes) + "/" + String.valueOf(mesDoAno+1) + "/" + String.valueOf(ano);
            editTextDataNascimento.setText(data);
           // Toast.makeText(CadastroActivity.this, "DATA = " + data, Toast.LENGTH_SHORT) .show();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }
    public void loginFacebookDialog(){
        Session.openActiveSession(this, true, new Session.StatusCallback() {

            // callback when session changes state
            @Override
            public void call(final Session session, SessionState state, Exception exception) {
                if (session.isOpened()) {
                    // make request to the /me API
                    Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {

                        // callback after Graph API response with user object
                        @Override
                        public void onCompleted(GraphUser user, Response response) {
                            if (user != null) {

                                URL = "https://graph.facebook.com/"+user.getId()+"/picture?type=large";
                                URLImagemFacebook.writeURLImagemFacebook(CadastroActivity.this,URL);

                                String data="",email="",sexo="";

                                try{
                                     data = user.getProperty("birthday").toString();//birthday
                                }catch (Exception e){
                                    data = "";
                                }
                                try{
                                     email = user.getProperty("email").toString();//email
                                }catch (Exception e){
                                    email = "";
                                }
                                try{
                                     sexo = user.getProperty("gender").toString();//gender
                                }catch(Exception e){
                                    sexo = "";
                                }

                                //String telefone = user.getProperty("gender").toString();


                                if(sexo.equalsIgnoreCase("male") || sexo.equalsIgnoreCase("")){
                                    setValorSexo("M");
                                    radioButtonFeminino.setChecked(false);
                                    radioButtonMasculino.setChecked(true);
                                }else{
                                    setValorSexo("F");
                                    radioButtonFeminino.setChecked(true);
                                    radioButtonMasculino.setChecked(false);
                                }

                                editTextNome.setText(user.getName());
                                editTextEmail.setText(email);
                                editTextDataNascimento.setText(data);

                                // Create an object for subclass of AsyncTask
                                GetXMLTask task = new GetXMLTask();
                                // Execute the task
                                task.execute(URL);

                                idUsuarioFacebook = Long.parseLong(user.getId());
                            }
                            usuarioFacebook = true;
                            dialog.dismiss();
                        }
                    });
                }
            }
        });
    }
    private class GetXMLTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap map = null;
            for (String url : urls) {
                map = DownloadsImagens.downloadImage(url);
            }
            return map;
        }
        // Sets the Bitmap returned by doInBackground
        @Override
        protected void onPostExecute(Bitmap result) {
            imageUsuario.setImageBitmap(result);
        }
    }
}