package br.com.cerradoinformatica.ePub.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.cerradoinformatica.ePub.adapter.LinearLayoutAdapter;
import br.com.cerradoinformatica.ePub.model.HistoricoConta;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.service.MovimentoProdutoSrv;
import br.com.cerradoinformatica.ePub.txts.DetalhesHistoricoDasContasTXT;

/**
 * Created by juliano on 10/01/14.
 */
public class DetalhesHistoricoActivity extends ActionBarActivity {

    private List<MovimentoProduto> lstMovimentoProduto;
    private MovimentoProdutoSrv movimentoProdutoSrv;
    private String arquivo;
    private TextView textTotalValor;
    private int somaQuantidade;
    private AlertDialog alertDialog;
    private HistoricoConta historicoConta;

    private double txServico;
    private double valorPagar;
    private  double subTotal;
    private int quantidadePessoasParaRateio = 0;

    private LinearLayout lProdutos;
    private TextView textNumeroDaComanda;
    private TextView textSubTotalValue;
    private TextView textTxServicoValue;
    private TextView textValorPagarValue;
    private TextView textValorPessoaValue;
    private TextView textQuantidadePessoa;
    private EditText editQtdPessoas;
    private ActionBar actionBar;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalhes_historico_activity);
        inicializaObjetos();

        getSupportActionBar().setTitle(R.string.detalhesHistorico_titulo);
        actionBar = getSupportActionBar();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayHomeAsUpEnabled(true);

       lstMovimentoProduto = movimentoProdutoSrv.getProdutoSelecionadosHistoricos(arquivo);

        if(lstMovimentoProduto == null){
            dialogHistorico("MobilePub", R.drawable.ic_action_about, "Você ainda não possui histórico de contas!", "Ok");
        }else{
            leValorConta();
        }
    }
    public void dialogHistorico(String titulo, int icon, String message, String buttonSim){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setIcon(icon);
        builder.setMessage(message);
        builder.setPositiveButton(buttonSim, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(DetalhesHistoricoActivity.this,AcompanhamentoDeContaTelaPrincipalActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setCancelable(false);
        alertDialog = builder.create();
        alertDialog.show();
    }
    public void inicializaObjetos(){
        lstMovimentoProduto = new ArrayList<MovimentoProduto>();
        movimentoProdutoSrv = new MovimentoProdutoSrv(this);
        textTotalValor = (TextView) findViewById(R.id.textTotalValor);
        arquivo = (String) getIntent().getExtras().getSerializable("br.com.cerradoinformatica.model.nomeArquivo");
        historicoConta = new HistoricoConta();

        lProdutos = (LinearLayout) findViewById(R.id.lLayoutProdutos);
        textSubTotalValue = (TextView) findViewById(R.id.textValorSubTotalValue);
        textTxServicoValue = (TextView) findViewById(R.id.textTaxaServicoValue);
        textValorPagarValue = (TextView) findViewById(R.id.textValorPagarValue);
        textValorPessoaValue=(TextView) findViewById(R.id.textValorPessoas);
        textQuantidadePessoa = (TextView) findViewById(R.id.textQuantidadePessoa);
        editQtdPessoas=(EditText) findViewById(R.id.editQtdPessoas);
        textNumeroDaComanda = (TextView) findViewById(R.id.textNumeroDaComanda);
        textNumeroDaComanda = (TextView) findViewById(R.id.textNumeroDaComanda);

        //textNumeroDaComanda.setText("Estabelecimento " + historicoConta.getIdEstabelecimento()  +" / Data " +  historicoConta.getDate());
    }
    public void leValorConta(){
        for(int a=0;a < lstMovimentoProduto.size();a++){
            for(int b = a + 1;b < lstMovimentoProduto.size();b++){
                if((lstMovimentoProduto.get(a).getIdProduto()==lstMovimentoProduto.get(b).getIdProduto())){
                    somaQuantidade = lstMovimentoProduto.get(a).getQuantidade();
                    somaQuantidade += lstMovimentoProduto.get(b).getQuantidade();
                    lstMovimentoProduto.get(a).setQuantidade(somaQuantidade);
                    lstMovimentoProduto.remove(b);
                    b--;
                }
            }
        }
        new LinearLayoutAdapter(this, lstMovimentoProduto, lProdutos);
        textSubTotalValue.setText(String.format("%.2f", calculaSubTotal()).replace(".", ","));
        textTxServicoValue.setText(String.format("%.2f", calculaTxServico(subTotal)).replace(".", ","));
        textValorPagarValue.setText(String.format("%.2f", calculaValor()).replace(".",","));
//        textValorPessoaValue.setText(String.format(textQuantidadePessoa.getText() + "%.2f", calculaValorPessoa()).replace(".", ","));

    }
    public double calculaSubTotal(){
        double total=0;
        for(MovimentoProduto  mov :lstMovimentoProduto){
            total+=mov.getValorUnitario()*mov.getQuantidade();
        }
        return subTotal= total;
    }
    public double calculaValor(){
        return valorPagar=subTotal+txServico;
    }
    public double calculaTxServico(double valor){
        double d=valor *(10f/100f);
        return txServico=d;
    }
    public double calculaValorPessoa(){
        if(!editQtdPessoas.getText().toString().equals("")||editQtdPessoas.getText()==null){
            valorPagar= calculaValor()/this.quantidadePessoasParaRateio;
            textQuantidadePessoa.setText("Valor para "+editQtdPessoas.getText()+" pessoas");
        }else{
            textQuantidadePessoa.setText("Valor por pessoa: ");
        }
        return valorPagar;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_historico, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.excluirConta){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("MobilePub");
            builder.setMessage("Você deseja excluir o historico da conta?");
            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    DetalhesHistoricoDasContasTXT.clearFile(arquivo);
                    Intent intent = new Intent(DetalhesHistoricoActivity.this, HistoricoActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alertDialog = builder.create();
            alertDialog.show();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}