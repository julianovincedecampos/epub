package br.com.cerradoinformatica.ePub.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.cerradoinformatica.ePub.adapter.MovimentoProdutoAdapter;
import br.com.cerradoinformatica.ePub.adapter.NavDrawerItem;
import br.com.cerradoinformatica.ePub.adapter.NavDrawerListAdapter;
import br.com.cerradoinformatica.ePub.funcionalidades.DatasUtils;
import br.com.cerradoinformatica.ePub.model.MovimentoMesa;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.Usuario;
import br.com.cerradoinformatica.ePub.model.UsuarioHerbalife;
import br.com.cerradoinformatica.ePub.service.AguardandoAutorizacaoService;
import br.com.cerradoinformatica.ePub.service.MovimentoProdutoSrv;
import br.com.cerradoinformatica.ePub.service.VerificaAPIService;
import br.com.cerradoinformatica.ePub.txts.UsuarioHerbalifeTXT;
import br.com.cerradoinformatica.ePub.txts.UsuarioTXT;
import br.com.cerradoinformatica.ePub.txts.UtilTXT;
import util.EpubUtils;
import br.com.cerradoinformatica.ePub.txts.PubMovimentosolicitacaoAutorizacaoUtil;
import util.HttpUtil;

/**
 * Created by juliano on 20/01/14.
 *
 */

public class AcompanhamentoDeContaTelaPrincipalActivity extends ActionBarActivity implements View.OnClickListener{

    private ImageView imageViewContaVazia;
    private ImageView imageViewAdicionar;
    private TextView textAcompanharConta;
    private TextView textTotalValor;
    private TextView textViewPequena;
    private TextView textViewGrande;
    //Dialog
    private TextView textViewCodigo,textViewTexto;
    private Button btnOk;
    //Dialog
    private ListView listViewProdutosSelec;
    private MovimentoMesa movimento;
    private MovimentoProduto movimentoProduto;
    private MovimentoProdutoSrv movimentoProdutoSrv;
    private AguardandoAutorizacaoService aguardandoAutorizacaoService;
    private MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao;
    private List<MovimentoProduto> lstMovimentoProduto;
    private MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacaoRetorno;
    private final String TAG="<<AcompanhamentoDeContaTelaPrincipalActivity>>";
    private AlertDialog alertDialog;
   // private AguardandoAutorizacaoService aguardandoAutorizacaoService;
    //Drawer
    private String[] mItensListaMenu;
    private ListView mListaMenu;
    private DrawerLayout mNavigationDrawer;
    private ActionBarDrawerToggle mSelectorActionBar;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    //Drawer
    private MenuItem menu_cancelar;
    private String versao ="";
    private ImageView minhaImagem;
    private Usuario usuario;
    private UsuarioHerbalife usuarioHerbalife;
    private Button btnCodigoAutorizacao;
    private EditText editTextCodgioAutorizacao;
    private Dialog dialog;
    private VerificaAPIService verificaAPIService;
    private static final String VERSAO_API_COMPATIVEL = "1.2";

    private StringBuffer stringBuffer = new StringBuffer("juliano");
    private StringBuilder stringBuilder = new StringBuilder("juliano");
    private String string = new String("juliano");


    public void teste(){
        stringBuffer.append("a");
        stringBuilder.append("a");
        string.concat("");
        String teste = stringBuffer.toString() + string.toString() + string.toString();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        usuario = UsuarioTXT.readUsuario(this);

        movimetoSolicitacaoAutorizacao = PubMovimentosolicitacaoAutorizacaoUtil.readMovimetoSolicitacaoAutorizacao(this);
        usuarioHerbalifeInicializa();

        if(usuarioHerbalife.isLogado()){
            inicializaObjetos();
        }else if(movimetoSolicitacaoAutorizacao == null){
            telaSemAcompanhamento();
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
            aguardandoAutorizacao();
        }else {
            inicializaObjetos();
        }
        criaDrawerLayout();
   }
   private void usuarioHerbalifeInicializa(){
       usuarioHerbalife = UsuarioHerbalifeTXT.readUsuarioHerbalife(this);
       if(usuarioHerbalife == null){
           usuarioHerbalife = new UsuarioHerbalife();
           usuarioHerbalife.setLogado(false);
       }
   }
   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       MenuInflater menuInflater = getMenuInflater();
       if(movimetoSolicitacaoAutorizacao == null){

       }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()){
           menuInflater.inflate(R.menu.menu_principal_produtos_selecionados, menu);
       }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
           menuInflater.inflate(R.menu.menu_principal_aguarda_autorizacao, menu);
       }
        return super.onCreateOptionsMenu(menu);
    }
    public void telaSemAcompanhamento(){
        setContentView(R.layout.tela_principal_sem_acompanhamento_de_conta);
        imageViewAdicionar = (ImageView) findViewById(R.id.imageViewAdicionar);
        imageViewAdicionar.setOnClickListener(this);
        verificaAPIService = new VerificaAPIService(this);
        new TaskVerificaVersao(this).execute();
    }
    public void inicializaObjetos(){
        setContentView(R.layout.acompanhamento_de_conta_tela_principal);
        movimento = EpubUtils.readMovimentoMesa(this);
        movimentoProduto = EpubUtils.readMovimentoProduto(this);
        movimentoProdutoSrv = new MovimentoProdutoSrv(this);
        lstMovimentoProduto = new ArrayList<MovimentoProduto>();
        listViewProdutosSelec =(ListView) findViewById(R.id.listProdutosAdd);
        textTotalValor = (TextView)findViewById(R.id.textTotalValor);
        textAcompanharConta = (TextView) findViewById(R.id.textInformacaoMesa);
        imageViewContaVazia = (ImageView) findViewById(R.id.imageViewContaVazia);
        textViewPequena = (TextView) findViewById(R.id.textViewPequena);
        textViewGrande = (TextView) findViewById(R.id.textViewGrande);

        if(usuarioHerbalife.isLogado()){
            movimetoSolicitacaoAutorizacao = new MovimetoSolicitacaoAutorizacao();
            movimetoSolicitacaoAutorizacao.setIdEstabelecimento(usuarioHerbalife.getIdEstabelecimento());
            movimetoSolicitacaoAutorizacao.setIdMesa(usuarioHerbalife.getIdMesa());
            movimetoSolicitacaoAutorizacao.setDataAbertura(DatasUtils.getDateString(usuarioHerbalife.getDataAbertura()));
            movimetoSolicitacaoAutorizacao.setNumComanda(usuarioHerbalife.getNumeroComanda());
            movimetoSolicitacaoAutorizacao.setUsuarioAutorizado(true);
            movimetoSolicitacaoAutorizacao.setCodigoAutorizacaoMoviSolicitacaoAutorizacao(usuarioHerbalife.getCodigoAutorizacaoMoviSolicitacaoAutorizacao());
            PubMovimentosolicitacaoAutorizacaoUtil.clearFile(this);
            PubMovimentosolicitacaoAutorizacaoUtil.writeMovimetoSolicitacaoAutorizacao(this,movimetoSolicitacaoAutorizacao);

            TaskProdutosSelecionadosHerbalife taskProdutosSelecionadosHerbalife = new TaskProdutosSelecionadosHerbalife(this);
            taskProdutosSelecionadosHerbalife.execute();
        }else{
            TaskProdutosSelecionados taskProdutosSelecionados = new TaskProdutosSelecionados(this);
            taskProdutosSelecionados.execute();
        }
    }
    public void aguardandoAutorizacao(){
        setContentView(R.layout.acompanhamento_de_conta_nao_autorizada);
        movimetoSolicitacaoAutorizacaoRetorno = new MovimetoSolicitacaoAutorizacao();
        aguardandoAutorizacaoService = new AguardandoAutorizacaoService(this);
        editTextCodgioAutorizacao = (EditText) findViewById(R.id.editTextCodgioAutorizacao);
        editTextCodgioAutorizacao.requestFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        editTextCodgioAutorizacao.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(editTextCodgioAutorizacao.getText().toString().length() < 4){
                    Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Código esta inválido!",Toast.LENGTH_SHORT).show();
                }else{
                    TaskMovimentoSolicitacaoAutorizacao taskMovimentoSolicitacaoAutorizacao = new TaskMovimentoSolicitacaoAutorizacao(AcompanhamentoDeContaTelaPrincipalActivity.this);
                    taskMovimentoSolicitacaoAutorizacao.execute();
                }
                return true;
            }
        });
        btnCodigoAutorizacao = (Button) findViewById(R.id.btnCodigoAutorizacao);
        btnCodigoAutorizacao.setOnClickListener(this);
    }
    public void animacaoCarregamento(){
        setContentView(R.layout.acompanhamento_de_conta_nao_autorizada);
        final ImageView minhaImagem = (ImageView) findViewById(R.id.minhaImagem);
        final Animation efeito = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        minhaImagem.startAnimation(efeito);
    }
    public void criaDrawerLayout(){
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        mListaMenu = (ListView)findViewById(R.id.lista);
       // mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Sair da Conta","Codigo Autorização"};
        if(usuarioHerbalife.isLogado()){
            AcompanhamentoDeContaTelaPrincipalActivity.this.getSupportActionBar().setTitle("H E R B A L I F E ");//
//            AcompanhamentoDeContaTelaPrincipalActivity.this.getSupportActionBar().setSubtitle("Mesa " + movimetoSolicitacaoAutorizacao.getIdMesa() + "  Comanda  " + movimetoSolicitacaoAutorizacao.getNumComanda());
            mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Codigo Autorização: "+usuarioHerbalife.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Sair da Conta"};
        }else if(movimetoSolicitacaoAutorizacao == null){
            AcompanhamentoDeContaTelaPrincipalActivity.this.getSupportActionBar().setTitle(R.string.app_name);
            mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre"};
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
            mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre"};
            if(movimetoSolicitacaoAutorizacao.getIdEstabelecimento() == 2){
                AcompanhamentoDeContaTelaPrincipalActivity.this.getSupportActionBar().setTitle("MUMBUCA BEER");
            }if(movimetoSolicitacaoAutorizacao.getIdEstabelecimento() == 5){
                AcompanhamentoDeContaTelaPrincipalActivity.this.getSupportActionBar().setTitle("PILÃO");
            }
            AcompanhamentoDeContaTelaPrincipalActivity.this.getSupportActionBar().setSubtitle("Mesa " + movimetoSolicitacaoAutorizacao.getIdMesa());
        }else{
            AcompanhamentoDeContaTelaPrincipalActivity.this.getSupportActionBar().setTitle("Conta " );
            AcompanhamentoDeContaTelaPrincipalActivity.this.getSupportActionBar().setSubtitle("Mesa " + movimetoSolicitacaoAutorizacao.getIdMesa() + "  Comanda  " + movimetoSolicitacaoAutorizacao.getNumComanda());
            mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Codigo Autorização: "+movimetoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Sair da Conta"};
        }

        mListaMenu.setAdapter(new ArrayAdapter<String>(this, R.layout.menu_item_layout, mItensListaMenu));
        navDrawerItems = new ArrayList<NavDrawerItem>();
        // adding nav drawer items to array

        //Conta
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[0], navMenuIcons.getResourceId(1, -1)));
        //Cardapio
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[1], navMenuIcons.getResourceId(0, -1)));
        //Fechar Conta
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[2], navMenuIcons.getResourceId(2, -1)));
        //Historico
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[3], navMenuIcons.getResourceId(3, -1)));
        //Perfil
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[4], navMenuIcons.getResourceId(4, -1)));
        //Sobre
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[5], navMenuIcons.getResourceId(5, -1)));

        if(usuarioHerbalife.isLogado()){
            //Codigo autorização
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[6], navMenuIcons.getResourceId(6, -1)));
            //Sair da conta
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[7], navMenuIcons.getResourceId(7, -1)));
        }else if(movimetoSolicitacaoAutorizacao == null ){

        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado() != false){
            //Codigo autorização
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[6], navMenuIcons.getResourceId(6, -1)));
            //Sair da conta
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[7], navMenuIcons.getResourceId(7, -1)));
        }
        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);
        mListaMenu.setAdapter(adapter);

        /*Configuração do DrawerLayout*/
        mNavigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mNavigationDrawer.setDrawerShadow(R.color.transparente, GravityCompat.START);

        /*Configuração do seletor no action bar*/
        mSelectorActionBar = new ActionBarDrawerToggle(AcompanhamentoDeContaTelaPrincipalActivity.this, mNavigationDrawer, R.drawable.ic_drawer, 0, 0 ){

            @Override
            public void onDrawerClosed(View drawerView){
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
                //Evento que informará que drawer foi fechado
            }
        };
        mNavigationDrawer.setDrawerListener(mSelectorActionBar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mListaMenu.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> container, View view, int indice, long id) {
                if(indice==0){
                    conta();
                }else if(indice==1){
                    cardapio();
                }else if(indice==2){
                    fecharConta();
                }else if(indice==3){
                    historico();
                }else if(indice==4){
                    perfil();
                }else if(indice==5){
                    sobre();
                }else if(indice==6){
                    if(usuarioHerbalife.isLogado()){
                        showAlertDialogCodigoAutorizacao(usuarioHerbalife.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Use este código para autorizar mais dispositivos e acompanhar sua conta ");
                    }else{
                        showAlertDialogCodigoAutorizacao(movimetoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Use este código para autorizar mais dispositivos e acompanhar sua conta ");
                    }
                }else if(indice==7){
                    showAlertDialogSairConta(R.drawable.ic_action_about, "Mobilepub", "Você tem certeza que deseja sair da conta?", "Sim", "Não");
                }
          }
        });
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mSelectorActionBar.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mSelectorActionBar.onConfigurationChanged(newConfig);
    }
    public void conta() {
        if(usuarioHerbalife.isLogado()){
            Toast.makeText(this, "Sua conta já foi liberada!", Toast.LENGTH_LONG).show();
        }else if(movimetoSolicitacaoAutorizacao == null){
            Intent intent = new Intent(this, EstablecimentosActivity.class);
            startActivity(intent);
            finish();
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
            Toast.makeText(this, "Sua conta ainda não foi liberada!", Toast.LENGTH_LONG).show();
        }
    }
    public void cardapio(){
        if(usuarioHerbalife.isLogado()){
            Intent intent = new Intent(this, CardapioActivity.class);
            startActivity(intent);
            finish();
        }else if(movimetoSolicitacaoAutorizacao == null){
            showAlertDialogDrawerLayout(R.drawable.ic_action_about,"Mobilepub","Você não possui conta aberta no momento! \n Deseja acompanhar uma conta?","Sim","Não");
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
            Toast.makeText(this, "conta ainda não foi liberada!", Toast.LENGTH_LONG).show();
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==true){
            Intent intent = new Intent(this, CardapioActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void fecharConta(){
        if(usuarioHerbalife.isLogado()){
            Intent intent = new Intent(this, FechamentoActivity.class);
            startActivity(intent);
            finish();
        }else if(movimetoSolicitacaoAutorizacao == null){
            showAlertDialogDrawerLayout(R.drawable.ic_action_about, "Mobilepub", "Você não possui conta aberta no momento! \n Deseja acompanhar uma conta?", "Sim", "Não");
        }else if(movimetoSolicitacaoAutorizacao.isUsuarioAutorizado()==false){
            Toast.makeText(this, "Você sua conta ainda não foi liberada!", Toast.LENGTH_LONG).show();
        }else{
            Intent intent = new Intent(this, FechamentoActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void historico(){
        Intent intent = new Intent(this, HistoricoActivity.class);
        startActivity(intent);
        finish();
    }
    public void perfil(){
        if(UsuarioTXT.readUsuario(this) == null){
            Intent intent = new Intent(this,CadastroActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("br.com.cerradoinformatica.ePub.fragments.DetalhesFragments",null);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(this,PerfilActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void sobre(){
        Intent intent = new Intent(this,SobreActivity.class);
        startActivity(intent);
        finish();
    }
    public void  limpar(){
        if(usuarioHerbalife.isLogado()){
            usuarioHerbalife.setLogado(false);
            UsuarioHerbalifeTXT.writeUsuarioHerbalife(this,usuarioHerbalife);
            UtilTXT.limparTodosArquivos(this);

            Intent intent = new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
            startActivity(intent);
            finish();
        }else{
            UtilTXT.limparTodosArquivos(this);
            Intent intent = new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
            startActivity(intent);
            finish();
        }
    }
   @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mSelectorActionBar.onOptionsItemSelected(item)) {
            return true;
        }
        if(item.getItemId()==R.id.menu_load_produtos){
            if(usuarioHerbalife.isLogado()){
                TaskProdutosSelecionadosHerbalife taskProdutosSelecionadosHerbalife = new TaskProdutosSelecionadosHerbalife(this);
                taskProdutosSelecionadosHerbalife.execute();
            }else{
                TaskProdutosSelecionados taskProdutosSelecionados = new TaskProdutosSelecionados(this);
                taskProdutosSelecionados.execute();
            }
        }else if(item.getItemId()==R.id.menu_cancelar){
            PubMovimentosolicitacaoAutorizacaoUtil.clearFile(AcompanhamentoDeContaTelaPrincipalActivity.this);
            Toast.makeText(this,"Solicitação Cancelada!",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, AcompanhamentoDeContaTelaPrincipalActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View view) {
        if(view == imageViewAdicionar){
            if(UsuarioTXT.readUsuario(this) != null){
                showAlertDialogQRCODE(0,"MobilePub", usuario.getNomeClienteDispositivo() +" deseja abrir uma conta apartir do QrCode ?","Sim","Não");
            }else{
                showAlertDialogQRCODE(0,"MobilePub","Você deseja abrir uma conta apartir do QrCode ?","Sim","Não");
            }
        }else if(view == btnCodigoAutorizacao){
            if(editTextCodgioAutorizacao.getText().toString().length() < 4){
                Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Código inválido!",Toast.LENGTH_SHORT).show();
            }else{
                TaskMovimentoSolicitacaoAutorizacao taskMovimentoSolicitacaoAutorizacao = new TaskMovimentoSolicitacaoAutorizacao(AcompanhamentoDeContaTelaPrincipalActivity.this);
                taskMovimentoSolicitacaoAutorizacao.execute();
            }
        }else if(view==btnOk){
            dialog.dismiss();
        }
    }
    private class TaskMovimentoSolicitacaoAutorizacao extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskMovimentoSolicitacaoAutorizacao(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            movimetoSolicitacaoAutorizacaoRetorno = aguardandoAutorizacaoService.getMovimetoSolicitacaoAutorizacao(movimetoSolicitacaoAutorizacao);
            progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if(movimetoSolicitacaoAutorizacaoRetorno == null){
                Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Código de autorização inválido!",Toast.LENGTH_SHORT).show();
            }else if(editTextCodgioAutorizacao.getText().toString().equalsIgnoreCase(""+movimetoSolicitacaoAutorizacaoRetorno.getCodigoAutorizacaoMoviSolicitacaoAutorizacao())){
                gravaUsuarioHerbalife(movimetoSolicitacaoAutorizacaoRetorno);
                movimetoSolicitacaoAutorizacaoRetorno.setUsuarioAutorizado(true);
                PubMovimentosolicitacaoAutorizacaoUtil.clearFile(AcompanhamentoDeContaTelaPrincipalActivity.this);
                PubMovimentosolicitacaoAutorizacaoUtil.writeMovimetoSolicitacaoAutorizacao(AcompanhamentoDeContaTelaPrincipalActivity.this, movimetoSolicitacaoAutorizacaoRetorno);
                Intent intent = new Intent(AcompanhamentoDeContaTelaPrincipalActivity.this, AcompanhamentoDeContaTelaPrincipalActivity.class);
                startActivity(intent);
                Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Mesa autorizada!",Toast.LENGTH_SHORT).show();
                finish();
            }else {
                Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Código de autorização inválido!!",Toast.LENGTH_SHORT).show();
            }
          /*  if(!movimetoSolicitacaoAutorizacaoRetorno.getDataNegacao().equalsIgnoreCase("null")){
                PubMovimentosolicitacaoAutorizacaoUtil.clearFile(AcompanhamentoDeContaTelaPrincipalActivity.this);
                Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Solicitação negada!",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(AcompanhamentoDeContaTelaPrincipalActivity.this, AcompanhamentoDeContaTelaPrincipalActivity.class);
                startActivity(intent);
                finish();
            }else if(!movimetoSolicitacaoAutorizacaoRetorno.getDataAutorizacao().equalsIgnoreCase("null")){
                PubMovimentosolicitacaoAutorizacaoUtil.writeMovimetoSolicitacaoAutorizacao(AcompanhamentoDeContaTelaPrincipalActivity.this, movimetoSolicitacaoAutorizacaoRetorno);
                Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Solicitação Autorizada!",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(AcompanhamentoDeContaTelaPrincipalActivity.this, AcompanhamentoDeContaTelaPrincipalActivity.class);
                startActivity(intent);
                finish();
            }else if(movimetoSolicitacaoAutorizacao.getDataAutorizacao().equalsIgnoreCase("null") || movimetoSolicitacaoAutorizacaoRetorno.getDataAutorizacao().equalsIgnoreCase("null")){
                Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Sua Mesa Ainda não foi autorizada!",Toast.LENGTH_SHORT).show();
            }else if(!movimetoSolicitacaoAutorizacao.getDataNegacao().equalsIgnoreCase("null") || !movimetoSolicitacaoAutorizacaoRetorno.getDataAbertura().equalsIgnoreCase("null")){
                Toast.makeText(AcompanhamentoDeContaTelaPrincipalActivity.this,"Mesa Negada!",Toast.LENGTH_LONG).show();
                limpar();
            }*/
        }
    };
    public void gravaUsuarioHerbalife(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
        if(movimetoSolicitacaoAutorizacao.getIdEstabelecimento() == 4 && UsuarioHerbalifeTXT.readUsuarioHerbalife(AcompanhamentoDeContaTelaPrincipalActivity.this) == null){
            usuarioHerbalife = new UsuarioHerbalife();
            usuarioHerbalife.setIdEstabelecimento(movimetoSolicitacaoAutorizacao.getIdEstabelecimento());
            usuarioHerbalife.setIdMesa(movimetoSolicitacaoAutorizacao.getIdMesa());
            usuarioHerbalife.setDataAbertura(DatasUtils.getStringDate(movimetoSolicitacaoAutorizacao.getDataAbertura()));
            usuarioHerbalife.setNumeroComanda(movimetoSolicitacaoAutorizacao.getNumComanda());
            usuarioHerbalife.setCodigoAutorizacaoMoviSolicitacaoAutorizacao(movimetoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao());
            usuarioHerbalife.setLogado(true);
            UsuarioHerbalifeTXT.writeUsuarioHerbalife(AcompanhamentoDeContaTelaPrincipalActivity.this,usuarioHerbalife);
        }
    }
    private class TaskProdutosSelecionados extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskProdutosSelecionados(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            lstMovimentoProduto = movimentoProdutoSrv.getProdutoSelecionados(movimetoSolicitacaoAutorizacao);
            progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if(lstMovimentoProduto == null || lstMovimentoProduto.size() <= 0){
                imageViewContaVazia.setVisibility(View.VISIBLE);
                textViewPequena.setVisibility(View.VISIBLE);
                textViewGrande.setVisibility(View.VISIBLE);
                listViewProdutosSelec.setVisibility(View.INVISIBLE);
            }else{
                //Collections.reverse(lstMovimentoProduto);
                textViewPequena.setVisibility(View.INVISIBLE);
                textViewGrande.setVisibility(View.INVISIBLE);
                imageViewContaVazia.setVisibility(View.INVISIBLE);
                listViewProdutosSelec.setVisibility(View.VISIBLE);
                listViewProdutosSelec.setAdapter(new MovimentoProdutoAdapter(AcompanhamentoDeContaTelaPrincipalActivity.this, lstMovimentoProduto));
                textTotalValor.setText(String.format("R$ %.2f",EpubUtils.getValorConta(AcompanhamentoDeContaTelaPrincipalActivity.this)));
                calculaConta();
            }
        }
    };
    private class TaskProdutosSelecionadosHerbalife extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskProdutosSelecionadosHerbalife(Context context){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            lstMovimentoProduto = movimentoProdutoSrv.getProdutoSelecionadosHerbalife(usuarioHerbalife);
            progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            if(lstMovimentoProduto == null || lstMovimentoProduto.size() <= 0){
                imageViewContaVazia.setVisibility(View.VISIBLE);
                textViewPequena.setVisibility(View.VISIBLE);
                textViewGrande.setVisibility(View.VISIBLE);
                listViewProdutosSelec.setVisibility(View.INVISIBLE);
            }else{
                //Collections.reverse(lstMovimentoProduto);
                textViewPequena.setVisibility(View.INVISIBLE);
                textViewGrande.setVisibility(View.INVISIBLE);
                imageViewContaVazia.setVisibility(View.INVISIBLE);
                listViewProdutosSelec.setVisibility(View.VISIBLE);
                listViewProdutosSelec.setAdapter(new MovimentoProdutoAdapter(AcompanhamentoDeContaTelaPrincipalActivity.this, lstMovimentoProduto));
                textTotalValor.setText(String.format("R$ %.2f",EpubUtils.getValorConta(AcompanhamentoDeContaTelaPrincipalActivity.this)));
                calculaConta();
            }
        }
    };
    private class TaskVerificaVersao extends AsyncTask<String, String, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskVerificaVersao(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            versao = verificaAPIService.getVerificaAPISerive();
            progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
           if(versao != null){
               versao = versao.substring(3,versao.length()-4);
                if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_DESCONHECIDO")){
                   showAlertDialogErro(0, "MobilePub", "Erro de Conexão","Sair do aplicativo!");
               }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR")){
                   showAlertDialogErro(0, "MobilePub", "Erro ao conectar ao servidor!","Sair do aplicativo!");
               }else  if(HttpUtil.MENSAGEM.equalsIgnoreCase("TIME_OUT")){
                   showAlertDialogErro(0, "MobilePub", "Time out!","Sair do aplicativo!");
               }else if(!versao.equalsIgnoreCase(VERSAO_API_COMPATIVEL)){
                    showAlertDialogAtualizar(0, "Mobilepub", "Existe versão mais nova do aplicativo, favor atualizalo.\n" +
                            "Seu aplicativo esta na versão antigo e pode apresentar falhas.", "Atualizar aplicativo");
                }
           }else{
               showAlertDialogAtualizar(0, "MobilePub", "Problemas técnicos!","OK");
           }
        }
    };
    private void calculaConta(){
        double valor=0;
        for (MovimentoProduto movimento : lstMovimentoProduto) {
            valor+=movimento.getValorUnitario()*movimento.getQuantidade();
        }
        EpubUtils.setValorConta(this, valor);
    }
    @Override
    public void onBackPressed() {
        showAlertDialogSairDoAplicativo(R.drawable.ic_action_about,"Mobilepub","Você deseja Sair do aplicativo?","Sim","Não");
    }
    public void showAlertDialogSairConta(int name_incone, String titulo, String mensagem, String nomeButtonPositive,String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                limpar();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void showAlertDialogCodigoAutorizacao(int codigoAutorizacao,String mensagem){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_codigo_autorizacao);
        dialog.setTitle("Código de autorização");
        textViewCodigo = (TextView) dialog.findViewById(R.id.textViewCodigo);
        textViewTexto = (TextView) dialog.findViewById(R.id.textViewTexto);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);

        textViewCodigo.setText(""+codigoAutorizacao);
        textViewTexto.setText(mensagem);
        dialog.show();
        dialog.setCancelable(false);
    }
    public void showAlertDialogDrawerLayout(int nomeIcone,String titulo,String mensagem,String nomeButtonPositive,String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(nomeIcone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(AcompanhamentoDeContaTelaPrincipalActivity.this, AbrirConta.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void showAlertDialogSairDoAplicativo(int nomeIcone,String titulo,String mensagem,String nomeButtonPositive,String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(nomeIcone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void showAlertDialogAtualizar(int nomeIcone,String titulo,String mensagem,String nomeButtonPositive){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(nomeIcone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                linkLoja();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void showAlertDialogErro(int nomeIcone,String titulo,String mensagem,String nomeButtonPositive){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(nomeIcone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void showAlertDialogQRCODE(int nomeIcone,String titulo,String mensagem,String nomeButtonPositive, String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(nomeIcone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent =  new Intent(AcompanhamentoDeContaTelaPrincipalActivity.this,QRCode.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                conta();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    private void linkLoja(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=br.com.cerradoinformatica.ePub.activity&hl=pt_BR"));
        startActivity(intent);
        finish();
    }
}