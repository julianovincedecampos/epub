package br.com.cerradoinformatica.ePub.activity;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

/**
 * Created by danilo on 05/11/13.
 */
public class CustomActionProvider extends ActionProvider {
    Context context;
    public CustomActionProvider(Context context) {
        super(context);
        this.context=context;

    }

    @Override
    public View onCreateActionView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.spinner_item,null);
        ImageButton img = (ImageButton) v.findViewById(R.id.imageItem);
        img.setImageResource(R.drawable.ic_action_view_as_grid);
        return v;
    }

    @Override
    public View onCreateActionView(MenuItem forItem) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.spinner_item,null);
        ImageView img = (ImageButton) v.findViewById(R.id.imageItem);
        img.setImageResource(R.drawable.ic_action_view_as_grid);
        return v;
    }
}
