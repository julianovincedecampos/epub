package br.com.cerradoinformatica.ePub.activity;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observer;

import br.com.cerradoinformatica.ePub.adapter.CardapioNavigationAdapter;
import br.com.cerradoinformatica.ePub.adapter.NavDrawerItem;
import br.com.cerradoinformatica.ePub.adapter.NavDrawerListAdapter;
import br.com.cerradoinformatica.ePub.fragments.DetalhesFragments;
import br.com.cerradoinformatica.ePub.fragments.FragmentObserver;
import br.com.cerradoinformatica.ePub.fragments.GruposFragment;
import br.com.cerradoinformatica.ePub.fragments.ProdutosFragments;
import br.com.cerradoinformatica.ePub.model.Mesa;
import br.com.cerradoinformatica.ePub.model.MovimentoMesa;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.Usuario;
import br.com.cerradoinformatica.ePub.model.UsuarioHerbalife;
import br.com.cerradoinformatica.ePub.txts.PubMovimentosolicitacaoAutorizacaoUtil;
import br.com.cerradoinformatica.ePub.funcionalidades.NavigationPagerActivitImp;
import br.com.cerradoinformatica.ePub.funcionalidades.Observavel;
import br.com.cerradoinformatica.ePub.txts.UsuarioHerbalifeTXT;
import br.com.cerradoinformatica.ePub.txts.UsuarioTXT;
import br.com.cerradoinformatica.ePub.txts.UtilTXT;

/**
 * Created by juliano on 24/09/13.
 */
public class CardapioActivity extends ActionBarActivity implements NavigationPagerActivitImp,Observavel,Serializable, View.OnClickListener {

    private ViewPager pager;
    private CardapioNavigationAdapter pagerAdapter;
    private ActionBar actionBar;
    private static String TAG=" << CardapioActivity >>";
    private ArrayList<FragmentObserver> arrFragment;
    private final static  String KEY_NAVIGATION ="navigation";
    private final static  String KEY_OBSERVAVEL="observavel";
    private MovimetoSolicitacaoAutorizacao movimentoSolicitacaoAutorizacao;
    private GruposFragment gruposFragment;
    private ProdutosFragments produtosFragments;
    private DetalhesFragments detalhesFragments;
    private Mesa mesa;
    private MovimentoMesa movimentoMesa;
    private AlertDialog alertDialog;

    //Drawer
    private String[] mItensListaMenu;
    private ListView mListaMenu;
    private DrawerLayout mNavigationDrawer;
    private ActionBarDrawerToggle mSelectorActionBar;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    //Drawer

    //Dialog
    private TextView textViewCodigo,textViewTexto;
    private Button btnOk;
    private Dialog dialog;
    //Dialog

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acompanhamento);
        pager = (ViewPager) findViewById(R.id.pager);
        Bundle bundle = new Bundle();
        //Teste
        //bundle.putSerializable(KEY_NAVIGATION,(NavigationPagerActivitImp)this);
        //bundle.putSerializable(KEY_OBSERVAVEL,(Observavel)this);
        //Teste
        movimentoSolicitacaoAutorizacao  = PubMovimentosolicitacaoAutorizacaoUtil.readMovimetoSolicitacaoAutorizacao(this);

        arrFragment = new ArrayList<FragmentObserver>();

        if(savedInstanceState!=null) {
           gruposFragment = (GruposFragment) getSupportFragmentManager().getFragment(savedInstanceState,"grupos");
           produtosFragments=(ProdutosFragments)getSupportFragmentManager().getFragment(savedInstanceState,"produtos") ;
           detalhesFragments = (DetalhesFragments)getSupportFragmentManager().getFragment(savedInstanceState,"detalhes");
            //Recebendo  TelaPrincipal e passando para fragmentDetalhes
        }
            if(gruposFragment==null){
                gruposFragment = new GruposFragment();
                gruposFragment.setArguments(bundle);
            }

            if(produtosFragments==null){
                produtosFragments = new ProdutosFragments();
                produtosFragments.setArguments(bundle);
            }

            if(detalhesFragments==null){
                detalhesFragments = new DetalhesFragments();
                Log.i(TAG, "movimentoSolicitacaoAutorizacao SENDO PASSADO PARA --> DetalhesFragment" + movimentoSolicitacaoAutorizacao);
                detalhesFragments.setArguments(bundle);
            }


        arrFragment.add(gruposFragment);
        arrFragment.add( produtosFragments);
        arrFragment.add(detalhesFragments);


        Log.i(TAG,"OmCreate << Activity >>");
        pagerAdapter = new CardapioNavigationAdapter(getSupportFragmentManager(),this,arrFragment);
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(pagerAdapter);

        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayHomeAsUpEnabled(true);


        Tab tab = actionBar.newTab();
        tab.setText("Grupos");
        tab.setTabListener(pagerAdapter);
        actionBar.addTab(tab);
        Tab tab2 = actionBar.newTab();
        tab2.setText("Produtos");
        tab2.setTabListener(pagerAdapter);
        actionBar.addTab(tab2);
        Tab tab3 = actionBar.newTab();
        tab3.setText("Detalhes");
        tab3.setTabListener(pagerAdapter);
        actionBar.addTab(tab3);

        pager.setOnPageChangeListener(pagerAdapter);
        pager.setCurrentItem(1);
        actionBar.setSelectedNavigationItem(0);

        criaDrawerLayout();
    }

    @Override
    protected void onRestart() {
        Log.e(TAG + "_Juliano", "onRestart");
        super.onRestart();
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if(gruposFragment.isAdded())
        getSupportFragmentManager().putFragment(outState,"grupos",gruposFragment);

        if(produtosFragments.isAdded())
        getSupportFragmentManager().putFragment(outState,"produtos",produtosFragments);

        if(detalhesFragments.isAdded())
        getSupportFragmentManager().putFragment(outState,"detalhes",detalhesFragments);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null){
            Log.i(TAG,"onRestoreInstanceState");
        }
    }

    //interface CardapioNavigationAdapter

    @Override
    public void changeTab(int index) {
        actionBar.setSelectedNavigationItem(index);
        if(index ==0){
            Bundle bundle = new Bundle();
            bundle.putString(FragmentObserver.ESTADO_INICIAL, "ESTADO_INI");
            notifyObservers(bundle);
        }
    }

    @Override
    public void changePage(int index) {
      Log.i(TAG,"ChngePage index : " +index);
        pager.setCurrentItem(index);
    }

    @Override
    public void next() {
        if(pager.getCurrentItem()<pagerAdapter.getCount()){
            pager.setCurrentItem(pager.getCurrentItem() + 1);
            actionBar.setSelectedNavigationItem(pager.getCurrentItem());
        }
    }

    @Override
    public void previous() {
        if(pager.getCurrentItem()>0){
            pager.setCurrentItem(pager.getCurrentItem()-1);
            actionBar.setSelectedNavigationItem(pager.getCurrentItem());
        }
    }

    @Override
    public void onBackPressed() {
        if(pager.getCurrentItem()==0){
            super.onBackPressed();
        }else{
            pager.setCurrentItem(pager.getCurrentItem()-1);
        }
    }

    //interface Observavel
    @Override
    public void addObserver(Observer o) {
        arrFragment.add((FragmentObserver) o);
    }

    @Override
    public void removeObserver(Observer o) {
        arrFragment.remove(o);
    }

    @Override
    public void notifyObservers(Bundle bundle) {
        for(FragmentObserver o :arrFragment){
            o.update(this,bundle);
        }
    }


    public void criaDrawerLayout(){
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        mListaMenu = (ListView)findViewById(R.id.lista);
        // mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Sair da Conta","Codigo Autorização"};
        getSupportActionBar().setTitle("Cardápio");//por na string.xml
        mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Codigo Autorização: " + movimentoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Sair da Conta"};

        mListaMenu.setAdapter(new ArrayAdapter<String>(this, R.layout.menu_item_layout, mItensListaMenu));
        navDrawerItems = new ArrayList<NavDrawerItem>();
        // adding nav drawer items to array

        //Conta
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[0], navMenuIcons.getResourceId(1, -1)));
        //Cardapio
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[1], navMenuIcons.getResourceId(0, -1)));
        //Fechar Conta
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[2], navMenuIcons.getResourceId(2, -1)));
        //Historico
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[3], navMenuIcons.getResourceId(3, -1)));
        //Perfil
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[4], navMenuIcons.getResourceId(4, -1)));
        //Sobre
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[5], navMenuIcons.getResourceId(5, -1)));

        if(movimentoSolicitacaoAutorizacao == null ){

        }else if(movimentoSolicitacaoAutorizacao.isUsuarioAutorizado()!=false){
            //Codigo autorização
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[6], navMenuIcons.getResourceId(6, -1)));
            //Sair da conta
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[7], navMenuIcons.getResourceId(7, -1)));
        }

        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);
        mListaMenu.setAdapter(adapter);

        /*Configuração do DrawerLayout*/
        mNavigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mNavigationDrawer.setDrawerShadow(R.color.transparente, GravityCompat.START);

        /*Configuração do seletor no action bar*/
        mSelectorActionBar = new ActionBarDrawerToggle(CardapioActivity.this, mNavigationDrawer, R.drawable.ic_drawer, 0, 0 ){


            @Override
            public void onDrawerClosed(View drawerView){
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
                //Evento que informará que drawer foi fechado
            }
        };
        mNavigationDrawer.setDrawerListener(mSelectorActionBar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mListaMenu.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> container, View view, int indice, long id) {
                if(indice==0){
                    conta();
                }else if(indice==1){
                    cardapio();
                }else if(indice==2){
                    fecharConta();
                }else if(indice==3){
                    historico();
                }else if(indice==4){
                    perfil();
                }else if(indice==5){
                    sobre();
                }else if(indice==6){
                    showAlertDialogCodigoAutorizacao(movimentoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Use este código para autorizar mais dispositivos e acompanhar sua conta ");
                }else if(indice==7){
                    showAlertDialogSairConta(R.drawable.ic_action_about, "Mobilepub", "Você tem certeza que deseja sair da conta?", "Sim", "Não");
                }
            }
        });
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mSelectorActionBar.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mSelectorActionBar.onConfigurationChanged(newConfig);
    }
    public void conta() {
        Intent intent= new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }
    public void cardapio(){

    }
    public void fecharConta(){
        Intent intent = new Intent(this, FechamentoActivity.class);
        startActivity(intent);
        finish();
    }
    public void historico(){
        Intent intent = new Intent(this, HistoricoActivity.class);
        startActivity(intent);
        finish();
    }
    public void perfil(){
        if(UsuarioTXT.readUsuario(this) == null){
            Intent intent = new Intent(this,CadastroActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("br.com.cerradoinformatica.ePub.fragments.DetalhesFragments",null);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(this,PerfilActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void sobre(){
        Intent intent = new Intent(this,SobreActivity.class);
        startActivity(intent);
        finish();
    }
    public void  limpar(){
        if(UsuarioHerbalifeTXT.readUsuarioHerbalife(this) != null){
            if(UsuarioHerbalifeTXT.readUsuarioHerbalife(this).isLogado()){
                UsuarioHerbalife usuarioHerbalife = UsuarioHerbalifeTXT.readUsuarioHerbalife(this);
                usuarioHerbalife.setLogado(false);
                UsuarioHerbalifeTXT.writeUsuarioHerbalife(this,usuarioHerbalife);
            }
        }
        UtilTXT.limparTodosArquivos(CardapioActivity.this);
        Intent intent= new Intent(CardapioActivity.this,AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mSelectorActionBar.onOptionsItemSelected(item)){
            //Se for um click no ícone da aplicação, informa o seletor e encerra o tratamento
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showAlertDialogSairConta(int name_incone, String titulo, String mensagem, String nomeButtonPositive,String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                limpar();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void showAlertDialogCodigoAutorizacao(int codigoAutorizacao,String mensagem){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_codigo_autorizacao);
        dialog.setTitle("Código de autorização");
        textViewCodigo = (TextView) dialog.findViewById(R.id.textViewCodigo);
        textViewTexto = (TextView) dialog.findViewById(R.id.textViewTexto);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(CardapioActivity.this);

        textViewCodigo.setText(""+codigoAutorizacao);
        textViewTexto.setText(mensagem);
        dialog.show();
        dialog.setCancelable(false);
    }

    @Override
    public void onClick(View view) {
        if(view==btnOk){
            dialog.dismiss();
        }
    }
}
