package br.com.cerradoinformatica.ePub.activity;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;

import br.com.cerradoinformatica.ePub.adapter.EstabelecimentoAdapter;
import br.com.cerradoinformatica.ePub.funcionalidades.Transacao;
import br.com.cerradoinformatica.ePub.funcionalidades.TransacaoTask;
import br.com.cerradoinformatica.ePub.model.Estabelecimento;
import br.com.cerradoinformatica.ePub.model.UsuarioHerbalife;
import br.com.cerradoinformatica.ePub.service.EtabelecimentoSrv;
import br.com.cerradoinformatica.ePub.txts.UsuarioHerbalifeTXT;
import util.DownloadsImagens;
import util.HttpUtil;
import util.URLUtil;

/**
 * Created by juliano on 26/02/14.
 */
public class EstablecimentosActivity extends ActionBarActivity implements Transacao, AdapterView.OnItemClickListener {

    private ListView lstEstabelecimentos;
    private List<Estabelecimento> estabelecimentos;
    private EtabelecimentoSrv estabelecimentoSrv;
    private UsuarioHerbalife usuarioHerbalife;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.estabelecimentos);
        incializaObjetos();
    }
    public void incializaObjetos(){

        lstEstabelecimentos = (ListView) findViewById(R.id.listEstabelecimento);
        lstEstabelecimentos.setOnItemClickListener(this);
        estabelecimentoSrv = new EtabelecimentoSrv(this);

        TransacaoTask tks = new TransacaoTask(this, this, R.string.aguarde);
        tks.execute();

    }
    @Override
    public void executar() throws Exception {
        estabelecimentos = estabelecimentoSrv.getEstabelecimentos();
    }

    @Override
    public void atualizarView() {
        lstEstabelecimentos.setAdapter(new EstabelecimentoAdapter(this, estabelecimentos));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int posicao, long l) {
        Estabelecimento e = (Estabelecimento) parent.getAdapter().getItem(posicao);

        usuarioHerbalife = UsuarioHerbalifeTXT.readUsuarioHerbalife(this);
        if(usuarioHerbalife != null && e.getCodigo() == 4){
            UsuarioHerbalifeTXT.clearFile(this);
            usuarioHerbalife.setLogado(true);
            UsuarioHerbalifeTXT.writeUsuarioHerbalife(this,usuarioHerbalife);

            Intent intent = new Intent(this, AcompanhamentoDeContaTelaPrincipalActivity.class);
            startActivity(intent);
            finish();
        }else{
            Intent it = new Intent(this,SelecionarMesa.class);
            it.putExtra("IdEstabelecimento", e.getCodigo());
            startActivityForResult(it,0);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }
}