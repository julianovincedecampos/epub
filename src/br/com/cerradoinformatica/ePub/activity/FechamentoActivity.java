package br.com.cerradoinformatica.ePub.activity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import br.com.cerradoinformatica.ePub.adapter.LinearLayoutAdapter;
import br.com.cerradoinformatica.ePub.adapter.NavDrawerItem;
import br.com.cerradoinformatica.ePub.adapter.NavDrawerListAdapter;
import br.com.cerradoinformatica.ePub.model.FechamentoConta;
import br.com.cerradoinformatica.ePub.funcionalidades.Listas;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.RetornoPostFechamento;
import br.com.cerradoinformatica.ePub.model.UsuarioHerbalife;
import br.com.cerradoinformatica.ePub.service.FechamentoContaSrv;
import br.com.cerradoinformatica.ePub.service.MovimentoProdutoSrv;
import br.com.cerradoinformatica.ePub.funcionalidades.TransacaoTask;
import br.com.cerradoinformatica.ePub.model.DateUtil;
import br.com.cerradoinformatica.ePub.funcionalidades.GerenciadorAlertDialog;
import br.com.cerradoinformatica.ePub.txts.DetalhesHistoricoDasContasTXT;
import br.com.cerradoinformatica.ePub.txts.UsuarioHerbalifeTXT;
import br.com.cerradoinformatica.ePub.txts.UsuarioTXT;
import br.com.cerradoinformatica.ePub.txts.UtilTXT;
import util.HttpUtil;
import br.com.cerradoinformatica.ePub.txts.PubMovimentosolicitacaoAutorizacaoUtil;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.view.KeyEvent;

import android.view.Menu;
import android.view.MenuInflater;

import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;


/**
 * Created by Juliano Vince de Campos on 25/09/13.
 */
public class FechamentoActivity extends ActionBarActivity implements OnEditorActionListener, View.OnClickListener{

    private FechamentoConta fechamentoConta;
    private FechamentoContaSrv fechamentoContaSrv;
    private final int FECHAR_CONTA=120;
    private MovimentoProduto movimentoProduto;
    private Listas listas;
    private LinearLayout lProdutos;
    private List<MovimentoProduto> lstMovimentoProdutos;
    private TextView textNumeroDaComanda;
    private TextView textSubTotalValue;
    private TextView textTxServicoValue;
    private TextView textValorPagarValue;
    private TextView textValorPessoaValue;
    private TextView textTitulo;
    private TextView textQuantidadePessoa;
    private TransacaoTask task;
    private EditText editQtdPessoas;
    private  double subTotal;
    private double txServico;
    private double valorPagar;
    private static final String TAG = "FechamentoActivity";
    private MovimentoProdutoSrv movimentoProdutoSrv;
    private MovimetoSolicitacaoAutorizacao movimentoSolicitacaoAutorizacao;
    private AlertDialog alertDialog;
    private int somaQuantidade;
    private ActionBar actionBar;
    private int quantidadePessoasParaRateio = 0;
    private EditText editTextValorTroco;
    private double trocoPara = 0.0;
    private RetornoPostFechamento retornoPostFechamento;

    @Override
    public void onClick(View view) {
        if(view == btnOk){
            dialog.dismiss();
        }
    }

    private enum Acao{POST_FECHAMENTO_CONTA, GET_LISTA_PRODUTO};
    private Acao acao;
    private GerenciadorAlertDialog gerenciadorAlertDialog;
    private BigDecimal valorTotal;
    //Drawer
    private String[] mItensListaMenu;
    private ListView mListaMenu;
    private DrawerLayout mNavigationDrawer;
    private ActionBarDrawerToggle mSelectorActionBar;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    //Drawer
    //Dialog
    private TextView textViewCodigo,textViewTexto;
    private Button btnOk;
    private Dialog dialog;
    //Dialog

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        listas = new Listas();

        setContentView(R.layout.fechamento_conta);

        getSupportActionBar().setTitle(R.string.fechamentoConta);

        actionBar = getSupportActionBar();
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayHomeAsUpEnabled(true);

        this.fechamentoConta = new FechamentoConta();
        this.fechamentoContaSrv = new FechamentoContaSrv(this);

       /* acao = acao.GET_LISTA_PRODUTO;

        task = new TransacaoTask(this, this,R.string.aguarde);

        task.execute();*/
        instanciaElementos();
        TaskGETProduto taskGETProduto = new TaskGETProduto(this);
        taskGETProduto.execute();
        criaDrawerLayout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_fechamento, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.pagamentoDinheiro){
            //opcaoPagamento(1);
            exibeDialog();
        }else if(item.getItemId() == R.id.pagamentoCartao){
            opcaoPagamento();
        }
        if(mSelectorActionBar.onOptionsItemSelected(item)){
            //Se for um click no ícone da aplicação, informa o seletor e encerra o tratamento
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void instanciaElementos(){
        retornoPostFechamento = new RetornoPostFechamento();
        movimentoProduto = new MovimentoProduto();
        movimentoProdutoSrv = new MovimentoProdutoSrv(this);
        gerenciadorAlertDialog = new GerenciadorAlertDialog(this);
        this.movimentoSolicitacaoAutorizacao  = PubMovimentosolicitacaoAutorizacaoUtil.readMovimetoSolicitacaoAutorizacao(this);
        lProdutos = (LinearLayout) findViewById(R.id.lLayoutProdutos);

        textSubTotalValue = (TextView) findViewById(R.id.textValorSubTotalValue);
        textTxServicoValue = (TextView) findViewById(R.id.textTaxaServicoValue);
        textValorPagarValue = (TextView) findViewById(R.id.textValorPagarValue);
        textValorPessoaValue=(TextView) findViewById(R.id.textValorPessoas);
       // textTitulo = (TextView) findViewById(R.id.textTitulo);
        textQuantidadePessoa = (TextView) findViewById(R.id.textQuantidadePessoa);

        editTextValorTroco = (EditText) findViewById(R.id.editTextValorTroco);


        editQtdPessoas=(EditText) findViewById(R.id.editQtdPessoas);
        editQtdPessoas.setOnEditorActionListener(this);



        textNumeroDaComanda = (TextView) findViewById(R.id.textNumeroDaComanda);
        textNumeroDaComanda.setText("Parcial - Comanda " + movimentoSolicitacaoAutorizacao.getNumComanda() + " / Mesa " + movimentoSolicitacaoAutorizacao.getIdMesa() );
        this.somaQuantidade=0;
    }
    public void exibeDialog(){
        final Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.customdialog_troco);

        //define o título do Dialog
        dialog.setTitle("Informe o valor para troco!");

        //instancia os objetos que estão no layout customdialog_troco.xml
        final Button confirmar = (Button) dialog.findViewById(R.id.btn_Confirmar);
        final Button cancelar = (Button) dialog.findViewById(R.id.btn_Cancelar);
        final EditText editText = (EditText) dialog.findViewById(R.id.editTextValorTroco);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if(editText.getText().toString().equalsIgnoreCase("")){
                    tipoPagamento(1);
                }else{
                    trocoPara = Double.parseDouble(editText.getText().toString());
                    valorTotal = new BigDecimal(calculaValor()).setScale(3, RoundingMode.HALF_EVEN);
                }
                if(trocoPara == 0){
                    tipoPagamento(1);
                }else if(valorTotal.doubleValue() <= trocoPara ){
                    tipoPagamento(1);
                }else{
                    exibirTextoDialog();
                }
                //finaliza o dialog
                dialog.dismiss();
                return true;
            }
        });

        final TextView tvMens = (TextView) dialog.findViewById(R.id.tvMens);

        cancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //finaliza o dialog
                dialog.dismiss();
            }
        });

        confirmar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(editText.getText().toString().equalsIgnoreCase("")){
                    tipoPagamento(1);
                }else{
                    trocoPara = Double.parseDouble(editText.getText().toString());
                    valorTotal = new BigDecimal(calculaValor()).setScale(3, RoundingMode.HALF_EVEN);
                    if(trocoPara == 0){
                        tipoPagamento(1);
                    }else if(valorTotal.doubleValue() <= trocoPara ){
                        tipoPagamento(1);
                    }else{
                        exibirTextoDialog();
                    }
                }
                //finaliza o dialog
                dialog.dismiss();
            }
        });
        //exibe na tela o dialog
        dialog.show();
    }
    public void exibirTextoDialog(){
        Toast.makeText(this,"Valor não e valido para troco!",Toast.LENGTH_LONG).show();
    }

    public void opcaoPagamento(){
        //Cria o gerador do AlertDialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //define o titulo
            builder.setTitle("Confirmar pagamento em cartão?");

            //define um botão como cartão
            builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    tipoPagamento(2);
                }
            });
            builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //faz nada
                }
            });
        //cria o AlertDialog
        alertDialog = builder.create();
        //Exibe
        alertDialog.show();
    }
    public void tipoPagamento(int tipoPagamento){
        this.fechamentoConta.setId(1);
        this.fechamentoConta.setIdUsuario(0);
        this.fechamentoConta.setIdEstabelecimento(movimentoSolicitacaoAutorizacao.getIdEstabelecimento());
        this.fechamentoConta.setIdMesa(movimentoSolicitacaoAutorizacao.getIdMesa());
        this.fechamentoConta.setDataConta(DateUtil.getWebApiDate(movimentoSolicitacaoAutorizacao.getDataAbertura()));
        this.fechamentoConta.setNumeroComanda(movimentoSolicitacaoAutorizacao.getNumComanda());
        this.fechamentoConta.setIdUsuarioDispositivo(144);

        Date dataHoje = new Date();
        SimpleDateFormat formataData = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String data = formataData.format(dataHoje);

        this.fechamentoConta.setDataSolicitacao(data);
        this.fechamentoConta.setSequencia(1);
        this.fechamentoConta.setParcial(false);
        if(tipoPagamento==1){
            this.fechamentoConta.setIdFormaPagamento(tipoPagamento);
        }
        if(tipoPagamento==2){
            this.fechamentoConta.setIdFormaPagamento(tipoPagamento);
        }
        if(!editQtdPessoas.getText().toString().equals("")){
            quantidadePessoasParaRateio = Integer.parseInt(editQtdPessoas.toString());
        }

        this.fechamentoConta.setTrocoPara(trocoPara);
        this.fechamentoConta.setPessoasParaRateio(quantidadePessoasParaRateio);
        this.fechamentoConta.setDataAutorizacao(null);
        this.fechamentoConta.setAutorizadoPor(0);
        this.fechamentoConta.setDataNegacao(null);
        this.fechamentoConta.setNegadoPor(0);
        this.fechamentoConta.setSituacaoAtual("SOLICITADA");

        TaskPostFechamentoConta  taskPostFechamentoConta= new TaskPostFechamentoConta(this);
        taskPostFechamentoConta.execute();
    }
    public double calculaSubTotal(){
        double total=0;
        for(MovimentoProduto  mov :lstMovimentoProdutos){
            total+=mov.getValorUnitario()*mov.getQuantidade();
        }
        return subTotal= total;
    }
    public double calculaValor(){
        return valorPagar=subTotal+txServico;
    }
    public double calculaTxServico(double valor){
        double d=valor *(10f/100f);
        return txServico=d;
    }
    public double calculaValorPessoa(){
        if(!editQtdPessoas.getText().toString().equals("")||editQtdPessoas.getText()==null){
            valorPagar= calculaValor()/this.quantidadePessoasParaRateio;
            textQuantidadePessoa.setText("Valor para: "+editQtdPessoas.getText()+" pessoas ");
        }else{
            textQuantidadePessoa.setText("Valor por pessoa: ");
        }
        return valorPagar;
    }
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(actionId==EditorInfo.IME_ACTION_DONE){
            if(editQtdPessoas.getText().toString().length() > 0){
                this.quantidadePessoasParaRateio = Integer.parseInt(editQtdPessoas.getText().toString());
                textValorPessoaValue.setText(String.format("%.2f", calculaValorPessoa()).replace(".",","));
                editQtdPessoas.setText("");
            }
        }
        return false;
    }
    /*  @Override
      public void executar() throws Exception {
          switch(acao){
              case GET_LISTA_PRODUTO:
                  listaProdutos();
                  break;
          }
      }*/
    private class TaskPostFechamentoConta extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskPostFechamentoConta(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Void... params) {
            retornoPostFechamento = fechamentoContaSrv.postFechamentoContaService(fechamentoConta);
            return null;
        }
        @Override
        protected void onProgressUpdate(Void... values) {

        }
        @Override
        protected void onPostExecute(String result) {
            if(movimentoProduto == null){
                if(HttpUtil.MENSAGEM.equalsIgnoreCase("304")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(FechamentoActivity.this);
                    builder.setTitle("Atenção!");
                    builder.setMessage("Mesa: " + movimentoSolicitacaoAutorizacao.getIdMesa() + " está com a Comanda: " + movimentoSolicitacaoAutorizacao.getNumComanda() + " fechada!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            direciona();
                        }});
                    //cria o AlertDialog
                    alertDialog = builder.create();
                    //Exibe
                    alertDialog.show();
                }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(FechamentoActivity.this);
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro de Comunicação com servidor!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            direciona();
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("TIME_OUT")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(FechamentoActivity.this);
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro Time out!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            direciona();
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_DESCONHECIDO")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(FechamentoActivity.this);
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro de Conexão");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            direciona();
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }
            }else{
                Toast.makeText(FechamentoActivity.this,"Solicitação de fechamento de conta realizado!",Toast.LENGTH_LONG).show();
                UtilTXT.limparTodosArquivos(FechamentoActivity.this);
                direciona();
            }
        }
    };
    private class TaskGETProduto extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskGETProduto(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            lstMovimentoProdutos = fechamentoContaSrv.getProdutoSelecionadosFechamento(movimentoSolicitacaoAutorizacao);
            progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onProgressUpdate(Void... values) {

        }
        @Override
        protected void onPostExecute(String result) {
            if(lstMovimentoProdutos == null){
                if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_DESCONHECIDO")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(FechamentoActivity.this);
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro de Conexão");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(FechamentoActivity.this);
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro de Comunicação com servidor!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("TIME_OUT")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(FechamentoActivity.this);
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro Time out!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }
            }else{
               /* for(int a=0;a < lstMovimentoProdutos.size();a++){
                    for(int b = a + 1;b < lstMovimentoProdutos.size();b++){
                        if((lstMovimentoProdutos.get(a).getIdProduto()==lstMovimentoProdutos.get(b).getIdProduto())){
                            somaQuantidade = lstMovimentoProdutos.get(a).getQuantidade();
                            somaQuantidade += lstMovimentoProdutos.get(b).getQuantidade();
                            lstMovimentoProdutos.get(a).setQuantidade(somaQuantidade);
                            lstMovimentoProdutos.remove(b);
                            b--;
                        }
                    }
                }*/
                new LinearLayoutAdapter(FechamentoActivity.this, lstMovimentoProdutos, lProdutos);
                textSubTotalValue.setText(String.format("R$ %.2f", calculaSubTotal()).replace(".", ","));
                textTxServicoValue.setText(String.format("R$ %.2f", calculaTxServico(subTotal)).replace(".", ","));
                textValorPagarValue.setText(String.format("R$ %.2f", calculaValor()).replace(".",","));
                textValorPessoaValue.setText(String.format(textQuantidadePessoa.getText() + "%.2f", calculaValorPessoa()).replace(".", ","));
            }
        }
    };
 /*   public void listaProdutos(){
        try{
            lstMovimentoProdutos = movimentoProdutoSrv.getProdutoSelecionados(this.movimentoSolicitacaoAutorizacao);
            for(int a=0;a < lstMovimentoProdutos.size();a++){
                for(int b = a + 1;b < lstMovimentoProdutos.size();b++){
                    if((lstMovimentoProdutos.get(a).getIdProduto()==lstMovimentoProdutos.get(b).getIdProduto())){
                        this.somaQuantidade = lstMovimentoProdutos.get(a).getQuantidade();
                        this.somaQuantidade += lstMovimentoProdutos.get(b).getQuantidade();
                        lstMovimentoProdutos.get(a).setQuantidade(this.somaQuantidade);
                        lstMovimentoProdutos.remove(b);
                        b--;
                    }
                }
            }
        }catch(Exception e){
            Log.e(TAG,"AQUIIIIIIIIIIIIIIIIIIIIIII: "+e.getMessage());
        }
    }*/
    /*@Override
    public void atualizarView() {
        MENSAGEM_ERRO="";
        new LinearLayoutAdapter(FechamentoActivity.this, lstMovimentoProdutos, lProdutos);
        textSubTotalValue.setText(String.format("%.2f",calculaSubTotal()).replace(".", ","));
        textTxServicoValue.setText(String.format("%.2f",calculaTxServico(subTotal)).replace(".", ","));
        textValorPagarValue.setText(String.format("%.2f", calculaValor()).replace(".",","));
        textValorPessoaValue.setText(String.format(textQuantidadePessoa.getText() + "%.2f", calculaValorPessoa()).replace(".",","));
    }*/

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }

    public void criaDrawerLayout(){
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        mListaMenu = (ListView)findViewById(R.id.lista);
        // mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Sair da Conta","Codigo Autorização"};
        getSupportActionBar().setTitle("Cardápio");//por na string.xml
        mItensListaMenu = new String[]{"Conta ","Cardápio","Fechar Conta", "Histórico","Perfil","Sobre","Codigo Autorização: " + movimentoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Sair da Conta"};

        mListaMenu.setAdapter(new ArrayAdapter<String>(this, R.layout.menu_item_layout, mItensListaMenu));
        navDrawerItems = new ArrayList<NavDrawerItem>();
        // adding nav drawer items to array

        //Conta
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[0], navMenuIcons.getResourceId(1, -1)));
        //Cardapio
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[1], navMenuIcons.getResourceId(0, -1)));
        //Fechar Conta
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[2], navMenuIcons.getResourceId(2, -1)));
        //Historico
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[3], navMenuIcons.getResourceId(3, -1)));
        //Perfil
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[4], navMenuIcons.getResourceId(4, -1)));
        //Sobre
        navDrawerItems.add(new NavDrawerItem(mItensListaMenu[5], navMenuIcons.getResourceId(5, -1)));

        if(movimentoSolicitacaoAutorizacao == null ){

        }else if(movimentoSolicitacaoAutorizacao.isUsuarioAutorizado()!=false){
            //Codigo autorização
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[6], navMenuIcons.getResourceId(6, -1)));
            //Sair da conta
            navDrawerItems.add(new NavDrawerItem(mItensListaMenu[7], navMenuIcons.getResourceId(7, -1)));
        }

        // Recycle the typed array
        navMenuIcons.recycle();

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems);
        mListaMenu.setAdapter(adapter);

        /*Configuração do DrawerLayout*/
        mNavigationDrawer = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mNavigationDrawer.setDrawerShadow(R.color.transparente, GravityCompat.START);

        /*Configuração do seletor no action bar*/
        mSelectorActionBar = new ActionBarDrawerToggle(FechamentoActivity.this, mNavigationDrawer, R.drawable.ic_drawer, 0, 0 ){


            @Override
            public void onDrawerClosed(View drawerView){
                super.onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView){
                super.onDrawerOpened(drawerView);
                //Evento que informará que drawer foi fechado
            }
        };
        mNavigationDrawer.setDrawerListener(mSelectorActionBar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mListaMenu.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> container, View view, int indice, long id) {
                if(indice==0){
                    conta();
                }else if(indice==1){
                    cardapio();
                }else if(indice==2){
                    fecharConta();
                }else if(indice==3){
                    historico();
                }else if(indice==4){
                    perfil();
                }else if(indice==5){
                    sobre();
                }else if(indice==6){
                    showAlertDialogCodigoAutorizacao(movimentoSolicitacaoAutorizacao.getCodigoAutorizacaoMoviSolicitacaoAutorizacao(),"Use este código para autorizar mais dispositivos e acompanhar sua conta ");
                }else if(indice==7){
                    showAlertDialogSairConta(R.drawable.ic_action_about, "Mobilepub", "Você tem certeza que deseja sair da conta?", "Sim", "Não");
                }
            }
        });
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mSelectorActionBar.syncState();
    }
    /* onConfigurationChanged
     * É chamado quando activity ouve uma chamada para alterar suas configurações de estilo/funcionamento
     * Aqui, avisamos o selector do action bar que houve uma mudança de configuração
    */

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mSelectorActionBar.onConfigurationChanged(newConfig);
    }
    public void conta() {
        Intent intent= new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }
    public void cardapio(){
        Intent intent= new Intent(this,CardapioActivity.class);
        startActivity(intent);
        finish();
    }
    public void fecharConta(){

    }
    public void historico(){
        Intent intent = new Intent(this, HistoricoActivity.class);
        startActivity(intent);
        finish();
    }
    public void perfil(){
        if(UsuarioTXT.readUsuario(this) == null){
            Intent intent = new Intent(this,CadastroActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("br.com.cerradoinformatica.ePub.fragments.DetalhesFragments",null);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }else{
            Intent intent = new Intent(this,PerfilActivity.class);
            startActivity(intent);
            finish();
        }
    }
    public void sobre(){
        Intent intent = new Intent(this,SobreActivity.class);
        startActivity(intent);
        finish();
    }
    public void  limpar(){
        if(UsuarioHerbalifeTXT.readUsuarioHerbalife(this) != null){
            if(UsuarioHerbalifeTXT.readUsuarioHerbalife(this).isLogado()){
                UsuarioHerbalife usuarioHerbalife = UsuarioHerbalifeTXT.readUsuarioHerbalife(this);
                usuarioHerbalife.setLogado(false);
                UsuarioHerbalifeTXT.writeUsuarioHerbalife(this,usuarioHerbalife);
            }
        }
        UtilTXT.limparTodosArquivos(this);
        Intent intent= new Intent(this,AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }
    public void showAlertDialogSairConta(int name_incone, String titulo, String mensagem, String nomeButtonPositive,String nomeButtonNegative){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titulo);
        builder.setMessage(mensagem);
        builder.setIcon(name_incone);
        builder.setPositiveButton(nomeButtonPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                limpar();
            }
        });
        builder.setNegativeButton(nomeButtonNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }
    public void showAlertDialogCodigoAutorizacao(int codigoAutorizacao,String mensagem){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_codigo_autorizacao);
        dialog.setTitle("Código de autorização");
        textViewCodigo = (TextView) dialog.findViewById(R.id.textViewCodigo);
        textViewTexto = (TextView) dialog.findViewById(R.id.textViewTexto);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(FechamentoActivity.this);

        textViewCodigo.setText(""+codigoAutorizacao);
        textViewTexto.setText(mensagem);
        dialog.show();
        dialog.setCancelable(false);
    }
    public void direciona(){
        Intent intent = new Intent(FechamentoActivity.this,AcompanhamentoDeContaTelaPrincipalActivity.class);
        startActivity(intent);
        finish();
    }
}
