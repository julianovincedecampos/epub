package br.com.cerradoinformatica.ePub.activity;

import java.util.ArrayList;
import java.util.List;

import br.com.cerradoinformatica.ePub.adapter.MesaAdapter;
import br.com.cerradoinformatica.ePub.model.ListMesas;
import br.com.cerradoinformatica.ePub.model.Mesa;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.Usuario;
import br.com.cerradoinformatica.ePub.model.UsuarioHerbalife;
import br.com.cerradoinformatica.ePub.service.MesaSrv;
import br.com.cerradoinformatica.ePub.service.MovimentoMesaSrv;
import br.com.cerradoinformatica.ePub.funcionalidades.Transacao;
import br.com.cerradoinformatica.ePub.funcionalidades.TransacaoTask;
import br.com.cerradoinformatica.ePub.txts.PubMovimentosolicitacaoAutorizacaoUtil;
import br.com.cerradoinformatica.ePub.txts.UsuarioHerbalifeTXT;
import br.com.cerradoinformatica.ePub.txts.UsuarioTXT;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


public class SelecionarMesa extends ActionBarActivity implements Transacao, OnItemClickListener{

	private List<Mesa>lstMesas;
    private ListMesas listMesas;
	private MesaSrv mesaSrv;
	private int idEstabelecimento;
	private ListView listViewMesas;
	private int acao=1;
	private Mesa mesa;
	private MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao;
	private MovimentoMesaSrv movSrv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mesas);
		
		listViewMesas= (ListView) findViewById(R.id.listMesas);
		listViewMesas.setOnItemClickListener(this);
		mesaSrv =new MesaSrv(this);
		mesa=new Mesa();
        movimetoSolicitacaoAutorizacao = new MovimetoSolicitacaoAutorizacao();
		movSrv=new MovimentoMesaSrv(this);
        lstMesas = new ArrayList<Mesa>();

		getIdEstabelecimento();
        listViewMesas.setAdapter(new MesaAdapter(this, lstMesas	));

		TransacaoTask task = new TransacaoTask(this,this, R.string.aguarde);
		task.execute();
	}
	
	public void getIdEstabelecimento(){
		Intent it =getIntent();
		idEstabelecimento = it.getIntExtra("IdEstabelecimento", 0);
	}
	

	@Override
	public void executar() throws Exception {
		// TODO Auto-generated method stub

        listMesas=new ListMesas();
        lstMesas=mesaSrv.getMesas(idEstabelecimento);
        listMesas.setMesaList(lstMesas);
		
		switch (acao) {
		case 1:
             listMesas=new ListMesas();
             lstMesas=mesaSrv.getMesas(idEstabelecimento);
             listMesas.setMesaList(lstMesas);
        break;
		case 2:
            movimetoSolicitacaoAutorizacao = movSrv.postMovimentoAutorizar(movimetoSolicitacaoAutorizacao);
            movimetoSolicitacaoAutorizacao.setUsuarioAutorizado(false);
            PubMovimentosolicitacaoAutorizacaoUtil.clearFile(this);
            PubMovimentosolicitacaoAutorizacaoUtil.writeMovimetoSolicitacaoAutorizacao(this, movimetoSolicitacaoAutorizacao);
            Intent intent = new Intent(this, AcompanhamentoDeContaTelaPrincipalActivity.class);
            startActivity(intent);
            finish();
            break;
		}
	}

	@Override
	public void atualizarView() {
		// TODO Auto-generated method stub
        listViewMesas.setAdapter(new MesaAdapter(this, lstMesas	));
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int posicao, long id) {
		// TODO Auto-generated method stub
        mesa = (Mesa) parent.getAdapter().getItem(posicao);
        movimetoSolicitacaoAutorizacao.setIdEstabelecimento(idEstabelecimento);
        movimetoSolicitacaoAutorizacao.setIdMesa(mesa.getId());
        movimetoSolicitacaoAutorizacao.setAutorizadoPor(0);
        movimetoSolicitacaoAutorizacao.setDataAutorizacao(null);
        movimetoSolicitacaoAutorizacao.setNumero(0);

        if(UsuarioHerbalifeTXT.readUsuarioHerbalife(this) != null && idEstabelecimento == 4){
            UsuarioHerbalife usuarioHerbalife = UsuarioHerbalifeTXT.readUsuarioHerbalife(this);
            usuarioHerbalife.setNome(mesa.getNome());
            UsuarioHerbalifeTXT.writeUsuarioHerbalife(this,usuarioHerbalife);
        }

		acao=2;

        Bundle bundle = new Bundle();
        bundle.putSerializable("movimetoSolicitacaoAutorizacao", movimetoSolicitacaoAutorizacao);

        TransacaoTask task = new TransacaoTask(this,this, R.string.aguarde);
        task.execute();
		
	}
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, EstablecimentosActivity.class);
        startActivity(intent);
        finish();
    }
}
