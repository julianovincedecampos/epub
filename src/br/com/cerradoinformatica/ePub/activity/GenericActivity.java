package br.com.cerradoinformatica.ePub.activity;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

public class GenericActivity extends ActionBarActivity{
//	TextView text;
//
//	@Override
//	protected void onResume() {
//		// TODO Auto-generated method stub
//		super.onResume();
//		text = (TextView) findViewById(R.id.textHeader);
//		Typeface typeFace =Typeface.createFromAsset(getAssets(),"bobcayge.ttf");
//		text.setTypeface(typeFace);
//
//	}


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }
}
