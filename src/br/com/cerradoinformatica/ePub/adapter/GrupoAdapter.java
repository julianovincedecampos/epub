package br.com.cerradoinformatica.ePub.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.Estabelecimento;
import br.com.cerradoinformatica.ePub.model.Grupo;
import util.DownloadsImagens;
import util.URLUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GrupoAdapter extends BaseAdapter{

    private LayoutInflater inflater;
    private final List<Grupo> lstGrupo;
    private final Activity context;

    public GrupoAdapter(Activity context, List<Grupo>lstGrupo) {
        // TODO Auto-generated constructor stub
        this.context=context;
        this.lstGrupo=lstGrupo;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return lstGrupo!=null?lstGrupo.size():0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return lstGrupo!=null?lstGrupo.get(position):null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolderGrupo holder = null;

        if(view==null){
            holder=new ViewHolderGrupo();
            int layout = R.layout.grupo_item;
            view = inflater.inflate(layout, null);
            view.setTag(holder);
            holder.setTextNome((TextView) view.findViewById(R.id.textNomeGrupo));
            holder.setImagem((ImageView) view.findViewById(R.id.imageGrupo));
        }
        else{
            holder = (ViewHolderGrupo) view.getTag();
        }

        Grupo grupo = lstGrupo.get(position);


       holder.getTextNome().setText(grupo.getNomeGrupoProduto());
/*
        InputStream stream;

        try {
            stream = context.getResources().getAssets().open(grupo.getNomeImagemGrupoProduto());
        } catch (IOException e) {
            stream = null;
        }

        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        holder.getImagem().setImageBitmap(bitmap);

        */

        BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), context.getFileStreamPath(grupo.getNomeImagemGrupoProduto()).getAbsolutePath());
        InputStream stream;

        try{
            stream = context.getResources().getAssets().open(grupo.getNomeImagemGrupoProduto());
        }catch(IOException e){
            stream = null;
        }

        if(bitmapDrawable.getBitmap() == null && stream == null){
            TaskGETImagensGrupos taskGETImagensEstabelecimento = new TaskGETImagensGrupos(context,grupo,holder);
            taskGETImagensEstabelecimento.execute();
        }else if(bitmapDrawable.getBitmap() != null){
            holder.getImagem().setImageDrawable(bitmapDrawable);
        }else{
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            holder.getImagem().setImageBitmap(bitmap);
        }

        return view;
    }
    private class TaskGETImagensGrupos extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;
        Grupo grupo;
        ViewHolderGrupo holder;

        public TaskGETImagensGrupos(Context context, Grupo grupo, ViewHolderGrupo holder){
            this.context = context;
            this.grupo = grupo;
            this.holder = holder;
        }
        @Override
        protected void onPreExecute() {
           // progressDialog = new ProgressDialog(context);
           // progressDialog = ProgressDialog.show(context, "", "Download de produtos");
           // progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            DownloadsImagens.Download(context, URLUtil.getUrlImagens(context) + this.grupo.getNomeImagemGrupoProduto(), this.grupo.getNomeImagemGrupoProduto());
        //    progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), context.getFileStreamPath(this.grupo.getNomeImagemGrupoProduto()).getAbsolutePath());
            holder.getImagem().setImageDrawable(bitmapDrawable);
        }

    };
    /*
         switch (grupo.getIdGrupoProduto()){
            case 1:
                holder.getImagem().setBackgroundResource(R.drawable.cervejas);
                break;
            case 2:
                holder.getImagem().setBackgroundResource(R.drawable.aguas_refrigerantes);
                break;
            case 3:
                holder.getImagem().setBackgroundResource(R.drawable.sucos);
                break;
            case 4:
                holder.getImagem().setBackgroundResource(R.drawable.a_la_carte);
                break;
            case 5:
                holder.getImagem().setBackgroundResource(R.drawable.coqueteis);
                break;
            case 6:
                holder.getImagem().setBackgroundResource(R.drawable.petiscos_porcoes);
                break;
            case 7:
                holder.getImagem().setBackgroundResource(R.drawable.frios);
                break;
           case 8:
               holder.getImagem().setBackgroundResource(R.drawable.guarnicoes);
               break;
            case 9:
                holder.getImagem().setBackgroundResource(R.drawable.guarnicoes);
                break;
            case 10:
                holder.getImagem().setBackgroundResource(R.drawable.cachacas);
                break;
            case 11:
                holder.getImagem().setBackgroundResource(R.drawable.panelinhas);
                break;
            case 12:
                holder.getImagem().setBackgroundResource(R.drawable.saladas);
                break;
            case 13:
                holder.getImagem().setBackgroundResource(R.drawable.licores);
                break;
            case 14:
                holder.getImagem().setBackgroundResource(R.drawable.whisky_e_vodkas);
                break;
            case 15:
                holder.getImagem().setBackgroundResource(R.drawable.combos);
                break;
            case 16:
                holder.getImagem().setBackgroundResource(R.drawable.bebidas_diversas);
                break;
            case 17:
                holder.getImagem().setBackgroundResource(R.drawable.doses);
                break;
            case 18:
                holder.getImagem().setBackgroundResource(R.drawable.vinhos);
                break;
            case 19:
                holder.getImagem().setBackgroundResource(R.drawable.espumantes);
                break;
            case 20:
                holder.getImagem().setBackgroundResource(R.drawable.cremes);
                break;
            case 21:
                holder.getImagem().setBackgroundResource(R.drawable.outros);
                break;
            case 23:
                holder.getImagem().setBackgroundResource(R.drawable.chicletes_balas);
                break;
            default:
                holder.getImagem().setBackgroundResource(R.drawable.grupo_sem_imagem);
                break;
                */
    //}


        /*
		if(grupo.getNome().equals("Cerveja")){
			holder.getImagem().setBackgroundResource(R.drawable.cervejas);
			}
		else if(grupo.getNome().equalsIgnoreCase("Refrigerante")){
			holder.getImagem().setBackgroundResource(R.drawable.aguas_refrigerantes);
		}
		else if(grupo.getNome().equalsIgnoreCase("Sucos")){
			holder.getImagem().setBackgroundResource(R.drawable.sucos);
		}
        else if(grupo.getNome().equalsIgnoreCase("a la carte")){
            holder.getImagem().setBackgroundResource(R.drawable.a_la_carte);
        }
        else if(grupo.getNome().equalsIgnoreCase("coqueteis")){
            holder.getImagem().setBackgroundResource(R.drawable.coqueteis);
        }
        else if(grupo.getNome().equalsIgnoreCase("porcoes")){
            holder.getImagem().setBackgroundResource(R.drawable.petiscos_porcoes);
        }

		/*else if(grupo.getNome().equalsIgnoreCase("Drinks")){
			holder.getImagem().setBackgroundResource(R.drawable.doses);
		}
		else if(grupo.getNome().equalsIgnoreCase("Shake")){
			holder.getImagem().setBackgroundResource(R.drawable.doses);
		}
		else if(grupo.getNome().equalsIgnoreCase("Petiscos")){
			holder.getImagem().setBackgroundResource(R.drawable.a_la_carte);
		}
		else if(grupo.getNome().equalsIgnoreCase("Espetinhos")){
			holder.getImagem().setBackgroundResource(R.drawable.a_la_carte);
		}*/
}
