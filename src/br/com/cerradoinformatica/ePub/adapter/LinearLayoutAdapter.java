package br.com.cerradoinformatica.ePub.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;


public class LinearLayoutAdapter {

    private Context context;
    private LinearLayout layout;
    private List<MovimentoProduto> lstMov;
    private View view;
    private LayoutInflater inflater;
    private static final String TAG = "<< LinearLayoutAdapter >>";

    public LinearLayoutAdapter(Context context, List<MovimentoProduto> lstMov,LinearLayout layout) {
        this.context=context;
        this.lstMov=lstMov;
        this.layout=layout;
        inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        add();
    }

    public void add(){
        for(MovimentoProduto mov :lstMov){
            view = inflater.inflate(R.layout.item_fechamento, null);
            TextView textProduto = (TextView) view.findViewById(R.id.textProduto);
            TextView textProdutoValue =(TextView)view.findViewById(R.id.textProdutoValue);
            String str = String.format("%d X %.2f = %.2f", mov.getQuantidade(),mov.getValorUnitario(),mov.getValorUnitario()*mov.getQuantidade()).replace(".",",");
            textProduto.setText(mov.getNomeProduto());
            textProdutoValue.setText(str);
            layout.addView(view);
        }
    }
}
