package br.com.cerradoinformatica.ePub.adapter;

import android.widget.TextView;

/**
 * Created by juliano on 04/02/14.
 */
public class ViewHolderMovimentoProduto {

    private TextView textNomeProduto;
    private TextView textTempo;
    private TextView textPreco;
    private TextView textPrecoTotal;

    public TextView getTextNomeProduto() {
        return textNomeProduto;
    }

    public void setTextNomeProduto(TextView textNomeProduto) {
        this.textNomeProduto = textNomeProduto;
    }

    public TextView getTextTempo() {
        return textTempo;
    }

    public void setTextTempo(TextView textTempo) {
        this.textTempo = textTempo;
    }

    public TextView getTextPreco() {
        return textPreco;
    }

    public void setTextPreco(TextView textPreco) {
        this.textPreco = textPreco;
    }

    public TextView getTextPrecoTotal() {
        return textPrecoTotal;
    }

    public void setTextPrecoTotal(TextView textPrecoTotal) {
        this.textPrecoTotal = textPrecoTotal;
    }
}
