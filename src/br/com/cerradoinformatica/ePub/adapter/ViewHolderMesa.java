package br.com.cerradoinformatica.ePub.adapter;

import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolderMesa {
	
	private TextView textNomeMesa;
	private TextView textDisponibilidade;
	private ImageView imagem;
	public TextView getTextNomeMesa() {
		return textNomeMesa;
	}
	public void setTextNomeMesa(TextView textNomeMesa) {
		this.textNomeMesa = textNomeMesa;
	}
	public TextView getTextDisponibilidade() {
		return textDisponibilidade;
	}
	public void setTextDisponibilidade(TextView textDisponibilidade) {
		this.textDisponibilidade = textDisponibilidade;
	}
	public ImageView getImagem() {
		return imagem;
	}
	public void setImagem(ImageView imagem) {
		this.imagem = imagem;
	}
	
	

}
