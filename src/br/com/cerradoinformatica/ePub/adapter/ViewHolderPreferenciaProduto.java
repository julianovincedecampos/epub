package br.com.cerradoinformatica.ePub.adapter;

import android.widget.CheckBox;
import android.widget.TextView;

/**
 * Created by juliano on 05/02/14.
 */
public class ViewHolderPreferenciaProduto {

    private TextView textViewNomePreferencias;
    private CheckBox checkBoxPreferencias;

    public TextView getTextViewNomePreferencias() {
        return textViewNomePreferencias;
    }

    public void setTextViewNomePreferencias(TextView textViewNomePreferencias) {
        this.textViewNomePreferencias = textViewNomePreferencias;
    }

    public CheckBox getCheckBoxPreferencias() {
        return checkBoxPreferencias;
    }

    public void setCheckBoxPreferencias(CheckBox checkBoxPreferencias) {
        this.checkBoxPreferencias = checkBoxPreferencias;
    }
}
