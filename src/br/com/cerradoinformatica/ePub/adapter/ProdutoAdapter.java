package br.com.cerradoinformatica.ePub.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.Grupo;
import br.com.cerradoinformatica.ePub.model.Produto;
import util.DownloadsImagens;
import util.URLUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProdutoAdapter extends BaseAdapter{

    private final Activity context;
    private final List<Produto> lstProduto;
    private LayoutInflater inflater;

    public ProdutoAdapter(Activity context, List<Produto> listProduto) {
        // TODO Auto-generated constructor stub
        this.context=context;
        this.lstProduto=listProduto;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return lstProduto!=null?lstProduto.size():0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return lstProduto!=null?lstProduto.get(position):null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder holder = null;
        if(view==null){
            holder=new ViewHolder();
            int layout = R.layout.produto_item;
            view = inflater.inflate(layout, null);
            view.setTag(holder);
            holder.setTextView1((TextView) view.findViewById(R.id.textNomeProduto));
            holder.setTextView2((TextView) view.findViewById(R.id.textPreco));
            holder.setImageView((ImageView) view.findViewById(R.id.imageViewProduto));
        }
        else{
            holder=(ViewHolder) view.getTag();
        }

        Produto produto = lstProduto.get(position);

        holder.getTextView1().setText(produto.getNome());
        holder.getTextView2().setText(String.format("R$ %.2f", produto.getPreco()));

        /*InputStream stream;

        try {
            stream = context.getResources().getAssets().open(produto.getNomeImagem());
        } catch (IOException e) {
            stream = null;
        }

        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        holder.getImageView().setImageBitmap(bitmap);
        */



        return view;
    }


 /*
		switch (produto.getId_Grupo()) {
		case 1:
			holder.getImageView().setBackgroundResource(R.drawable.cervejas);
			break;
		case 2:
			holder.getImageView().setBackgroundResource(R.drawable.aguas_refrigerantes);
			break;
		case 3:
			holder.getImageView().setBackgroundResource(R.drawable.sucos);
			break;

		case 4:
			holder.getImageView().setBackgroundResource(R.drawable.a_la_carte);
			break;
		case 5:
			holder.getImageView().setBackgroundResource(R.drawable.coqueteis);
			break;
		case 6:
			holder.getImageView().setBackgroundResource(R.drawable.petiscos_porcoes);
			break;
		case 7:
			holder.getImageView().setBackgroundResource(R.drawable.frios);
			break;
        case 8:
                holder.getImageView().setBackgroundResource(R.drawable.guarnicoes);
        break;
        case 9:
            holder.getImageView().setBackgroundResource(R.drawable.guarnicoes);
            break;
        case 10:
            holder.getImageView().setBackgroundResource(R.drawable.cachacas);
            break;
        case 11:
            holder.getImageView().setBackgroundResource(R.drawable.panelinhas);
            break;
        case 12:
            holder.getImageView().setBackgroundResource(R.drawable.saladas);
            break;
        case 13:
            holder.getImageView().setBackgroundResource(R.drawable.licores);
            break;
        case 14:
            holder.getImageView().setBackgroundResource(R.drawable.whisky_e_vodkas);
            break;
        case 15:
            holder.getImageView().setBackgroundResource(R.drawable.combos);
            break;
        case 16:
            holder.getImageView().setBackgroundResource(R.drawable.bebidas_diversas);
            break;
        case 17:
            holder.getImageView().setBackgroundResource(R.drawable.doses);
            break;
        case 18:
            holder.getImageView().setBackgroundResource(R.drawable.vinhos);
            break;
        case 19:
            holder.getImageView().setBackgroundResource(R.drawable.espumantes);
            break;
        case 20:
            holder.getImageView().setBackgroundResource(R.drawable.cremes);
            break;
        case 21:
            holder.getImageView().setBackgroundResource(R.drawable.outros);
            break;
        case 23:
          //  holder.getImageView().setBackgroundResource(R.drawable.chicletes_balas);
            break;
		default:
			break;
		}*/
}
