package br.com.cerradoinformatica.ePub.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.PreferenciaProduto;
import br.com.cerradoinformatica.ePub.txts.PreferenciasSelecionadasTXT;

public class PreferenciaProdutoAdapter extends BaseAdapter {

	private final Activity context;
	private final List<PreferenciaProduto> listaPreferenciaProduto;
    private List<String> listaPreferenciaProdutoSelecionado;
	private LayoutInflater inflater;
    private String idsUtilizados = "";
    private static final String TAG = "<<PreferenciaProdutoAdapter>>";

	public PreferenciaProdutoAdapter(Activity context, List<PreferenciaProduto> listaPreferenciaProduto) {
		this.context=context;
		this.listaPreferenciaProduto = listaPreferenciaProduto;
        this.listaPreferenciaProdutoSelecionado = new ArrayList<String>();
		inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listaPreferenciaProduto!=null?listaPreferenciaProduto.size():0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listaPreferenciaProduto!=null?listaPreferenciaProduto.get(position):null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolderPreferenciaProduto viewHolderPreferenciaProduto = null;
        final int  posicao = position;
        final  PreferenciaProduto preferenciaProduto = listaPreferenciaProduto.get(position);


        if(view==null){
            viewHolderPreferenciaProduto = new ViewHolderPreferenciaProduto();
            int layout = R.layout.preferencias_item;
            view = inflater.inflate(layout, null);

            viewHolderPreferenciaProduto.setTextViewNomePreferencias((TextView) view.findViewById(R.id.textViewNomePreferencias));
            viewHolderPreferenciaProduto.setCheckBoxPreferencias((CheckBox) view.findViewById(R.id.checkBoxPreferencias));
            view.setTag(viewHolderPreferenciaProduto);
        }else{
            viewHolderPreferenciaProduto = (ViewHolderPreferenciaProduto) view.getTag();
        }

        viewHolderPreferenciaProduto.getCheckBoxPreferencias().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox cb = (CheckBox) view;
                if (cb.isChecked() == true) {
                    preferenciaProduto.setSelecionado(true);
                    idsUtilizados += preferenciaProduto.getIdPreferenciaProduto() + "#";
                    preferenciaProduto.setIdsUtilizados(idsUtilizados);
                    PreferenciasSelecionadasTXT.writePreferencia(context, preferenciaProduto);
                } else {
                    preferenciaProduto.setIdsUtilizados(idsUtilizados);
                    listaPreferenciaProdutoSelecionado = new ArrayList<String>();
                    idsUtilizados = "";
                    preferenciaProduto.setSelecionado(false);
                    Log.i(TAG,"id = " + preferenciaProduto.getIdPreferenciaProduto());
                    String[] vetorPreferenciaProduto = preferenciaProduto.getIdsUtilizados().split("#");

                    for(int i = 0; i < vetorPreferenciaProduto.length; i++ ){
                        listaPreferenciaProdutoSelecionado.add(vetorPreferenciaProduto[i]);
                    }
                    if(listaPreferenciaProdutoSelecionado.contains(preferenciaProduto.getIdPreferenciaProduto())){
                        listaPreferenciaProdutoSelecionado.remove(preferenciaProduto.getIdPreferenciaProduto());
                        idsUtilizados = "";
                        preferenciaProduto.setIdsUtilizados(idsUtilizados);
                        for(int i =0; i < listaPreferenciaProdutoSelecionado.size(); i ++){
                            idsUtilizados += listaPreferenciaProdutoSelecionado.get(i) +"#";
                            preferenciaProduto.setIdsUtilizados(idsUtilizados);
                        }
                        PreferenciasSelecionadasTXT.writePreferencia(context, preferenciaProduto);
                    }
                }
            }
        });
        viewHolderPreferenciaProduto.getTextViewNomePreferencias().setText(preferenciaProduto.getNome());
        viewHolderPreferenciaProduto.getCheckBoxPreferencias().setChecked(preferenciaProduto.isSelecionado());
        return view;
	}
}
