package br.com.cerradoinformatica.ePub.adapter;

import android.widget.ImageView;
import android.widget.TextView;

public class ViewHolder {
	
	private TextView textView1;
	private TextView textView2;
	private ImageView imageView;
	
	public TextView getTextView1() {
		return textView1;
	}
	public void setTextView1(TextView textView1) {
		this.textView1 = textView1;
	}
	public TextView getTextView2() {
		return textView2;
	}
	public void setTextView2(TextView textView2) {
		this.textView2 = textView2;
	}
	public ImageView getImageView() {
		return imageView;
	}
	public void setImageView(ImageView imageView) {
		this.imageView = imageView;
	}
}
