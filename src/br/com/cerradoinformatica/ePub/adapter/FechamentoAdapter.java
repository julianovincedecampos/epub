package br.com.cerradoinformatica.ePub.adapter;

import java.util.List;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FechamentoAdapter  extends BaseAdapter{
	
	private List<MovimentoProduto> lstMovProduto;
	private Context context;
	private LayoutInflater inflater;
	
	public FechamentoAdapter(List<MovimentoProduto> lstMovProduto,Context context) {
		this.lstMovProduto=lstMovProduto;
		this.context=context;
		inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return (lstMovProduto.size()!=0)?lstMovProduto.size():0;
	}

	@Override
	public Object getItem(int position) {
	
		return (lstMovProduto.size()!=0)?lstMovProduto.get(position):null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		ViewHolder holder=null;
		if(view==null){
			holder=new ViewHolder();
			view = inflater.inflate(R.layout.item_fechamento,null);
			holder.setTextView1((TextView) view.findViewById(R.id.textProduto));
			holder.setTextView2((TextView) view.findViewById(R.id.textProdutoValue));
			view.setTag(holder);
		}else{
			holder=(ViewHolder) view.getTag();
		}
		MovimentoProduto movProduto = lstMovProduto.get(position);
		holder.getTextView1().setText(movProduto.getNomeProduto());
		holder.getTextView2().setText(String.format("%d X %.2f = %.2f", movProduto.getQuantidade(),
				movProduto.getValorUnitario(),movProduto.getValorUnitario()*movProduto.getQuantidade()).replace(".",","));
		return view;
	}

}
