package br.com.cerradoinformatica.ePub.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.HistoricoConta;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import util.DateUtil;

/**
 * Created by juliano on 14/01/14.
 */
public class HistoricosAdapter extends BaseAdapter{

    private LayoutInflater inflater;
    private final Activity context;
    private final List<HistoricoConta> listaDeHistoricos;


    public HistoricosAdapter(Activity context,List<HistoricoConta>listaDeHistoricos){
        this.context = context;
        this.listaDeHistoricos=listaDeHistoricos;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listaDeHistoricos!=null?listaDeHistoricos.size():0;
    }

    @Override
    public Object getItem(int position) {
        return listaDeHistoricos!=null?listaDeHistoricos.get(position):null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolderHistoricos holder = null;

        if(view==null){
            holder = new ViewHolderHistoricos();
            int layout = R.layout.historico_item;
            view = inflater.inflate(layout, null);
            holder.setTextNomeEstabelecimento((TextView) view.findViewById(R.id.textNomeEstabelecimento));
            holder.setTextData((TextView) view.findViewById(R.id.textData));
            view.setTag(holder);
        }
        else{
            holder=(ViewHolderHistoricos) view.getTag();
        }

        HistoricoConta historicoConta = listaDeHistoricos.get(position);
        if(historicoConta.getIdEstabelecimento()==2){
            holder.getTextNomeEstabelecimento().setText("MUMBUCA BEER");
        }else{
            holder.getTextNomeEstabelecimento().setText("TESTE");
        }

        holder.getTextData().setText(DateUtil.getDataString(historicoConta.getDate()));

        return view;
    }
}
