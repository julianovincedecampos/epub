package br.com.cerradoinformatica.ePub.adapter;

import java.util.List;

import br.com.cerradoinformatica.ePub.funcionalidades.DatasUtils;
import util.DateUtil;
import util.EpubUtils;
import util.IOUtils;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.MovimentoMesa;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MovimentoProdutoAdapter extends BaseAdapter{
	
	private final Activity context;
	private final List<MovimentoProduto> listMovimentoProduto;
	private LayoutInflater inflater;
	private final MovimentoMesa movimentoMesa;
	
	public MovimentoProdutoAdapter(Activity context,List<MovimentoProduto>listMovimentoProduto) {
		this.context=context;
		this.listMovimentoProduto=listMovimentoProduto;
		
		inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		movimentoMesa = IOUtils.readObjectInFile(context, "movimento.txt");
		
	}
	

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listMovimentoProduto!=null?listMovimentoProduto.size():0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listMovimentoProduto!=null?listMovimentoProduto.get(position):null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolderMovimentoProduto viewHolderMovimentoProduto = null;

        if(view==null){
            viewHolderMovimentoProduto = new ViewHolderMovimentoProduto();
            int layout = R.layout.produto_item_movimento_produto;
            view = inflater.inflate(layout, null);

            viewHolderMovimentoProduto.setTextNomeProduto((TextView) view.findViewById(R.id.textNomeProduto));
            viewHolderMovimentoProduto.setTextTempo((TextView) view.findViewById(R.id.textTempo));
            viewHolderMovimentoProduto.setTextPreco((TextView) view.findViewById(R.id.textPreco));
            viewHolderMovimentoProduto.setTextPrecoTotal((TextView) view.findViewById(R.id.textPrecoTotal));

            view.setTag(viewHolderMovimentoProduto);

        }
        else{
            viewHolderMovimentoProduto = (ViewHolderMovimentoProduto) view.getTag();
        }
        MovimentoProduto movProduto = listMovimentoProduto.get(position);

        viewHolderMovimentoProduto.getTextNomeProduto().setText(movProduto.getNomeProduto());
        viewHolderMovimentoProduto.getTextTempo().setText(DatasUtils.getDataDiaMesHoraMinuto(movProduto.getDataMovimento()));
        viewHolderMovimentoProduto.getTextPreco().setText(String.format("%.2f X %s", movProduto.getValorUnitario(), movProduto.getQuantidade()));
        double valor = (movProduto.getValorUnitario() * movProduto.getQuantidade());
        //viewHolderMovimentoProduto.getTextPrecoTotal().setText(""+valor );
        viewHolderMovimentoProduto.getTextPrecoTotal().setText(String.format("R$ %.2f",valor));

        return view;
	}

}
