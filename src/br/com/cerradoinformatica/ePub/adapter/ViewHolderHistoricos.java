package br.com.cerradoinformatica.ePub.adapter;

import android.widget.TextView;

/**
 * Created by juliano on 16/01/14.
 */
public class ViewHolderHistoricos {
    private TextView textNomeEstabelecimento;
    private TextView textData;


    public TextView getTextNomeEstabelecimento() {
        return textNomeEstabelecimento;
    }

    public void setTextNomeEstabelecimento(TextView textNomeEstabelecimento) {
        this.textNomeEstabelecimento = textNomeEstabelecimento;
    }

    public TextView getTextData() {
        return textData;
    }

    public void setTextData(TextView textData) {
        this.textData = textData;
    }
}
