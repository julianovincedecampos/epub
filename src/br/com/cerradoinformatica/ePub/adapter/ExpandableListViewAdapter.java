package br.com.cerradoinformatica.ePub.adapter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.Produto;
import util.DownloadsImagens;
import util.URLUtil;

/**
 * Created by juliano on 17/03/14.
 */
public class ExpandableListViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<Produto> arrayListProdutos;
    private DecimalFormat formato;
    // Definindo o conteúdo de nossa lista e sublista

    public ExpandableListViewAdapter(Context context, List<Produto> arrayListProdutos) {
        this.context = context;
        this.arrayListProdutos = arrayListProdutos;
        formato = (DecimalFormat) DecimalFormat.getInstance(Locale.getDefault());
        formato.applyPattern(".00");
    }
    @Override
    public Object getChild(int groupPosition, int childPosition) {
    // TODO Auto-generated method stub
        return arrayListProdutos.get(groupPosition);
    }
    @Override
    public long getChildId(int groupPosition, int childPosition) {
    // TODO Auto-generated method stub
        return arrayListProdutos.get(groupPosition).getIdGrupo();
    }
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        //Interface do filho
        View view = convertView;

        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.produto_caracteristicas,parent,false);
        }

        TextView textCaracteristicas = (TextView) view.findViewById(R.id.textCaracteristicas);

        Produto produto = arrayListProdutos.get(groupPosition);
        textCaracteristicas.setText(produto.getCaracteristicas());

        return view;
    }
    @Override
    public int getChildrenCount(int groupPosition) {
    // TODO Auto-generated method stub
        return 1;
    }
    @Override
    public Object getGroup(int groupPosition) {
    // TODO Auto-generated method stub
        return arrayListProdutos.get(groupPosition);
    }
    @Override
    public int getGroupCount() {
    // TODO Auto-generated method stub
        return arrayListProdutos.size();
    }
    @Override
    public long getGroupId(int groupPosition) {
    // TODO Auto-generated method stub
        return arrayListProdutos.get(groupPosition).getIdGrupo();
    }
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {
        //Interface do Pai
        View view = convertView;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.produto,parent,false);
        }

        TextView textViewProdutoTitulo = (TextView) view.findViewById(R.id.textViewProdutoTitulo);
        TextView textViewPreco  = (TextView) view.findViewById(R.id.textViewPreco);
        ImageView imageViewProduto = (ImageView) view.findViewById(R.id.imageViewProduto);


        Produto produto = arrayListProdutos.get(groupPosition);
        textViewProdutoTitulo.setText(produto.getNome());
        textViewPreco.setText("R$ "+formato.format(produto.getPreco()));



        BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), context.getFileStreamPath(produto.getNomeImagem()).getAbsolutePath());
        InputStream stream;

        try{
            stream = context.getResources().getAssets().open(produto.getNomeImagem());
        }catch(IOException e){
            stream = null;
        }

        if(bitmapDrawable.getBitmap() == null && stream == null){
            TaskGETImagensProdutos taskGETImagensEstabelecimento = new TaskGETImagensProdutos(context,produto,imageViewProduto);
            taskGETImagensEstabelecimento.execute();
        }else if(bitmapDrawable.getBitmap() != null){
            imageViewProduto.setImageDrawable(bitmapDrawable);
        }else{
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            imageViewProduto.setImageBitmap(bitmap);
        }

        return view;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        // Defina o return como sendo true se vc desejar que sua sublista seja selecionável

        return true;
    }

    private class TaskGETImagensProdutos extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;
        Produto produto;
        ImageView imageViewProduto;
        BitmapDrawable bitmapDrawable;

        public TaskGETImagensProdutos(Context context, Produto produto, ImageView imageViewProduto){
            this.context = context;
            this.produto = produto;
            this.imageViewProduto = imageViewProduto;
        }
        @Override
        protected void onPreExecute() {
            //progressDialog = new ProgressDialog(context);
            //progressDialog = ProgressDialog.show(context, "", "Download de produtos");
            //progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            DownloadsImagens.Download(context, URLUtil.getUrlImagens(context) + this.produto.getNomeImagem(), this.produto.getNomeImagem());
            //progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
             bitmapDrawable = new BitmapDrawable(context.getResources(), context.getFileStreamPath(this.produto.getNomeImagem()).getAbsolutePath());
            if(bitmapDrawable.getBitmap() != null){
                imageViewProduto.setImageDrawable(bitmapDrawable);
            }
        }

    };
}

