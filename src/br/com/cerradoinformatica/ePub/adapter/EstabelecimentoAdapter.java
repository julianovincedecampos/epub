package br.com.cerradoinformatica.ePub.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.Estabelecimento;
import util.DownloadsImagens;
import util.URLUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class EstabelecimentoAdapter extends BaseAdapter {
	
	private LayoutInflater inflater;
	private final List<Estabelecimento> listaEstabelecimento;
	private final Activity context;
	
	public EstabelecimentoAdapter(Activity context,List<Estabelecimento> estabelecimentos) {
		this.context=context;
		this.listaEstabelecimento=estabelecimentos;
		this.inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listaEstabelecimento!=null?listaEstabelecimento.size():0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listaEstabelecimento!=null?listaEstabelecimento.get(position):null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if(view==null){
			holder=new ViewHolder();
			int layout = R.layout.estabelecimento_item;
			view =inflater.inflate(layout, null);
			view.setTag(holder);
			holder.setTextView1((TextView) view.findViewById(R.id.textNomeFantasia));
			holder.setTextView2((TextView)view.findViewById(R.id.textRazaoSocial));
            holder.setImageView((ImageView)view.findViewById(R.id.imageViewEstabelecimento));
		}
		else{
			holder=(ViewHolder)view.getTag();
		}
		Estabelecimento estabelecimento = listaEstabelecimento.get(position);
		holder.getTextView1().setText(estabelecimento.getNomeFantasia());
		holder.getTextView2().setText(estabelecimento.getRazaoSocial());


        BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), context.getFileStreamPath(estabelecimento.getNomeImagemEstabelecimento()).getAbsolutePath());

        InputStream stream;

        try{
            stream = context.getResources().getAssets().open(estabelecimento.getNomeImagemEstabelecimento());
        }catch(IOException e){
            stream = null;
        }

        if(bitmapDrawable.getBitmap() == null && stream == null){
            TaskGETImagensEstabelecimento taskGETImagensEstabelecimento = new TaskGETImagensEstabelecimento(context,estabelecimento,holder);
            taskGETImagensEstabelecimento.execute();
        }else if(bitmapDrawable.getBitmap() != null){
            holder.getImageView().setImageDrawable(bitmapDrawable);
        }else{
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            holder.getImageView().setImageBitmap(bitmap);
        }
		return view;
	}

    private class TaskGETImagensEstabelecimento extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;
        Estabelecimento estabelecimento;
        ViewHolder holder;

        public TaskGETImagensEstabelecimento(Context context, Estabelecimento estabelecimento, ViewHolder holder){
            this.context = context;
            this.estabelecimento = estabelecimento;
            this.holder = holder;
        }
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", "Download Estabelecimento...");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            DownloadsImagens.Download(context, URLUtil.getUrlImagens(context)+this.estabelecimento.getNomeImagemEstabelecimento(),this.estabelecimento.getNomeImagemEstabelecimento());
            progressDialog.dismiss();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), context.getFileStreamPath(estabelecimento.getNomeImagemEstabelecimento()).getAbsolutePath());
            holder.getImageView().setImageDrawable(bitmapDrawable);
        }

    };
}
