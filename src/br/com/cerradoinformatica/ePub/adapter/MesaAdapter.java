package br.com.cerradoinformatica.ePub.adapter;

import java.util.List;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.model.Mesa;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MesaAdapter extends BaseAdapter{
	
	private LayoutInflater inflater;
	private final List<Mesa> lstMesa;
	private final Activity context;
	
	
	public MesaAdapter(Activity context,List<Mesa>lstMesa) {
		// TODO Auto-generated constructor stub
		
		this.lstMesa=lstMesa;
		this.context=context;
		
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstMesa!=null?lstMesa.size():0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return lstMesa!=null?lstMesa.get(position):0;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolderMesa holderMesa =null;
		
		if(view==null){
			holderMesa = new ViewHolderMesa();
			int layout = R.layout.mesa_item;
			view =inflater.inflate(layout, null);
			view.setTag(holderMesa);
			
			holderMesa.setImagem((ImageView) view.findViewById(R.id.imageMesa));
			
			
			
			holderMesa.setTextDisponibilidade((TextView) view.findViewById(R.id.textDisponibilidade));
			holderMesa.setTextNomeMesa((TextView) view.findViewById(R.id.textNumMesa));
			
		}
		else{
			holderMesa=(ViewHolderMesa) view.getTag();
		}
		
		Mesa mesa = lstMesa.get(position);
		if(mesa.getDisponibilidade().equalsIgnoreCase("Conectada")){
			holderMesa.getImagem().setBackgroundResource(R.drawable.mesa_conectada);
		}
		else {
			holderMesa.getImagem().setBackgroundResource(R.drawable.mesa_livre);
		}
		/*else if(mesa.getDisponibilidade().equalsIgnoreCase("Reservada")){
			holderMesa.getImagem().setBackgroundResource(R.drawable.mesa_reservada);
		}*/
		holderMesa.getTextDisponibilidade().setText(mesa.getDisponibilidade());
	    holderMesa.getTextNomeMesa().setText(mesa.getNome());
		return view;
	
	}

}
