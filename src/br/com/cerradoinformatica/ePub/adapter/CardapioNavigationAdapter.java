package br.com.cerradoinformatica.ePub.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;

import java.io.Serializable;
import java.util.ArrayList;

import br.com.cerradoinformatica.ePub.fragments.FragmentObserver;
import br.com.cerradoinformatica.ePub.funcionalidades.NavigationPagerActivitImp;
import br.com.cerradoinformatica.ePub.funcionalidades.Observavel;

/**
 * Created by danilo on 25/09/13.
 */
public class CardapioNavigationAdapter extends FragmentStatePagerAdapter implements ActionBar.TabListener,ViewPager.OnPageChangeListener,Observavel,Serializable{

    private static final int NUM_PAGES=3;
    private NavigationPagerActivitImp nav;
    private int currentTab=0;
    private ArrayList<FragmentObserver> arrFragment;

    private final static String KEY_NAVIGATION = "navigation";
    private final  static  String KEY_OBSERVAVEL = "observavel";
    private final  static  String TAG = "<< CardapioNavigtationAdapter >>";

    public CardapioNavigationAdapter(FragmentManager fm,NavigationPagerActivitImp nav,ArrayList<FragmentObserver> arrFragment){
        super(fm);
        this.nav=nav;
        this.arrFragment=arrFragment;
    }

    //Métodos da Tab

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        nav.changePage(tab.getPosition());
        currentTab=tab.getPosition();

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }


     //Métodos do Pager

    @Override
    public Fragment getItem(int i) {

        return  arrFragment.get(i);

    }
    @Override
    public int getCount() {


        return NUM_PAGES;
    }


    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageSelected(int i) {
        nav.changeTab(i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {


    }

    @Override
    public void addObserver(java.util.Observer o) {
        arrFragment.add((FragmentObserver) o);
    }

    @Override
    public void removeObserver(java.util.Observer o) {
        arrFragment.remove(o);
    }

    @Override
    public void notifyObservers(Bundle bundle) {
        for(FragmentObserver o :arrFragment){
            o.update(this,bundle);
        }

    }
}
