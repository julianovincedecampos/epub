package br.com.cerradoinformatica.ePub.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.cerradoinformatica.ePub.activity.AcompanhamentoDeContaTelaPrincipalActivity;
import br.com.cerradoinformatica.ePub.activity.CadastroActivity;
import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.adapter.PreferenciaProdutoAdapter;
import br.com.cerradoinformatica.ePub.funcionalidades.DatasUtils;
import br.com.cerradoinformatica.ePub.model.MovimentoMesa;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.PreferenciaProduto;
import br.com.cerradoinformatica.ePub.model.Produto;
import br.com.cerradoinformatica.ePub.model.Usuario;
import br.com.cerradoinformatica.ePub.service.MovimentoProdutoSrv;
import br.com.cerradoinformatica.ePub.service.PreferenciaProdutoServices;
import br.com.cerradoinformatica.ePub.txts.PreferenciasSelecionadasTXT;
import br.com.cerradoinformatica.ePub.txts.PreferenciasTXT;
import br.com.cerradoinformatica.ePub.txts.PubMovimentosolicitacaoAutorizacaoUtil;
import br.com.cerradoinformatica.ePub.funcionalidades.GerenciadorAlertDialog;

import br.com.cerradoinformatica.ePub.funcionalidades.NavigationPagerActivitImp;
import br.com.cerradoinformatica.ePub.funcionalidades.Observavel;
import br.com.cerradoinformatica.ePub.txts.UsuarioHerbalifeTXT;
import br.com.cerradoinformatica.ePub.txts.UsuarioTXT;
import util.HttpUtil;

/**
 * Created by Juliano Vince de Campos on 25/09/13.
 */
public class DetalhesFragments extends FragmentObserver implements View.OnClickListener, Serializable {
    //Inicio
    private TextView textTitulo;
    private TextView textDescricao;
    private TextView textValorProduto;
    private Produto produto;
    private ImageView imageProduto;
    private Button btnCancelar;
    private Button btnConfirmar;
    private TextView textQuantidade;
    private TextView textSubtotalProdutos;
    private double subTotalDeProdutos;
    private MovimentoProduto movimentoProduto, retornoMovimentoProduto;
    private MovimentoMesa movimentoMesa;
    private MovimentoProdutoSrv movimentoprodutoSrv;
    private int quantidadeProduto = 1;
    private TextView textAdicionar, textRemover;
    private int idImagemProduto;
    private DecimalFormat formato;
    private MovimetoSolicitacaoAutorizacao movimentoSolicitacaoAutorizacao;
    private static final String KEY_DETALHES_PRODUTOS = "KEY_DETALHES_PRODUTOS";
    private static final String KEY_DETALHES_QUANTIDADE = "KEY_DETALHES_QUANTIDADE";
    //Fim
    private static  final String TAG ="<< DetalhesFragment >>";
    private final static String KEY_NAVIGATION = "navigation";
    private final  static  String KEY_OBSERVAVEL = "observavel";
    private TextView textDetalhes;
    private Observavel observavel;
    private NavigationPagerActivitImp nav;
    private AlertDialog alertDialog;
    private PreferenciaProdutoServices preferenciaProdutoServices;
    private GerenciadorAlertDialog gerenciadorAlertDialog;
    private ListView listViewListaPreferenciasProdutos;
    private  List<PreferenciaProduto>listaPreferenciaProduto;
    private PreferenciaProduto preferenciaProduto;
    private Usuario usuario;

    public DetalhesFragments(Observavel observavel,NavigationPagerActivitImp nav){
        this.observavel=observavel;
        this.nav=nav;
        // observavel.addObserver(this);
    }
    public DetalhesFragments (){

    }
    //Metodo recebe parametro de Cardaoio activity
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //movimentoSolicitacaoAutorizacao = new MovimetoSolicitacaoAutorizacao();
        movimentoSolicitacaoAutorizacao = PubMovimentosolicitacaoAutorizacaoUtil.readMovimetoSolicitacaoAutorizacao(getActivity());
        usuario = UsuarioTXT.readUsuario(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup viewRoot = (ViewGroup) inflater.inflate(R.layout.frag_detalhes,container,false);

        nav=(NavigationPagerActivitImp)getActivity();
        observavel=(Observavel)getActivity();

        preferenciaProdutoServices = new PreferenciaProdutoServices(getActivity());
        listViewListaPreferenciasProdutos = (ListView) viewRoot.findViewById(R.id.listViewListaPreferenciasProdutos);
        produto = new Produto();
        movimentoProduto = new MovimentoProduto();
        movimentoMesa = new MovimentoMesa();
        //getArguments semelhante a getIntent()Intent
        movimentoMesa = (MovimentoMesa) getArguments().getSerializable("MovimentoMesa");
        Log.i(TAG, "Movimento é: " + (movimentoMesa==null));
        movimentoprodutoSrv = new MovimentoProdutoSrv(getActivity());
        textTitulo = (TextView) viewRoot.findViewById(R.id.textTitulo);
        textDescricao = (TextView) viewRoot.findViewById(R.id.textDescricao);
        textValorProduto = (TextView) viewRoot.findViewById(R.id.textValorProduto);
        imageProduto = (ImageView) viewRoot.findViewById(R.id.imageProduto);
        textQuantidade = (TextView) viewRoot.findViewById(R.id.textQuantidade);
        formato = (DecimalFormat) DecimalFormat.getInstance(Locale.getDefault());
        formato.applyPattern(".00");

        this.subTotalDeProdutos = 0.0;

        textAdicionar = (TextView) viewRoot.findViewById(R.id.textAdicionar);
        textAdicionar.setOnClickListener(this);

        textRemover = (TextView) viewRoot.findViewById(R.id.textRemover);
        textRemover.setOnClickListener(this);

        btnConfirmar = (Button) viewRoot.findViewById(R.id.btnConfirmar);
        btnConfirmar.setOnClickListener(this);

        btnCancelar = (Button) viewRoot.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);


        if(savedInstanceState !=null){
            produto = (Produto) savedInstanceState.getSerializable(KEY_DETALHES_PRODUTOS);//recupera o estado salvo do objeto produto
            this.quantidadeProduto = savedInstanceState.getInt(KEY_DETALHES_QUANTIDADE);//recupera o estado salvo da variavel quandidade de produtos
            preencheView(produto);
            Log.i(" << TESTE >> ", produto.getId() + " " + produto.getNome());
        }
        //fim
        Log.i(TAG,"Fragment detalhes criado");

        return viewRoot;
        //   movimentoSolicitacaoAutorizacao = (MovimetoSolicitacaoAutorizacao) getArguments().getSerializable("movimentoSolicitacaoAutorizacao");

    }

    public void cancelarVoltar(){
        this.quantidadeProduto = 1;
        this.subTotalDeProdutos=0.0;
        textQuantidade.setText("1");
        produto = new Produto();
        setListaInvisivel();
        nav.changePage(0);
    }
    @Override
    public void update(Observavel o, Bundle bundle) {
        this.quantidadeProduto = 1;
        this.subTotalDeProdutos=0.0;
        if (o==observavel){
            if(bundle.getSerializable(KEY_PRODUTOS)!=null){
                produto = (Produto) bundle.getSerializable(KEY_PRODUTOS);
                preencheView(produto);
            }
        }
    }
    public void preencheView(Produto produto){
        textTitulo.setText(produto.getNome().toUpperCase() + "\nR$ " + formato.format(produto.getPreco()));
        //textValorProduto.setText("Valor Unitário R$ "+ ));
        this.subTotalDeProdutos = produto.getPreco();
        textDescricao.setText("Total R$ " + formato.format(this.subTotalDeProdutos));
        if(produto.getIdGrupo()!=0){
            InputStream stream;
            try {
                stream = getResources().getAssets().open(produto.getNomeImagem());
            } catch (IOException e) {
                stream = null;
            }
            Bitmap bitmap = BitmapFactory.decodeStream(stream);
            if(bitmap != null || stream != null){
                imageProduto.setImageBitmap(bitmap);
            }else{
                BitmapDrawable bitmapDrawable = new BitmapDrawable(getActivity().getResources(), getActivity().getFileStreamPath(this.produto.getNomeImagem()).getAbsolutePath());
                imageProduto.setImageDrawable(bitmapDrawable);
            }
            filtraPreferencias();
        }
        textQuantidade.setText(""+ this.quantidadeProduto);
    }
    public void adicionaProduto(){
            this.quantidadeProduto++;
            textQuantidade.setText(""+this.quantidadeProduto);
            this.subTotalDeProdutos += produto.getPreco();
            textDescricao.setText("Total R$ " + formato.format(subTotalDeProdutos));
    }
    public void removeProduto(){
         if(this.quantidadeProduto > 1){
            this.quantidadeProduto--;
            this.subTotalDeProdutos -=produto.getPreco();
            textDescricao.setText("Total R$ " + formato.format(subTotalDeProdutos));
            textQuantidade.setText("" + this.quantidadeProduto);
        }
    }
    @Override
    public void onClick(View v) {
        if(v==btnCancelar){
            cancelarVoltar();
        }else if(v==textAdicionar){
            adicionaProduto();
        }else if(v==textRemover){
            removeProduto();
        }else if(v==btnConfirmar){
            if(UsuarioTXT.readUsuario(getActivity()) == null){
                TaskCadastro taskCadastro = new TaskCadastro(getActivity());
                taskCadastro.execute();
            }else if(produto.getId()==0){
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setIcon(R.drawable.ic_action_about);
                        builder.setTitle("MobilePub");
                        builder.setMessage("Selecione um produto!");
                        //builder.setMessage("Tente novamente.");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Toast.makeText(getActivity(),"Selecione um produto!",Toast.LENGTH_SHORT).show();
                                nav.changePage(0);
                            }});
                        //cria o AlertDialog
                        alertDialog = builder.create();
                        //Exibe
                        alertDialog.show();
              }else{
                  TaskPostProduto taskPostProduto = new TaskPostProduto(getActivity());
                  taskPostProduto.execute();
                  }
        }
    }
    private void filtraPreferencias(){
        final List<PreferenciaProduto>listaPreferenciaProduto = preferenciaProdutoServices.getPreferencias(movimentoSolicitacaoAutorizacao.getIdEstabelecimento());
        ArrayList<PreferenciaProduto> listaPreferenciaProdutosFiltrada = new ArrayList<PreferenciaProduto>();
        if(listaPreferenciaProduto != null){
            for(PreferenciaProduto preferenciaProduto: listaPreferenciaProduto){
                Log.i(TAG,"preferenciaProduto.getIdGrupo() = " + preferenciaProduto.getIdGrupo() + "produto.getId() = " + produto.getId());
                if(preferenciaProduto.getIdGrupo()==produto.getIdGrupo()){
                    listaPreferenciaProdutosFiltrada.add(preferenciaProduto);
                }
            }
            listViewListaPreferenciasProdutos.setAdapter(new PreferenciaProdutoAdapter(getActivity(), listaPreferenciaProdutosFiltrada));
        }else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setIcon(R.drawable.ic_action_about);
            builder.setTitle("MobilePub");
            builder.setMessage("Esse produto não possui preferencias!");
            //builder.setMessage("Tente novamente.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Toast.makeText(getActivity(),"Selecione um produto!",Toast.LENGTH_SHORT).show();
                }});
            //cria o AlertDialog
            alertDialog = builder.create();
            //Exibe
            alertDialog.show();
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_DETALHES_PRODUTOS,produto);//salva o estado do elemento(Objeto produtos)
        outState.putInt(KEY_DETALHES_QUANTIDADE, this.quantidadeProduto);//Sava o estado do elemento(quantidadeProdutos)
    }

    /**
     * Metodo utlizado para executar o post de confirmação de produto.
     */
    private class TaskPostProduto extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskPostProduto(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            //Cria novo ProgressDialog e exibe
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
                preferenciaProduto = PreferenciasSelecionadasTXT.readPreferencia(getActivity());
                if(preferenciaProduto == null){
                    preferenciaProduto = new PreferenciaProduto();
                    preferenciaProduto.setIdsUtilizados("");
                }
                movimentoProduto.setIdEstabelecimento(movimentoSolicitacaoAutorizacao.getIdEstabelecimento());
                movimentoProduto.setIdMesa(movimentoSolicitacaoAutorizacao.getIdMesa());
                movimentoProduto.setDataAbertura(movimentoSolicitacaoAutorizacao.getDataAbertura());
                movimentoProduto.setNumComanda(movimentoSolicitacaoAutorizacao.getNumComanda());
                movimentoProduto.setDataMovimento(DatasUtils.getStringDate("2000/01/01'T'11:22:33"));
                movimentoProduto.setIdProduto(produto.getId());
                movimentoProduto.setQuantidade(quantidadeProduto);
                movimentoProduto.setValorUnitario(produto.getPreco());
                movimentoProduto.setDetalhe(preferenciaProduto.getIdsUtilizados());
                movimentoProduto.setIdUsuario(0);
                movimentoProduto.setIdUsuarioMovel(usuario.getIdClienteDispositivo());
                movimentoProduto.setEstabelecimentoCiente(null);
                movimentoProduto.setProducaoIniciada(null);
                movimentoProduto.setProducaoConcluida(null);
                movimentoProduto.setGarConAcionado(null);
                movimentoProduto.setEntregueNaMesa(null);
                movimentoProduto.setCanceladoUsuario(null);
                movimentoProduto.setCanceladoEstabelecimento(null);
                 //movimentoProduto.setNomeProduto(produto.getNome());
                movimentoProduto.setAutorizadoGarcomMovProduto(false);
                retornoMovimentoProduto =  movimentoprodutoSrv.postMovimento(movimentoProduto);
                PreferenciasSelecionadasTXT.clearFile(context);
                PreferenciasSelecionadasTXT.clearFile(context);
                progressDialog.dismiss();
                return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if(retornoMovimentoProduto == null){
                if(HttpUtil.MENSAGEM.equalsIgnoreCase("304")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Erro: " + HttpUtil.MENSAGEM);
                    builder.setMessage("Esta mesa foi fechada pelo estabelecimento \n Tente novamente.");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            PubMovimentosolicitacaoAutorizacaoUtil.clearFile(getActivity());
                            UsuarioHerbalifeTXT.clearFile(getActivity());
                            Intent intent = new Intent(getActivity(),AcompanhamentoDeContaTelaPrincipalActivity.class);
                            startActivity(intent);
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_DESCONHECIDO")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro de Conexão");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Intent intent = new Intent(getActivity(),AcompanhamentoDeContaTelaPrincipalActivity.class);
                            //progressDialog.dismiss();
                            //startActivity(intent);
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro de Comunicação com servidor!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Intent intent = new Intent(getActivity(),AcompanhamentoDeContaTelaPrincipalActivity.class);
                            //progressDialog.dismiss();
                            //startActivity(intent);
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }else if(HttpUtil.MENSAGEM.equalsIgnoreCase("TIME_OUT")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("MobilePub");
                    builder.setMessage("Erro Time out!");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Intent intent = new Intent(getActivity(),AcompanhamentoDeContaTelaPrincipalActivity.class);
                            //progressDialog.dismiss();
                            //startActivity(intent);
                        }});
                    alertDialog = builder.create();
                    alertDialog.show();
                }
            }else {
                Toast.makeText(getActivity(), "Produto Confirmado!", Toast.LENGTH_SHORT).show();
                quantidadeProduto=1;
                subTotalDeProdutos=0.0;
                textQuantidade.setText("1");
                Intent intent = new Intent(getActivity(),AcompanhamentoDeContaTelaPrincipalActivity.class);
                startActivity(intent);
                getActivity().finish();
                //metodo utilizado para  chamar o fragment necessario(Nesse caso o fragment-grupo!
                // progressDialog.dismiss();
                setListaInvisivel();
            }
            getFragmentManager().getFragments().clear();
        }
    };
    public void setListaInvisivel(){
        listViewListaPreferenciasProdutos.setVisibility(View.INVISIBLE);
    }
    public void setListaVisivel(){
        listViewListaPreferenciasProdutos.setVisibility(View.VISIBLE);
    }
    private class TaskCadastro extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        Context context;

        public TaskCadastro(Context context ){
            this.context = context;
        }
        @Override
        protected void onPreExecute() {
            //Cria novo ProgressDialog e exibe
            progressDialog = new ProgressDialog(context);
            progressDialog = ProgressDialog.show(context, "", context.getString(R.string.app_name));
            progressDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Intent intent = new Intent(getActivity(),CadastroActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("br.com.cerradoinformatica.ePub.fragments.DetalhesFragments",movimentoSolicitacaoAutorizacao);
            intent.putExtras(bundle);
            startActivity(intent);
            getActivity().finish();
            progressDialog.dismiss();
        }
    };
}
