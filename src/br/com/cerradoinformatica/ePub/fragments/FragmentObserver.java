package br.com.cerradoinformatica.ePub.fragments;

import android.support.v4.app.Fragment;

import java.io.Serializable;

import br.com.cerradoinformatica.ePub.funcionalidades.ObserverImplementacao;

/**
 * Created by juliano on 27/09/13.
 */
public abstract class FragmentObserver extends Fragment implements ObserverImplementacao,Serializable {

    protected static  final String KEY_DETALHES="detalhes";
    protected static final String KEY_GRUPOS="grupos";
    protected  static final String KEY_PRODUTOS="produtos";
    public static final String ID_PRODUTOS = "ID_PRODUTOS";
    protected   static final int ATUALIZAR_VIEW=1;
    protected static final String ID_GRUPOS = "ID_GRUPOS";
    public static final String ESTADO_INICIAL = "ESTADO_INICIAL";
    public static final String PREFERENCIAS_PRODUTO = "PREFERENCIAS_PRODUTO";
}
