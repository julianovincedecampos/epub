package br.com.cerradoinformatica.ePub.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.cerradoinformatica.ePub.activity.R;
import br.com.cerradoinformatica.ePub.adapter.ExpandableListViewAdapter;
import br.com.cerradoinformatica.ePub.adapter.ProdutoAdapter;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.Produto;
import br.com.cerradoinformatica.ePub.service.ProdutoSrv;
import br.com.cerradoinformatica.ePub.funcionalidades.Transacao;
import br.com.cerradoinformatica.ePub.funcionalidades.TransacaoTask;
import br.com.cerradoinformatica.ePub.txts.PubMovimentosolicitacaoAutorizacaoUtil;
import br.com.cerradoinformatica.ePub.txts.PubProdutoUtil;
import br.com.cerradoinformatica.ePub.funcionalidades.Listas;
import br.com.cerradoinformatica.ePub.funcionalidades.NavigationPagerActivitImp;
import br.com.cerradoinformatica.ePub.funcionalidades.Observavel;
/**
 * Created by Juliano Vince de Campos on 25/09/13.
 */
public class ProdutosFragments extends FragmentObserver implements AdapterView.OnItemClickListener, Transacao,Serializable, ExpandableListView.OnChildClickListener {

    //Inicio
    private List<Produto> listaAuxProdutos;
    private List<Produto> lstProduto;
    private ProdutoSrv produtoSrv;
    private ListView listViewProdutos;
    private Listas produtosObj;
    private TransacaoTask transacaoTask;
    MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao;

    //Fim

   //Tela
    ExpandableListView expandableListViewListaProdutos;
   //Tela

    private Button btnDetalhes;
    private TextView textView;

    private String TAG ="<< ProdutosGragment >> ";
    private Observavel observavel;

    private final static String KEY_NAVIGATION = "navigation";
    private final  static  String KEY_OBSERVAVEL = "observavel";

    private NavigationPagerActivitImp nav;


    public  ProdutosFragments(Observavel observavel,NavigationPagerActivitImp nav){
        this.nav=nav;
        this.observavel=observavel;
        // observavel.addObserver(this);

    }
    public ProdutosFragments (){

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup viewRoot = (ViewGroup) inflater.inflate(R.layout.produtos_fragment,container,false);
        Log.i(TAG,"oncCreateView");

        //Inicio
//        listViewProdutos = (ListView) viewRoot.findViewById(R.id.listProdutos);
        lstProduto = new ArrayList<Produto>();
        produtosObj = new Listas();
//        listViewProdutos.setOnItemClickListener(this);
        produtoSrv = new ProdutoSrv(getActivity());
        listaAuxProdutos = new ArrayList<Produto>();
        movimetoSolicitacaoAutorizacao = PubMovimentosolicitacaoAutorizacaoUtil.readMovimetoSolicitacaoAutorizacao(getActivity());
        //Fim

        //Tela
        expandableListViewListaProdutos = (ExpandableListView) viewRoot.findViewById(R.id.expandableListViewListaProdutos);
        expandableListViewListaProdutos.setOnChildClickListener(this);
        //Tela

        nav=(NavigationPagerActivitImp)getActivity();
        observavel=(Observavel)getActivity();


        transacaoTask = new TransacaoTask(getActivity(), this, R.string.app_name);
        transacaoTask.execute();

        if(PubProdutoUtil.readProduto(getActivity(),movimetoSolicitacaoAutorizacao) != null){
            lstProduto = produtoSrv.getTotosProdutos(movimetoSolicitacaoAutorizacao);
            listaAuxProdutos = lstProduto;
            this.produtosObj.setListProduto(lstProduto);
        }else{
            transacaoTask = new TransacaoTask(getActivity(), this, R.string.app_name);
            transacaoTask.execute();
        }

        return viewRoot;
    }
    @Override
    public void update(Observavel o, Bundle bundle) {
        if(o == observavel){
            if(bundle.getInt(ID_GRUPOS)!=0){
                getProdutoGrupos(bundle.getInt(ID_GRUPOS));
                atualizarView();
            }else if(bundle.getString(ESTADO_INICIAL)!=null){
                listaAuxProdutos = lstProduto;
                atualizarView();
            }
        }
    }
    public void getProdutoGrupos(int idGrupo){
        listaAuxProdutos = new ArrayList<Produto>();
        for(Produto produto :lstProduto){
            if(produto.getIdGrupo() == idGrupo){
                listaAuxProdutos.add(produto);
            }
        }
    }
    @Override
    public void executar() throws Exception {
        lstProduto = produtoSrv.getTotosProdutos(movimetoSolicitacaoAutorizacao);
        listaAuxProdutos = lstProduto;
        this.produtosObj.setListProduto(lstProduto);
    }
    public void getProdutos(){
        /*lstProduto = produtoSrv.getTotosProdutos(movimetoSolicitacaoAutorizacao);
        listaAuxProdutos = lstProduto;
        this.produtosObj.setListProduto(lstProduto);


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Problema de conexão.\n Deseja tentar novamente ?");
        builder.setPositiveButton("sim",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getProdutos();
                }
            });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
            }
            });*/
    }

    @Override
    public void atualizarView() {
        //listViewProdutos.setAdapter(new ProdutoAdapter(getActivity(), listaAuxProdutos));
        //Log.i("ProdutoActivity", "LstProduto : "+lstProduto.size());
       // ExpandableListView expandableListViewListaProdutos = (ExpandableListView) viewRoot.findViewById(R.id.expandableListViewListaProdutos);
//        expandableListViewListaProdutos.setAdapter(new ExpandableListViewAdapter(getActivity()));
        expandableListViewListaProdutos.setAdapter(new ExpandableListViewAdapter(getActivity(),listaAuxProdutos));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int posicao, long id) {
        //Bundle bundle = new Bundle();
        //bundle.putSerializable(KEY_PRODUTOS, listaAuxProdutos.get(posicao));
        //observavel.notifyObservers(bundle);
        //nav.next();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.produtosObj.setListProduto(lstProduto);
        outState.putSerializable(Listas.KEY_PRODUTO,produtosObj);

        produtosObj = new Listas();
        produtosObj.setListProduto(listaAuxProdutos);
        outState.putSerializable(Listas.KEY_PRODUTO + ID_GRUPOS, produtosObj);
    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long l) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_PRODUTOS, listaAuxProdutos.get(i));
        observavel.notifyObservers(bundle);
        nav.next();
        return true;
    }
}


