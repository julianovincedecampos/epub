package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import br.com.cerradoinformatica.ePub.model.Usuario;
import util.IOUtils;

public class UsuarioTXT {

	private static final String FILE_USUARIO = "usuario.txt";

	public static void writeUsuario(Context context, Usuario usuario) {
		IOUtils.writeObjectInFile(context, usuario, FILE_USUARIO);
	}

	public static Usuario readUsuario(Context context) {
		Usuario usuario =IOUtils.readObjectInFile(context, FILE_USUARIO);
		return usuario;
	}

	public static void clearFile(Context context) {
		IOUtils.writeObjectInFile(context, null, FILE_USUARIO);
	}
}
