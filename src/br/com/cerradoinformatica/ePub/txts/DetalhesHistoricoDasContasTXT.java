package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;
import java.util.ArrayList;
import br.com.cerradoinformatica.ePub.model.HistoricoConta;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import util.DateUtil;
import util.IOUtils;

/**
 * Created by juliano on 10/01/14.
 */
public class DetalhesHistoricoDasContasTXT {
    private static String FILE_DETALHES_HISTORICOS = "" ;

    public static void writeHistoricos(Context context, String stringHistoricos,MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
        alteraNomeArquivo(movimetoSolicitacaoAutorizacao);
        IOUtils.writeObjectInFile(context, stringHistoricos, FILE_DETALHES_HISTORICOS);
    }
    public static ArrayList<HistoricoConta> readHistoricosList(Context context){
        ArrayList<String>arrayHistoricoString;
        ArrayList<HistoricoConta>arrayHistoricoContas = new ArrayList<HistoricoConta>();
        HistoricoConta historicoConta;

        arrayHistoricoString = IOUtils.readListInFile(context);

        for(int i=0; i < arrayHistoricoString.size(); i++){
            historicoConta  = new HistoricoConta();
            String[] vetorQuebraNome = arrayHistoricoString.get(i).toString().split("_");
            historicoConta.setDate(DateUtil.getStringData(vetorQuebraNome[1]));
            historicoConta.setIdEstabelecimento(Integer.parseInt(vetorQuebraNome[2]));
            historicoConta.setNomeArquivo(arrayHistoricoString.get(i));
            arrayHistoricoContas.add(historicoConta);
        }
        return arrayHistoricoContas;
    }
    public static String readHistoricosProdutos(Context context, String arquivo){
        String stringHistoricos = IOUtils.readObjectInFile(context, arquivo);
        return stringHistoricos;
    }
    private static void alteraNomeArquivo(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
        FILE_DETALHES_HISTORICOS = "MovProduto_"+movimetoSolicitacaoAutorizacao.getDataAbertura()+"_"+movimetoSolicitacaoAutorizacao.getIdEstabelecimento()+"_"+movimetoSolicitacaoAutorizacao.getNumComanda()+"_.txt";
    }
    public static void clearFile(String arquivo) {
        IOUtils.clearFile(arquivo);
    }
}
