package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import util.IOUtils;
import util.URLUtil;

/**
 * Created by juliano on 25/02/14.
 */
public class PreferenciasTXT {

    private static String FILE_PREFERENCIA_PRODUTOS = "preferencias.txt";
    private static String AMBIENTE;
    private static String VALOR;

    private static void alteraFilePrefereciasProdutos(int idEstabelecimento){
        AMBIENTE = String.valueOf(URLUtil.mode);
        if(AMBIENTE.equalsIgnoreCase("DESENVOLVIMENTO")){
            VALOR = "T";
        }else if(AMBIENTE.equalsIgnoreCase("PRODUCAO")){
            VALOR = "P";
        }
        FILE_PREFERENCIA_PRODUTOS = "preferencias-"+VALOR+idEstabelecimento+".txt";
    }
    public static void writePreferencias(Context context, String stringPreferencias,int idEstabelecimento){
        //alteraFilePrefereciasProdutos(idEstabelecimento);
        IOUtils.writeObjectInFile(context, stringPreferencias, FILE_PREFERENCIA_PRODUTOS);
    }
    public static String readPreferencias(Context context, int idEstabelecimento){
        //alteraFilePrefereciasProdutos(idEstabelecimento);
        String stringPreferencias = IOUtils.readObjectInFile(context, FILE_PREFERENCIA_PRODUTOS);
        return stringPreferencias;
    }
    public static void clearFile(Context context) {
        IOUtils.writeObjectInFile(context, null, FILE_PREFERENCIA_PRODUTOS);
    }
}
