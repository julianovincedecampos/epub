package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import util.IOUtils;

/**
 * Created by juliano on 05/05/2014.
 */
public class URLImagemFacebook {

    private static String FILE_URL_IMAGEM_FACEBOOK = "urlImagemFacebook.txt";

    public static void writeURLImagemFacebook(Context context, String stringURLImagemFacebook){
        IOUtils.writeObjectInFile(context, stringURLImagemFacebook, FILE_URL_IMAGEM_FACEBOOK);
    }
    public static String readURLImagemFacebook(Context context){
        String stringURLImagemFacebook = IOUtils.readObjectInFile(context, FILE_URL_IMAGEM_FACEBOOK);
        return stringURLImagemFacebook;
    }
    public static void clearFile(Context context) {
        IOUtils.writeObjectInFile(context, null, FILE_URL_IMAGEM_FACEBOOK);
    }
}
