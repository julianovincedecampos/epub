package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;

/**
 * Created by juliano on 06/03/14.
 */
public class UtilTXT {

    public static void limparTodosArquivos(Context context){
        PreferenciasTXT.clearFile(context);
        PubProdutoUtil.clearFile(context);
        PubGrupoUtil.clearFile(context);
        PubMovimentosolicitacaoAutorizacaoUtil.clearFile(context);
    }
}
