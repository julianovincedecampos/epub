package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import br.com.cerradoinformatica.ePub.model.Perfil;
import br.com.cerradoinformatica.ePub.model.Usuario;
import util.IOUtils;

/**
 * Created by juliano on 03/02/14.
 */
public class PerfilTXT {
    private static String FILE_PERFIL = "perfil.txt";

    public static void writePerfil(Context context, Perfil perfil){
        IOUtils.writeObjectInFile(context, perfil, FILE_PERFIL);
    }
    public static Perfil readPerfil(Context context){
        Perfil perfil = IOUtils.readObjectInFile(context, FILE_PERFIL);
        return perfil;
    }
    public static void clearFile(Context context) {
        IOUtils.writeObjectInFile(context, null, FILE_PERFIL);
    }
}
