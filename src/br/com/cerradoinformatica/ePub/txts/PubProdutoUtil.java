package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import util.IOUtils;
import util.URLUtil;

/**
 * Created by juliano on 17/12/13.
 */
public class PubProdutoUtil {

    private static  String FILE_PRODUTO ="produto.txt";
    private static String AMBIENTE;
    private static String VALOR;

    private static void ALTERA_FILE_PRODUTO(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
        AMBIENTE = String.valueOf(URLUtil.mode);
        if(AMBIENTE.equalsIgnoreCase("DESENVOLVIMENTO")){
            VALOR = "T";
        }else if(AMBIENTE.equalsIgnoreCase("PRODUCAO")){
            VALOR = "P";
        }
        FILE_PRODUTO = "produtos-"+VALOR+ movimetoSolicitacaoAutorizacao.getIdEstabelecimento() + ".txt";
    }
    public static void writeProduto(Context context, String stringProduto, MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
        //ALTERA_FILE_PRODUTO(movimetoSolicitacaoAutorizacao);
        IOUtils.writeObjectInFile(context, stringProduto, FILE_PRODUTO);
    }
    public static String readProduto(Context context, MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
        //ALTERA_FILE_PRODUTO(movimetoSolicitacaoAutorizacao);
        String stringProduto = IOUtils.readObjectInFile(context, FILE_PRODUTO);
        return stringProduto;
    }
    public static void clearFile(Context context) {
        IOUtils.writeObjectInFile(context, null, FILE_PRODUTO);
    }
}

