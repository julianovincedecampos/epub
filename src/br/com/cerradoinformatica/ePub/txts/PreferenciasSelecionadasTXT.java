package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import br.com.cerradoinformatica.ePub.model.PreferenciaProduto;
import util.IOUtils;

/**
 * Created by juliano on 10/02/14.
 */
public class PreferenciasSelecionadasTXT {
    private static String FILE_PREFERENCIA_PRODUTOS = "preferenciasProdutos.txt";

    public static void writePreferencia(Context context, PreferenciaProduto preferenciaProduto){
        IOUtils.writeObjectInFile(context, preferenciaProduto, FILE_PREFERENCIA_PRODUTOS);
    }
    public static PreferenciaProduto readPreferencia(Context context){
        PreferenciaProduto preferenciaProduto = IOUtils.readObjectInFile(context, FILE_PREFERENCIA_PRODUTOS);
        return preferenciaProduto;
    }
    public static void clearFile(Context context) {
        IOUtils.writeObjectInFile(context, null, FILE_PREFERENCIA_PRODUTOS);
    }
}
