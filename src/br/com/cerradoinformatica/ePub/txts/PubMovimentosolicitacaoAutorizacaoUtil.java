package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import java.util.ArrayList;

import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import util.IOUtils;

/**
 * Created by juliano on 26/12/13.
 */
public class PubMovimentosolicitacaoAutorizacaoUtil {

    private static String FILE_MOVIMENTO_SOLICITACAO_AUTORIZACAO = "movimetoSolicitacaoAutorizacao.txt";

    public static void writeMovimetoSolicitacaoAutorizacao(Context context, MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao) {
        IOUtils.writeObjectInFile(context, movimetoSolicitacaoAutorizacao, FILE_MOVIMENTO_SOLICITACAO_AUTORIZACAO);
    }
    public static MovimetoSolicitacaoAutorizacao readMovimetoSolicitacaoAutorizacao(Context context) {
        MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao =IOUtils.readObjectInFile(context, FILE_MOVIMENTO_SOLICITACAO_AUTORIZACAO);
        return movimetoSolicitacaoAutorizacao;
    }

    public static void clearFile(Context context) {
        IOUtils.writeObjectInFile(context, null, FILE_MOVIMENTO_SOLICITACAO_AUTORIZACAO);
    }
}
