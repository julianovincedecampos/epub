package br.com.cerradoinformatica.ePub.txts;

import android.content.Context;

import br.com.cerradoinformatica.ePub.model.Usuario;
import br.com.cerradoinformatica.ePub.model.UsuarioHerbalife;
import util.IOUtils;

/**
 * Created by juliano on 10/04/14.
 */
public class UsuarioHerbalifeTXT {

    private static final String FILE_USUARIO_HERBALIFE = "usuarioHerbalife.txt";

    public static void writeUsuarioHerbalife(Context context, UsuarioHerbalife usuarioHerbalife) {
        IOUtils.writeObjectInFile(context, usuarioHerbalife, FILE_USUARIO_HERBALIFE);
    }

    public static UsuarioHerbalife readUsuarioHerbalife(Context context) {
        UsuarioHerbalife usuarioHerbalife = IOUtils.readObjectInFile(context, FILE_USUARIO_HERBALIFE);
        return usuarioHerbalife;
    }

    public static void clearFile(Context context) {
        IOUtils.writeObjectInFile(context, null, FILE_USUARIO_HERBALIFE);
    }
}
