package br.com.cerradoinformatica.ePub.service;

import android.content.Context;

import org.apache.http.client.ClientProtocolException;

import java.util.List;

import br.com.cerradoinformatica.ePub.model.Mesa;
import util.HttpUtil;
import util.JSonParser;
import util.URLUtil;



public class MesaSrv {
	
	private final String TAG ="MESA SRV";
	
	private Context context;
	
	public MesaSrv(Context context) {
		this.context=context;
	}

    public List<Mesa> getMesas(int idEstabelecimento) throws ClientProtocolException {
        final String URL = URLUtil.getUrlMesas(context)+"/ChaveX?pIdEstabelecimento="+idEstabelecimento;
        String strJson=HttpUtil.httpGet(URL);
        List<Mesa> lstMesa = JSonParser.jsonToMesaList(strJson);
        return lstMesa;
    }
    public Mesa getMesa(int idEstabelecimento, int idMesa) throws ClientProtocolException {
        final String URL = URLUtil.getUrlMesa(context)+"/ChaveX?pIdEstabelecimento="+idEstabelecimento+"&pIdMesa="+idMesa;
        String stringJson = HttpUtil.httpGet(URL);
        Mesa mesa = JSonParser.jsonToMesa(stringJson);
        return mesa;
    }
		/*String url ="http://mobilepubapi.cloudapp.net/api/mesasEstabelecimento/"+idEstabelecimento;
		
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAccept(Collections.singletonList(new MediaType("application","json")));
	    HttpEntity<?> requestEntity = new HttpEntity<Mesa>(requestHeaders);
		
		RestTemplate restTemplete = new RestTemplate();
		restTemplete.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		
		ResponseEntity<Mesa[]> responseEntity = restTemplete.exchange(url, HttpMethod.GET, requestEntity, Mesa[].class);
		
		Mesa[] mesas = responseEntity.getBody();
		
		List<Mesa> lstMesas = new ArrayList<Mesa>();
		
		for(int i=0;i<mesas.length;i++){
			lstMesas.add(mesas[i]);
		}
		
		return lstMesas;*/
	
//	public String postMovimento(MovimentoMesa mov){
//		
//		final String URL ="http://mobilepub.cloudapp.net/api/movaberturamesa";
//		
////		//Configurando cabe�alho		
////		HttpHeaders requestHeaders =new HttpHeaders();
////		requestHeaders.setContentType(new MediaType("application","json"));
////		HttpEntity<MovimentoMesa> requestEntity = new HttpEntity<MovimentoMesa>(mov, requestHeaders);
////		
////		//Criando um Rest Temlate
////		RestTemplate restTemplet = new RestTemplate();
////		
////		//Adcionando Conversores
////		restTemplet.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
////		restTemplet.getMessageConverters().add(new StringHttpMessageConverter());
////		
////		//Faz post HTTP passando o JSON e retornando uma String;
////		
////		ResponseEntity<String> responseEntity = restTemplet.exchange(URL, HttpMethod.POST, requestEntity, String.class);
////		 
////		String result =responseEntity.getBody();
//		String result = "";
//		
//		MovimentoMesa movResposta = getResponse(URL, mov);
//		
//		IOUtils.writeObjectInFile(context, movResposta, "movimento.txt");
//		
//		Log.i("GG", result);
//		return result;
//		
//	}
//	
//	private MovimentoMesa getResponse(String url, MovimentoMesa movimento){
//		
//		try{
//		DefaultHttpClient httpClient = new DefaultHttpClient();
//		HttpPost httpPost = new HttpPost(url);
//		JSONObject holder = getJsonMovimento(movimento);
//		
//		StringEntity se = new StringEntity(holder.toString());
//		se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,"application/json"));
//		httpPost.setEntity(se);
//		//httpPost.setHeader("Accept","application/json");
//		HttpResponse response = httpClient.execute(httpPost);
//		
//		if(response==null){
//			return null;
//		}
//		
//		ObjectMapper mapper = new ObjectMapper();
//		MovimentoMesa mv = mapper.readValue(IOUtils.toString(response.getEntity().getContent(), "UTF-8"),MovimentoMesa.class);
//		
//		
////		return IOUtils.toString(response.getEntity().getContent(), "UTF-8");
//		return mv;
//		
//		
//		}catch(Exception e){
//			Log.e(TAG, e.getMessage());
//			return null;
//		}
//		
//	}
//	
//	
//	private JSONObject getJsonMovimento(MovimentoMesa movimento) {
//		
//		JSONObject holder = new JSONObject();
//		
//		try {
//			holder.put("IdEstabelecimento", movimento.getIdEstabelecimento());
//			
//			holder.put("IdMesa", movimento.getIdMesa());
//			holder.put("Data",movimento.getData());
//			holder.put("Numero_da_Comanda", movimento.getNumComanda());
//			
//			return holder;
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//		
//		
//	}

}
