package br.com.cerradoinformatica.ePub.service;

import java.util.List;

import util.HttpUtil;
import util.JSonParser;
import util.URLUtil;

import android.content.Context;
import br.com.cerradoinformatica.ePub.model.Estabelecimento;

public class EtabelecimentoSrv {
	private final Context context;
	public EtabelecimentoSrv(Context context) {
		this.context=context;
	}
	public List<Estabelecimento> getEstabelecimentos(){
		final String URL= URLUtil.getUrlEstabelecimento(context)+"/?pIdGrupo=0";
		String strJson = HttpUtil.httpGet(URL);
		List<Estabelecimento> lstEstabelecimento = JSonParser.jsonToEstabelecimentoList(strJson);
		return lstEstabelecimento;
	}
}
