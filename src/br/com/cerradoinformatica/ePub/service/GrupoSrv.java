package br.com.cerradoinformatica.ePub.service;

import java.util.List;

import br.com.cerradoinformatica.ePub.model.Grupo;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.txts.PubGrupoUtil;
import util.HttpUtil;
import util.JSonParser;
import util.URLUtil;
import android.content.Context;

public class GrupoSrv {

	private Context context;
    public static String stringJsonGrupo;
    private List<Grupo> lstGrupos;

    public GrupoSrv(Context context) {
		this.context=context;
	}

	/*public List<Grupo> getGrupos(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao) {

        if(PubGrupoUtil.readGrupo(context,movimetoSolicitacaoAutorizacao) != null){
            stringJsonGrupo =  PubGrupoUtil.readGrupo(context,movimetoSolicitacaoAutorizacao);
        }else{
            final String URL=URLUtil.getUrlGruposProdutos(context)+"ChaveX?pIdEstabelecimento="+movimetoSolicitacaoAutorizacao.getIdEstabelecimento()+"&pId=0";
            stringJsonGrupo = HttpUtil.httpGet(URL);
            PubGrupoUtil.writeGrupo(context,stringJsonGrupo,movimetoSolicitacaoAutorizacao);
	    }
        lstGrupos = JSonParser.jsonToGrupoProdutoList(stringJsonGrupo);
        return lstGrupos;
    }*/
    public List<Grupo> getGrupos(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao) {

        if(PubGrupoUtil.readGrupo(context,movimetoSolicitacaoAutorizacao) != null){
            stringJsonGrupo =  PubGrupoUtil.readGrupo(context,movimetoSolicitacaoAutorizacao);
            lstGrupos = JSonParser.jsonToGrupoProdutoList(stringJsonGrupo);
        }else{
            final String URL=URLUtil.getUrlGruposProdutos(context)+"ChaveX?pIdEstabelecimento="+movimetoSolicitacaoAutorizacao.getIdEstabelecimento()+"&pSomenteVisivel="+true;
            stringJsonGrupo = HttpUtil.httpGet(URL);
            if(stringJsonGrupo.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR")||stringJsonGrupo.equalsIgnoreCase("TIME_OUT")||stringJsonGrupo.equalsIgnoreCase("ERRO_DESCONHECIDO")){
                lstGrupos = null;
            }else{
                PubGrupoUtil.writeGrupo(context,stringJsonGrupo,movimetoSolicitacaoAutorizacao);
                lstGrupos = JSonParser.jsonToGrupoProdutoList(stringJsonGrupo);
            }
        }
        return lstGrupos;
    }
}
