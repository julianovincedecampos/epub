package br.com.cerradoinformatica.ePub.service;

import br.com.cerradoinformatica.ePub.model.DateUtil;
import br.com.cerradoinformatica.ePub.model.FechamentoConta;
import br.com.cerradoinformatica.ePub.model.MovimentoProduto;
import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.RetornoPostFechamento;
import util.HttpUtil;
import util.JSonParser;
import util.URLUtil;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.List;


public class FechamentoContaSrv {
    private static final String TAG = "FechamentoContaSrv";
    private Context context;
    private String jsonRetorno, stringJson;
    private RetornoPostFechamento retornoPostFechamento;
    private List<MovimentoProduto> lstProduto;

    public FechamentoContaSrv(Context context){
        this.context = context;
    }

    public RetornoPostFechamento postFechamentoContaService(FechamentoConta fechamentoConta){
        jsonRetorno = "";
        retornoPostFechamento = null;

        final String URL = URLUtil.getUrlFechamentoConta(context);

        stringJson = JSonParser.fechamentoContaToJson(fechamentoConta);

        jsonRetorno = HttpUtil.httpPost(URL, stringJson);

        if(jsonRetorno.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR") || jsonRetorno.equalsIgnoreCase("TIME_OUT") || jsonRetorno.equalsIgnoreCase("ERRO_DESCONHECIDO") || jsonRetorno.equalsIgnoreCase("304")){
            retornoPostFechamento = null;
        }else{
            retornoPostFechamento = JSonParser.jsonRetornoPost(jsonRetorno);
        }
        return retornoPostFechamento;
    }

    public List<MovimentoProduto> getProdutoSelecionadosFechamento(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
        lstProduto = null;
        final String URL = URLUtil.getUrlMovProduto(context)+"ChaveX?pIdEstabelecimento="+movimetoSolicitacaoAutorizacao.getIdEstabelecimento()
                +"&pIdMesa="+movimetoSolicitacaoAutorizacao.getIdMesa()
                +"&pDataAbertura="+ DateUtil.getWebApiDateString(DateUtil.getWebApiDate(movimetoSolicitacaoAutorizacao.getDataAbertura()))
                +"&pNumeroComanda="+movimetoSolicitacaoAutorizacao.getNumComanda()
                +"&pAlfabetica="+true;

        String stringJson=HttpUtil.httpGet(URL);

        Log.i(TAG,"string Json = " + stringJson);
        lstProduto = JSonParser.jsonToMovimentoProdutoListFechamento(stringJson);
        return lstProduto;
    }
}
