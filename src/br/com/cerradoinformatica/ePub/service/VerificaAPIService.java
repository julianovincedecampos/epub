package br.com.cerradoinformatica.ePub.service;

import android.content.Context;
import android.util.Log;

import util.HttpUtil;
import util.URLUtil;

/**
 * Created by juliano on 09/12/13.
 */
public class VerificaAPIService {
    private final String  TAG = "<<VerificaAPIService>>";
    private Context context;
    private  String stringJson;

    public VerificaAPIService(Context context){
        this.context = context;
    }
    public String getVerificaAPISerive(){
        final String URL = URLUtil.getUrlVerificaAPISerive(context);
        stringJson = HttpUtil.httpGet(URL);
        Log.e(TAG, "stringJson: " + stringJson);
        return stringJson;
    }
}
