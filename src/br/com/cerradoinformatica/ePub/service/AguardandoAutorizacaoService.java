package br.com.cerradoinformatica.ePub.service;

import android.content.Context;

import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import util.HttpUtil;
import util.JSonParser;
import util.URLUtil;

/**
 * Created by juliano on 26/12/13.
 */
public class AguardandoAutorizacaoService {
    private final String TAG ="AGUARDANDO_AUTORIZACAO_SERVICE";

    private Context context;

    public AguardandoAutorizacaoService(Context context) {
        this.context=context;
    }
    public  MovimetoSolicitacaoAutorizacao getMovimetoSolicitacaoAutorizacao(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){
        final String URL = URLUtil.getUrlMovSoliciatcaoAuturizacao(context)+"/ChaveX?pIdEstabelecimento="+movimetoSolicitacaoAutorizacao.getIdEstabelecimento()+"&pIdMesa="+movimetoSolicitacaoAutorizacao.getIdMesa()+"&pDataSolicitacao="+movimetoSolicitacaoAutorizacao.getDataSolicitacao()+"&pNumeroSolicitacao="+movimetoSolicitacaoAutorizacao.getNumero();
        String stringJson = HttpUtil.httpGet(URL);
        MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacaoRetorno = JSonParser.jsonToMovimetoSolicitacaoAutorizacao(stringJson);
        return movimetoSolicitacaoAutorizacaoRetorno;
    }
}
