package br.com.cerradoinformatica.ePub.service;

import java.util.ArrayList;
import java.util.List;

import br.com.cerradoinformatica.ePub.model.MovimetoSolicitacaoAutorizacao;
import br.com.cerradoinformatica.ePub.model.Produto;
import br.com.cerradoinformatica.ePub.txts.PubProdutoUtil;
import util.HttpUtil;
import util.JSonParser;
import util.URLUtil;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
/**
 * Created by Juliano Vince de Campos on 25/09/13.
 */
public class ProdutoSrv {
	private final Context context;
    private String stringJsonProdutos;
    public static String MENSAGEM_ERRO = "";
    private List<Produto>lstProduto;

    public ProdutoSrv(Context context) {
		this.context=context;
	}
    /* public List<Produto> getTotosProdutos(boolean atualizaProdutos, MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){

            lstProduto = null;
            if(atualizaProdutos==true){
                String URL = URLUtil.getUrlProdutosEstabelecimento(context)+ "ChaveX?pIdEstabelecimento="+movimetoSolicitacaoAutorizacao.getIdEstabelecimento()+"&pIdGrupo="+0+"&pIdProduto="+0;
                Log.i("Todos Produtos", "URL : " + URL);
                stringJsonProdutos = HttpUtil.httpGet(URL);
                if(stringJsonProdutos.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR") || stringJsonProdutos.equalsIgnoreCase("TIME_OUT") || stringJsonProdutos.equalsIgnoreCase("ERRO_DESCONHECIDO")){
                    lstProduto = null;
                }else{
                    PubProdutoUtil.writeProduto(context, stringJsonProdutos);
                    lstProduto = JSonParser.jsonToProdutoList(stringJsonProdutos);
                }
            }
            return lstProduto;
        }*/
    public List<Produto> getTotosProdutos(MovimetoSolicitacaoAutorizacao movimetoSolicitacaoAutorizacao){

        lstProduto = null;

        if(PubProdutoUtil.readProduto(context,movimetoSolicitacaoAutorizacao) != null){
            stringJsonProdutos =  PubProdutoUtil.readProduto(context,movimetoSolicitacaoAutorizacao);
            lstProduto = JSonParser.jsonToProdutoList(stringJsonProdutos);
        }else{
            String URL = URLUtil.getUrlProdutosEstabelecimento(context)+ "ChaveX?pIdEstabelecimento="+movimetoSolicitacaoAutorizacao.getIdEstabelecimento()+"&pIdGrupo="+0+"&pIdProduto="+0;
            stringJsonProdutos = HttpUtil.httpGet(URL);
            if(stringJsonProdutos.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR")||stringJsonProdutos.equalsIgnoreCase("TIME_OUT")||stringJsonProdutos.equalsIgnoreCase("ERRO_DESCONHECIDO")){
                lstProduto = null;
            }else{
                PubProdutoUtil.writeProduto(context, stringJsonProdutos,movimetoSolicitacaoAutorizacao);
                lstProduto = JSonParser.jsonToProdutoList(stringJsonProdutos);
            }
        }
        return lstProduto;
    }
}
