package br.com.cerradoinformatica.ePub.service;

/**
 * Created by juliano on 08/01/14.
 */
import android.content.Context;

import org.apache.http.client.ClientProtocolException;

import java.util.ArrayList;
import java.util.List;


import br.com.cerradoinformatica.ePub.model.PreferenciaProduto;
import br.com.cerradoinformatica.ePub.txts.PreferenciasTXT;
import util.HttpUtil;
import util.JSonParser;
import util.URLUtil;
public class PreferenciaProdutoServices {
    private Context context;
    private String stringJson;
    private List<PreferenciaProduto> listaPreferenciaProduto = null;

    public PreferenciaProdutoServices(Context context) {
        this.context=context;
    }

    public List<PreferenciaProduto> getPreferencias(int idEstabelecimento){

        if(PreferenciasTXT.readPreferencias(context,idEstabelecimento) != null){
            stringJson = PreferenciasTXT.readPreferencias(context, idEstabelecimento);
            listaPreferenciaProduto = JSonParser.jsonToPreferenciaProdutoListChar(stringJson);
        }else{
            final String URL = URLUtil.getUrlPreferenciaProduto(context)+"ChaveX?pIdEstabelecimento="+idEstabelecimento+"&pIdGrupo="+0;
            stringJson = HttpUtil.httpGet(URL);
            if(!stringJson.equalsIgnoreCase("ERRO_COMUNICACAO_COM_O_SERVIDOR") || !stringJson.equalsIgnoreCase("TIME_OUT")|| !stringJson.equalsIgnoreCase("ERRO_DESCONHECIDO") || !stringJson.equalsIgnoreCase("") ||!stringJson.equalsIgnoreCase("ERRO: (404)")){
                PreferenciasTXT.writePreferencias(context, stringJson, idEstabelecimento);
                listaPreferenciaProduto = JSonParser.jsonToPreferenciaProdutoListChar(stringJson);
            }
        }
        return listaPreferenciaProduto;
    }
}