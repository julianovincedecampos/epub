package br.com.cerradoinformatica.ePub.service;

import android.content.Context;
import android.util.Log;

import br.com.cerradoinformatica.ePub.model.Usuario;
import util.HttpUtil;
import util.JSonParser;
import util.URLUtil;

/**
 * Created by juliano on 11/02/14.
 */
public class UsuarioDispositivoService {

    private Context context;
    private String jsonRetorno;
    private Usuario usuarioResposta;
    private String TAG = "<< UsuarioDispositivoService >>";

    public UsuarioDispositivoService(Context context){
        this.jsonRetorno = "";
        this.usuarioResposta = null;
        this.context = context;
    }

    public Usuario postUsuario(Usuario usuario) {
        final String URL = URLUtil.getUrlUsuario(context);
        String stringJson = JSonParser.usuarioDispositivoToJson(usuario);
        Log.i(TAG, "Json produto : " + stringJson);

        String jsonRetorno = HttpUtil.httpPost(URL, stringJson);
        usuarioResposta = JSonParser.jsonToUsuario(jsonRetorno);
        return usuarioResposta;
    }
    public  Usuario getUsuario(String email,String senha){
        Usuario usuario;
        final String URL = URLUtil.getUrlUsuario(context)+"ChaveX?pEmail="+email+"&pSenha="+senha;
        String stringJson = HttpUtil.httpGet(URL);

        if(stringJson == null || stringJson.equalsIgnoreCase("")){
            usuario = null;
        }else{
            usuario = JSonParser.jsonToUsuarioArray(stringJson);
            //usuario = JSonParser.jsonToUsuario(stringJson);
        }
        return usuario;
    }
}
